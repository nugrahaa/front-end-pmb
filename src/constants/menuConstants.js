import { ROUTE_ABOUT, ROUTE_PUZZLE } from "./routeConstants";

const NAVBAR_ITEM_TASK = {
  name: "Task",
  url: "/task",
};

const NAVBAR_ITEM_EVENT = {
  name: "Event",
  url: "/event",
};

const NAVBAR_ITEM_ANNOUNCEMENT = {
  name: "Announcement",
  url: "/announcement",
};

const NAVBAR_ITEM_FRIENDS = {
  name: "Friends",
  url: "/friends",
};

const NAVBAR_ITEM_PUZZLE = {
  name: "Puzzle",
  url: ROUTE_PUZZLE,
};

const NAVBAR_ITEM_MORE = {
  name: "More",
  url: "/#",
};

const NAVBAR_ITEM_ABOUT = {
  name: "About",
  url: ROUTE_ABOUT,
};

export const NAVBAR_ITEM_PROFILE = {
  name: "Profile",
  url: "/profile",
};

export const NAVBAR_ITEM_STATISTICS = {
  name: "Statistics",
  url: "/statistics",
};

export const NAVBAR_ITEM_GALLERY = {
  name: "Gallery",
  url: "/gallery",
};

export const NAVBAR_ITEMS_MABA = [
  NAVBAR_ITEM_TASK,
  NAVBAR_ITEM_EVENT,
  NAVBAR_ITEM_ANNOUNCEMENT,
  NAVBAR_ITEM_FRIENDS,
  NAVBAR_ITEM_PUZZLE,
  NAVBAR_ITEM_MORE,
  NAVBAR_ITEM_ABOUT,
];

export const NAVBARS_ITEMS_NON_MABA = [
  NAVBAR_ITEM_EVENT,
  NAVBAR_ITEM_ANNOUNCEMENT,
  NAVBAR_ITEM_FRIENDS,
  NAVBAR_ITEM_MORE,
  NAVBAR_ITEM_ABOUT,
];

export const NAVBAR_ITEMS_MOBILE_MABA = [
  NAVBAR_ITEM_PROFILE,
  NAVBAR_ITEM_TASK,
  NAVBAR_ITEM_EVENT,
  NAVBAR_ITEM_ANNOUNCEMENT,
  NAVBAR_ITEM_FRIENDS,
  NAVBAR_ITEM_PUZZLE,
  NAVBAR_ITEM_STATISTICS,
  NAVBAR_ITEM_GALLERY,
  NAVBAR_ITEM_ABOUT,
];

export const NAVBAR_ITEMS_MOBILE_NON_MABA = [
  NAVBAR_ITEM_PROFILE,
  NAVBAR_ITEM_EVENT,
  NAVBAR_ITEM_ANNOUNCEMENT,
  NAVBAR_ITEM_FRIENDS,
  NAVBAR_ITEM_STATISTICS,
  NAVBAR_ITEM_GALLERY,
  NAVBAR_ITEM_ABOUT,
];
