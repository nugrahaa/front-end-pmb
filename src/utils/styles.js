// Styling Utility
// How to use:
// Import it in your component file
// Use the spread operator to inject the desired style to your component's style tag

// e.g. div with a horizontal padding of 24 and a margin top of 40
// <div style={{...styles.phxl, ...styles.mtxxxl}} >...</div>
const styles = {
  // [2,4,8,12,16,24,32,40,48]
  // [xxs,xs,s,m,l,xl,xxl,xxxl,xxxxl]

  // -------- paddings

  //   padding all
  paxxs: { padding: 2 },
  paxs: { padding: 4 },
  pas: { padding: 8 },
  pam: { padding: 12 },
  pal: { padding: 16 },
  paxl: { padding: 24 },
  paxxl: { padding: 32 },
  paxxxl: { padding: 40 },
  paxxxxl: { padding: 48 },

  // padding left
  plxxs: { paddingLeft: 2 },
  plxs: { paddingLeft: 4 },
  pls: { paddingLeft: 8 },
  plm: { paddingLeft: 12 },
  pll: { paddingLeft: 16 },
  plxl: { paddingLeft: 24 },
  plxxl: { paddingLeft: 32 },
  plxxxl: { paddingLeft: 40 },
  plxxxxl: { paddingLeft: 48 },

  //   padding right
  prxxs: { paddingRight: 2 },
  prxs: { paddingRight: 4 },
  prs: { paddingRight: 8 },
  prm: { paddingRight: 12 },
  prl: { paddingRight: 16 },
  prxl: { paddingRight: 24 },
  prxxl: { paddingRight: 32 },
  prxxxl: { paddingRight: 40 },
  prxxxxl: { paddingRight: 48 },

  //   padding top
  ptxxs: { paddingTop: 2 },
  ptxs: { paddingTop: 4 },
  pts: { paddingTop: 8 },
  ptm: { paddingTop: 12 },
  ptl: { paddingTop: 16 },
  ptxl: { paddingTop: 24 },
  ptxxl: { paddingTop: 32 },
  ptxxxl: { paddingTop: 40 },
  ptxxxxl: { paddingTop: 48 },

  //   padding bottom
  pbxxs: { paddingBottom: 2 },
  pbxs: { paddingBottom: 4 },
  pbs: { paddingBottom: 8 },
  pbm: { paddingBottom: 12 },
  pbl: { paddingBottom: 16 },
  pbxl: { paddingBottom: 24 },
  pbxxl: { paddingBottom: 32 },
  pbxxxl: { paddingBottom: 40 },
  pbxxxxl: { paddingBottom: 48 },

  //   padding vertical
  pvxxs: { paddingBottom: 2, paddingTop: 2 },
  pvxs: { paddingBottom: 4, paddingTop: 4 },
  pvs: { paddingBottom: 8, paddingTop: 8 },
  pvm: { paddingBottom: 12, paddingTop: 12 },
  pvl: { paddingBottom: 16, paddingTop: 16 },
  pvxl: { paddingBottom: 24, paddingTop: 24 },
  pvxxl: { paddingBottom: 32, paddingTop: 32 },
  pvxxxl: { paddingBottom: 40, paddingTop: 40 },
  pvxxxxl: { paddingBottom: 48, paddingTop: 48 },

  //   padding horizontal
  phxxs: { paddingLeft: 2, paddingRight: 2 },
  phxs: { paddingLeft: 4, paddingRight: 4 },
  phs: { paddingLeft: 8, paddingRight: 8 },
  phm: { paddingLeft: 12, paddingRight: 12 },
  phl: { paddingLeft: 16, paddingRight: 16 },
  phxl: { paddingLeft: 24, paddingRight: 24 },
  phxxl: { paddingLeft: 32, paddingRight: 32 },
  phxxxl: { paddingLeft: 40, paddingRight: 40 },
  phxxxxl: { paddingLeft: 48, paddingRight: 48 },

  //   -------- margins

  //   margin all
  maxxs: { margin: 2 },
  maxs: { margin: 4 },
  mas: { margin: 8 },
  mam: { margin: 12 },
  mal: { margin: 16 },
  maxl: { margin: 24 },
  maxxl: { margin: 32 },
  maxxxl: { margin: 40 },
  maxxxxl: { margin: 48 },

  //   margin left
  mlxxs: { marginLeft: 2 },
  mlxs: { marginLeft: 4 },
  mls: { marginLeft: 8 },
  mlm: { marginLeft: 12 },
  mll: { marginLeft: 16 },
  mlxl: { marginLeft: 24 },
  mlxxl: { marginLeft: 32 },
  mlxxxl: { marginLeft: 40 },
  mlxxxxl: { marginLeft: 48 },

  //   margin right
  mrxxs: { marginRight: 2 },
  mrxs: { marginRight: 4 },
  mrs: { marginRight: 8 },
  mrm: { marginRight: 12 },
  mrl: { marginRight: 16 },
  mrxl: { marginRight: 24 },
  mrxxl: { marginRight: 32 },
  mrxxxl: { marginRight: 40 },
  mrxxxxl: { marginRight: 48 },

  //   margin top
  mtxxs: { marginTop: 2 },
  mtxs: { marginTop: 4 },
  mts: { marginTop: 8 },
  mtm: { marginTop: 12 },
  mtl: { marginTop: 16 },
  mtxl: { marginTop: 24 },
  mtxxl: { marginTop: 32 },
  mtxxxl: { marginTop: 40 },
  mtxxxxl: { marginTop: 48 },

  //   margin bottom
  mbxxs: { marginBottom: 2 },
  mbxs: { marginBottom: 4 },
  mbs: { marginBottom: 8 },
  mbm: { marginBottom: 12 },
  mbl: { marginBottom: 16 },
  mbxl: { marginBottom: 24 },
  mbxxl: { marginBottom: 32 },
  mbxxxl: { marginBottom: 40 },
  mbxxxxl: { marginBottom: 48 },

  //   margin vertical
  mvxxs: { marginBottom: 2, marginTop: 2 },
  mvxs: { marginBottom: 4, marginTop: 4 },
  mvs: { marginBottom: 8, marginTop: 8 },
  mvm: { marginBottom: 12, marginTop: 12 },
  mvl: { marginBottom: 16, marginTop: 16 },
  mvxl: { marginBottom: 24, marginTop: 24 },
  mvxxl: { marginBottom: 32, marginTop: 32 },
  mvxxxl: { marginBottom: 40, marginTop: 40 },
  mvxxxxl: { marginBottom: 48, marginTop: 48 },

  //   margin horizontal
  mhxxs: { marginLeft: 2, marginRight: 2 },
  mhxs: { marginLeft: 4, marginRight: 4 },
  mhs: { marginLeft: 8, marginRight: 8 },
  mhm: { marginLeft: 12, marginRight: 12 },
  mhl: { marginLeft: 16, marginRight: 16 },
  mhxl: { marginLeft: 24, marginRight: 24 },
  mhxxl: { marginLeft: 32, marginRight: 32 },
  mhxxxl: { marginLeft: 40, marginRight: 40 },
  mhxxxxl: { marginLeft: 48, marginRight: 48 },

  // border radius
  brs: { borderRadius: 5 },
  brm: { borderRadius: 10 },
  brl: { borderRadius: 15 },

  // flexes
  flexMiddle: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },

  flexDirCol: {
    display: "flex",
    flexDirection: "column",
  },

  flexDirRow: {
    display: "flex",
    flexDirection: "row",
  },

  jcc: {
    justifyContent: "center",
  },
  aic: {
    alignItems: "center",
  },

  // layout
  fullScreen: {
    width: "100vw",
    height: "100vh",
  },

  // typography
  fwb: {
    fontWeight: "bold",
  },
  fwr: {
    fontWeight: "regular",
  },
  fcb: {
    color: "black",
  },
  fcw: {
    color: "white",
  },
  tac: {
    textAlign: "center",
  },

  // drop shadow
  shadow: {
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
  },

  fsu: {
    fontStyle: "underline",
  },

  w100: {
    width: "100%",
  },

  fcd: {
    color: "red",
  },

  bgp: {
    backgroundColor: "#4A69CA",
  },
};

export default styles;
