import auth from "./auth";
import axios from "axios";

let MAIN_URL = "https://pmb-staging.cahyanugraha12.site";

// if (process.env.REACT_APP_NODE_ENV === "production") {
//   MAIN_URL = "https://pmb.cahyanugraha12.site";
// }
let API_PREFIX_URL = `${MAIN_URL}/api`;

axios.interceptors.request.use(
  (config) => {
    const user = auth.loggedIn();

    if (user) {
      const token = user;
      if (token) {
        config.headers.Authorization = `JWT ${token}`;
      }
    }

    return config;
  },
  (error) => Promise.reject(error)
);

export const loggingInApi = `${API_PREFIX_URL}/account/login`;
export const loggingOutApi = `${API_PREFIX_URL}/account/logout`;
export const fetchProfileApi = `${API_PREFIX_URL}/account/profile/me`;
export const postEditProfileApi = `${API_PREFIX_URL}/account/profile/me`;
export const uploadPictureApi = `${API_PREFIX_URL}/account/upload-photo`;
export const fetchEventsApi = `${API_PREFIX_URL}/website/events`;
export const fetchEventsDetailApi = (id) =>
  `${API_PREFIX_URL}/website/events/${id}`;
export const fetchAnnouncementsApi = `${API_PREFIX_URL}/website/announcements`;
export const fetchPuzzlesApi = `${API_PREFIX_URL}/website/puzzles`;
export const postPuzzlesApi = (id) => `${API_PREFIX_URL}/website/puzzles/${id}`;
export const fetchTokenApi = (count) =>
  `${API_PREFIX_URL}/friends/token?count=${count}`;
export const checkTokenApi = `${API_PREFIX_URL}/friends/token`;
export const postTokenApi = `${API_PREFIX_URL}/friends/kenalan`;
export const getKenalanByIdApi = (id) =>
  `${API_PREFIX_URL}/friends/kenalan/${id}`;
export const patchKenalanByIdApi = (id) =>
  `${API_PREFIX_URL}/friends/kenalan/${id}`;
export const findMahasiswaApi = (query) =>
  `${API_PREFIX_URL}/friends/find-friends?query=${query}`;
export const fetchFriendListApi = `${API_PREFIX_URL}/friends/kenalan`;
export const fetchKenalanStatisticsApi = `${API_PREFIX_URL}/friends/kenalan-user-statistics`;

// export const fetchPuzzleQuestionApi = `${API_PREFIX_URL}/api​/website​/puzzles/`
export const fetchGroupApi = `${API_PREFIX_URL}/akun/kelompok`;

export const fetchAnnouncementDetailApi = (id) =>
  `${API_PREFIX_URL}/situs/announcements/${id}`;
export const fetchServerTime = `${API_PREFIX_URL}/website/server-time`;

export const getTaskListApi = `${API_PREFIX_URL}/website/tasks`;
export function getTaskDetail(id) {
  return `${API_PREFIX_URL}/website/tasks/${id}`;
}
export function taskSubmissionsApi(id) {
  return `${API_PREFIX_URL}/website/tasks/${id}/submissions`;
}
export const uploadFileApi = `${API_PREFIX_URL}/website/upload-file`;

export const getFeedbacks = `${API_PREFIX_URL}/situs/feedbacks`;
export const postCommentApi = `${API_PREFIX_URL}/situs/qnas`;
export const fetchFriendApi = (id) => `${API_PREFIX_URL}/teman/kenalan/${id}`;
export const fetchInterestApi = (id) =>
  `${API_PREFIX_URL}/akun/interests?user=${id}`;
export const fetchStatisticKenalanApi = `${API_PREFIX_URL}/teman/kenalan-user-statistics`;

export const editKenalanApi = (id) => `${API_PREFIX_URL}/teman/kenalan/${id}`;

export const interestApi = `${API_PREFIX_URL}/akun/interests`;
export function getUserInterestApi(id) {
  return `${API_PREFIX_URL}/akun/interests?user=${id}`;
}
export function deleteInterestApi(id) {
  return `${API_PREFIX_URL}/akun/interests/${id}`;
}
export function patchInterestApi(id) {
  return `${API_PREFIX_URL}/akun/interests/${id}`;
}
export const generateTokenApi = (limit) =>
  `${API_PREFIX_URL}/teman/token?count=${limit}`;
export const postKenalanApi = `${API_PREFIX_URL}/teman/kenalan`;
export const deleteKenalanApi = (id) => `${API_PREFIX_URL}/teman/kenalan/${id}`;

export const getRecentStories = `${API_PREFIX_URL}/situs/stories`;
export const getMyStories = `${API_PREFIX_URL}/situs/stories?user=me`;
export const fetchStory = `${API_PREFIX_URL}/situs/stories`;
export const editStoryApi = (id) => `${API_PREFIX_URL}/situs/stories/${id}`;
export const deleteStoryApi = (id) => `${API_PREFIX_URL}/situs/stories/${id}`;
export const postInterestApi = `${API_PREFIX_URL}/account/interests/me`;
export const getMediaRootApi = `${MAIN_URL}`;
export const getMediaUrl = (suffix) => `${getMediaRootApi}${suffix}`;

// export const fetchAnnouncementDetailApi = (id) => `${API_PREFIX_URL}/situs/announcements/${id}`;
// export function getTaskApi(id) {
//   return `${API_PREFIX_URL}/situs/tasks/${id}`;
// }
// export function taskSubmissionsApi(id) {
//   return `${API_PREFIX_URL}/situs/tasks/${id}/submissions`;
// }
// export const uploadApi = `${API_PREFIX_URL}/situs/upload-file`;

// export const getFeedbacks = `${API_PREFIX_URL}/situs/feedbacks`;
// export const postCommentApi = `${API_PREFIX_URL}/situs/qnas`;

// export const fetchFriendApi = (id) => `${API_PREFIX_URL}/teman/kenalan/${id}`;
// export const fetchInterestApi = (id) => `${API_PREFIX_URL}/akun/interests?user=${id}`;
// export const fetchStatisticKenalanApi = `${API_PREFIX_URL}/teman/kenalan-user-statistics`;

// export const editKenalanApi = (id) => `${API_PREFIX_URL}/teman/kenalan/${id}`;

// export const interestApi = `${API_PREFIX_URL}/akun/interests`;
// export function getUserInterestApi(id) {
//   return `${API_PREFIX_URL}/akun/interests?user=${id}`;
// }
// export function deleteInterestApi(id) {
//   return `${API_PREFIX_URL}/akun/interests/${id}`;
// }
// export function patchInterestApi(id) {
//   return `${API_PREFIX_URL}/akun/interests/${id}`;
// }
// export const generateTokenApi = (limit) => `${API_PREFIX_URL}/teman/token?count=${limit}`;
// export const postKenalanApi = `${API_PREFIX_URL}/teman/kenalan`;
// export const deleteKenalanApi = (id) => `${API_PREFIX_URL}/teman/kenalan/${id}`;

// export const getRecentStories = `${API_PREFIX_URL}/situs/stories`;
// export const getMyStories = `${API_PREFIX_URL}/situs/stories?user=me`;
// export const fetchStory = `${API_PREFIX_URL}/situs/stories`;
// export const editStoryApi = (id) => `${API_PREFIX_URL}/situs/stories/${id}`;
// export const deleteStoryApi = (id) => `${API_PREFIX_URL}/situs/stories/${id}`;
