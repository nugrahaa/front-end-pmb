/*
 *
 * Global constants
 *
 */

export const SET_AUTH = "src/SET_AUTH";
export const SET_USER = "src/SET_USER";
export const SET_USER_SUCCESS = "src/SET_USER_SUCCESS";
export const SET_USER_FAILURE = "src/SET_USER_FAILURE"

export const SET_SERVER_TIME = "src/SET_SERVER_TIME";
export const SET_USER_ACCOUNT = "src/SET_USER_ACCOUNT";

export const SENDING_REQUEST = "src/SENDING_REQUEST";
export const REQUEST_ERROR = "src/REQUEST_ERROR";
export const CLEAR_ERROR = "src/CLEAR_ERROR";
