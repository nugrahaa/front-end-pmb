export const isLoggedIn = (state) => state.global.get("loggedIn");
export const getUser = (state) => state.global.get("user");
export const isLoading = (state) => state.global.get("isLoading");
export const isMaba = (state) => {
  if (getUser(state).profile?.is_maba === undefined) {
    return "fetching";
  } else {
    return getUser(state).profile.is_maba === true;
  }
};
export const getTime = (state) => state.global.get("serverTime");

export const isLoadingServerTime = (state) =>
  state.global.get("currentlySending");
