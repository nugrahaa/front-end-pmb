export const getInternalEvents = (state) => state.eventReducer.get("internalEvents");
export const getExternalEvents = (state) => state.eventReducer.get("externalEvents");
export const getAllEvents = (state) => state.eventReducer.get("allEvents");
export const isEventsLoaded = (state) => state.eventReducer.get("isLoaded");
export const isEventsLoading = (state) => state.eventReducer.get("isLoading");
