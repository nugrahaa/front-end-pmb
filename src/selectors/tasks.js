export const isTasksLoading = state => state.taskPage.get("isLoading");

export const isTasksLoaded = state => state.taskPage.get("isLoaded");

export const getTasks = state => state.taskPage.get("tasks");

export const error = state => state.taskPage.get("error");
