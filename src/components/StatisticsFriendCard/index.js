/* eslint-disable no-confusing-arrow */
import React, { Component } from "react";
import PropTypes from "prop-types";
import Card from "../../components/Card";
import { StatisticsCardStyle, Progress, ProgressAngkatan } from "./style";

class StatisticsCard extends Component {

  render() {
    const {
      kenalan_approved_maung,
      kenalan_approved_quanta,
      kenalan_approved_tarung,
      kenalan_approved_elemen,
     } = this.props.stats[0]
    const {
      kenalan_created_maung,
      kenalan_created_quanta,
      kenalan_created_tarung,
      kenalan_created_elemen
    } = this.props.stats[0]
    const {
    required_maung,
    required_quanta,
    required_tarung,
    required_elemen,
   } = this.props.stats[0].kenalan_task

   const percentageMaung = Math.round((kenalan_approved_maung * 100) / required_maung)
   const percentageQuanta = Math.round((kenalan_approved_quanta * 100) / required_quanta)
   const percentageTarung = Math.round((kenalan_approved_tarung * 100) / required_tarung)
   const percentageAlumni = Math.round((kenalan_approved_elemen * 100) / required_elemen)

    return (
      <StatisticsCardStyle>
        {this.props.title !== null ? (
          <Card>
            <h3 class="title">Mengenal Angkatan</h3>
            <div className="maung progress">
              <h3>
                Progress Maung
                <AngkatanData
                  angkatanApproved={kenalan_approved_maung}
                  angkatanTotal={kenalan_created_maung}
                  angkatanRequirement={required_maung}
                />
                <span className="percentage"> {percentageMaung}% </span>
              </h3>
              <ProgressBar percentage={percentageMaung} />
            </div>
            <div className="quanta progress">
              <h3>
                Progress Quanta
                <AngkatanData
                  angkatanApproved={kenalan_approved_quanta}
                  angkatanTotal={kenalan_created_quanta}
                  angkatanRequirement={required_quanta}
                />
                <span className="percentage"> {percentageQuanta}% </span>
              </h3>
              <ProgressBar percentage={percentageQuanta} />
            </div>
            <div className="tarung progress">
              <h3>
                Progress Tarung
                <AngkatanData
                  angkatanApproved={kenalan_approved_tarung}
                  angkatanTotal={kenalan_created_tarung}
                  angkatanRequirement={required_tarung}
                />
                <span className="percentage"> {percentageTarung}% </span>
              </h3>
              <ProgressBar percentage={percentageTarung} />
            </div>
            <div className="alumni progress" style={{ paddingBottom: "2em" }}>
              <h3>
                Progress Alumni
                <AngkatanData
                  angkatanApproved={kenalan_approved_elemen}
                  angkatanTotal={kenalan_created_elemen}
                  angkatanRequirement={required_elemen}
                />
                <span className="percentage"> {percentageAlumni}% </span>
              </h3>
              <ProgressBar percentage={percentageAlumni} />
            </div>
          </Card>
        ) : (
          <></>
        )}
      </StatisticsCardStyle>
    );
  }
}

const AngkatanData = (props) => (
  <ProgressAngkatan>
    <div>
      ({props.angkatanApproved} approved / {props.angkatanTotal} Total /{" "}
      {props.angkatanRequirement} required)
    </div>
  </ProgressAngkatan>
);

const ProgressBar = (props) => (
  <Progress>
    <div className="progress-bar" />
    <div
      className="filler"
      style={{
        width: props.percentage <= 100 ? `${props.percentage}%` : "100%",
      }}
    />
  </Progress>
);


// StatisticsCard.defaultProps = defaultProp;
AngkatanData.propTypes = {
  angkatanApproved: PropTypes.number,
  angkatanRequirement: PropTypes.number,
  angkatanTotal: PropTypes.number,
};

ProgressBar.propTypes = {
  percentage: PropTypes.number,
};

export default StatisticsCard;
