import styled from "styled-components";

export const StatisticsCardStyle = styled.div`
  .Card {
    padding-bottom: 2rem;
  }

  .title {
    font-family: Metropolis;
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 16px;

    color: #4a69ca;
    padding: 1em 2em 0.5em;
  }

  .progress {
    padding: 0rem 2em 0rem 2em;
    h3 {
      font-family: "Metropolis";
      margin-block-start: 0.75rem;
      margin-block-end: 0.75rem;
      font-style: normal;
      font-weight: normal;
      font-size: 14px;
      line-height: 16px;
      color: #1b274b;
    }
  }

  .percentage {
    margin-left: 2em;
    color: #1b274b;
    float: right;
    font-size: 14px;
  }

  @media (min-width: 1025px) and (max-width: 1349px) {
    .progress {
      h3 {
        font-size: 14px;
      }
    }
  }

  @media (max-width: 64rem) {
    br {
      display: block;
    }

    span {
      display: block;
      font-size: 0.75rem;
      padding: 0.5rem 0 1rem 0;
      text-align: center;
    }
    .progress {
      h3 {
        font-size: 12px;
      }
    }
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    .progress {
      padding: 0rem 2em 0rem;
      h3 {
        font-size: 14px;
      }
    }
    .title {
      font-size: 16px;
      padding: 1em 2em 0.5em;
    }
    .percentage {
      margin-top: -2rem;
    }
  }

  @media (min-width: 480px) and (max-width: 768px) {
    .progress {
      padding: 0rem 2em 0rem;
      h3 {
        font-size: 14px;
      }
    }
    .title {
      font-size: 16px;
      padding: 1em 2em 0.5em;
    }
    .percentage {
      margin-top: -2rem;
    }
  }

  @media (min-width: 376px) and (max-width: 480px) {
    .Card {
      width: 100%;
    }

    .progress {
      display: flex;
      flex-direction: column;
      padding: 0rem 1em 0rem;
      h3 {
        font-size: 14px;
      }
    }

    .title {
      font-size: 16px;
      padding: 1em 1em 0.5em;
    }

    .percentage {
      margin-top: -2rem;
    }
  }

  @media (max-width: 376px) {
    .Card {
      width: 100%;
    }

    .progress {
      padding: 0rem 1em 0rem;
      h3 {
        font-size: 14px;
      }
    }

    .title {
      font-size: 16px;
      padding: 1em 1em 0.5em;
    }

    .percentage {
      margin-top: -2rem;
      font-size: 12px;
      max-width: 100%;
    }
  }
`;
export const Progress = styled.div`
  .progress-bar {
    height: 0.5rem;
    width: 100%;
    border-radius: 50px;
    background: #c4c4c4;
  }

  .filler {
    background: #4a69ca;
    height: 1rem;
    border-radius: 50px;
    margin-top: -0.75em;
  }

  @media (min-width: 1025px) and (max-width: 1349px) {
    .progress-bar {
      height: 0.5rem;
    }
  }

  @media (max-width: 64rem) {
    .progress-bar {
      height: 0.4rem;
    }
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    .progress-bar {
      margin-top: -1rem;
    }
  }

  @media (min-width: 480px) and (max-width: 768px) {
    .progress-bar {
      margin-top: -1rem;
    }
  }

  @media (min-width: 376px) and (max-width: 480px) {
    .progress-bar {
      margin-top: -1rem;
    }
  }

  @media (max-width: 376px) {
    .progress-bar {
      margin-top: -1rem;
    }
  }
`;

export const ProgressAngkatan = styled.span`
  div {
    font-size: 1rem;
    color: grey;
    display: inline;
    margin-left: 1em;
  }

  @media (min-width: 1350px) {
    div {
      font-size: 0.75rem;
    }
  }

  @media (min-width: 1025px) and (max-width: 1349px) {
    div {
      font-size: 0.75rem;
    }
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    div {
      font-size: 12px;
      display: block;
      margin-top: -2em;
    }
  }

  @media (min-width: 480px) and (max-width: 768px) {
    div {
      display: block;
      margin-top: -1.5rem;
      font-size: 12px;
      margin-left: 4rem;
    }
  }

  @media (min-width: 376px) and (max-width: 480px) {
    div {
      display: block;
      margin-top: -1.5rem;
      font-size: 10px;
      margin-left: 4rem;
    }
  }

  @media (max-width: 376px) {
    div {
      display: block;
      margin-top: -1.5rem;
      font-size: 10px;
      margin-left: 4rem;
    }
  }
`;
