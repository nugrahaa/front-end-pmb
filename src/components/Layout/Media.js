// Media Utility Component
// A media query component that can be wrapped around a component and act as a breakpoint.
// Breakpoints are taken from Bootstrap's responsive media queries.
//
// Usecase:
// Let's say we want to render hello world in mobile devices only,
// the code would be like this:
// ...
// <Media>
//   {matches => (
//     {matches.mobile ? (<h1>Hello World</h1>) : (<h1>Not in mobile breakpoint</h1>)}
//   )}
// </Media>
// ...
//
// Jonathan Filbert and Ristek Webdev 2020
import React from "react";
import ReactMedia from "react-media";

export const Media = (props) => {
  return (
    <ReactMedia
      queries={{
        xs: "(max-width: 575px)",
        xxs: "(max-width: 350px)",
        sm: "(min-width: 576px) and (max-width: 767px)",
        md: "(min-width: 768px) and (max-width: 991px)",
        lg: "(min-width: 992px) and (max-width: 1199px)",
        xl: "(min-width: 1200px)",
        mobile: "(max-width: 991px)",
      }}
      {...props}
    />
  );
};
