/* eslint-disable array-callback-return */
const LAYOUT_DICT = {
  fullScreen: ["/login"],
  hasRightColumn: ["/", "/dashboard", "/event", "/dummy", "/search"],
};

const getLayout = (url) => {
  let layoutResult = "twoColumns";
  Object.keys(LAYOUT_DICT).map((key) => {
    LAYOUT_DICT[key].map((route) => {
      const locationRegexResult = url.match(route);
      const result = locationRegexResult && locationRegexResult[0] === url;
      if (route === url || result) {
        layoutResult = key;
      }
    });
  });
  return layoutResult;
};

export default getLayout;
