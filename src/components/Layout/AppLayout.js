import React, { useEffect, useState } from "react";
/* eslint-disable react-hooks/exhaustive-deps */
import { useLocation, Redirect } from "react-router-dom";
import { isEqual } from "lodash";
import moment from "moment-timezone";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";

import { Media } from "./Media";
import styles from "../../utils/styles";
import Loading from "../Loading";
import Navbar from "../Navbar";
import TokenInput from "../../container/TokenInput";
import TokenGenerator from "../../container/TokenGenerator";

import getLayout from "./layoutDictionary";
import CalendarCard from "../CalendarCard";
import { getAllEvents } from "../../selectors/events";
import { getTasks } from "../../selectors/tasks";
import { isMaba, getUser, isLoading } from "../../selectors/user";
import { fetchEvent } from "../../container/EventPage/actions";
import { fetchTasksList } from "../../container/TaskListPage/actions";
import { setUser } from "../../globalActions";
// import EmptyCard from "../../components/EmptyCard";
import footerDesktop from "./assets/footerDesktop.png";
import footerMobile from "./assets/footerMobile.png";

const FooterContainer = styled.div`
  .footer-background {
    width: 100%;
    height: 220px;
    background-image: url(${footerDesktop});
    background-repeat: no-repeat;
    background-position: center bottom;
    background-size: cover;
    z-index: 10;

    @media (max-width: 750px) {
      height: 100px;
      background-image: url(${footerMobile});
    }
  }
`;

const DummyFooterComponent = () => (
  <div>
    <FooterContainer>
      <div className="footer-background" />
    </FooterContainer>
  </div>
);

//reference PMB 2019
//eventMap with date as key and event as value
const getActivityCalendarMap = (
  activityMap = new Map(),
  activityArr = [],
  dateStringGetter,
  linkPrefix,
  activityType
) => {
  const getActivityDetail = (
    activity,
    dateStringGetter,
    linkPrefix,
    activityType
  ) => ({
    title: activity.title,
    date: dateStringGetter(activity),
    link: `${linkPrefix}/${activity.id}`,
    activityType: activityType,
  });

  const getActivityKey = (dateString, activityType) => {
    return `${moment(dateString).startOf("day").unix()};${activityType}`;
  };

  activityArr.forEach((activity) => {
    const key = getActivityKey(dateStringGetter(activity), activityType);
    const activityDetail = getActivityDetail(
      activity,
      dateStringGetter,
      linkPrefix,
      activityType
    );
    !activityMap.has(key)
      ? activityMap.set(key, [activityDetail])
      : activityMap.get(key).push(activityDetail);
  });
};

const AppLayout = ({ children }) => {
  const location = useLocation();
  const layout = getLayout(location.pathname);
  const isFullScreen = isEqual(layout, "fullScreen");
  const hasRightColumn = isEqual(layout, "hasRightColumn");

  const user = useSelector((state) => getUser(state));
  const events = useSelector((state) => getAllEvents(state));
  const tasks = useSelector((state) => getTasks(state));
  const maba = useSelector((state) => isMaba(state));
  const loading = useSelector((state) => isLoading(state));
  const [calendarActivity, setCalendarActivity] = useState(new Map());
  // const loading = useSelector((state) => isLoading(state));

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchEvent());
    dispatch(fetchTasksList());
    dispatch(setUser());
  }, []);

  useEffect(() => {
    const calendarActivity = new Map();

    getActivityCalendarMap(
      calendarActivity,
      events,
      (event) => event.date,
      "/event",
      "event"
    );

    getActivityCalendarMap(
      calendarActivity,
      tasks,
      (task) => task.end_time,
      "/task",
      "task"
    );

    setCalendarActivity(calendarActivity);
  }, [tasks, events]);

  return (
    <>
      {location.pathname !== "/edit-profile" &&
      !user &&
      (!user.birth_place ||
        !user.birth_date ||
        !user.high_school ||
        !user.line_id ||
        (maba && user.kelompok === null)) ? (
        <>
          <Redirect to="/edit-profile" />
        </>
      ) : (
        <Media>
          {(matches) => (
            <div>
              {/* main app excluding footer */}
              <div
                style={{
                  paddingLeft: !isFullScreen && 18,
                  paddingRight: !isFullScreen && 18,
                  marginBottom: !isFullScreen && 18,
                  minHeight: !isFullScreen && "100vh",
                }}
              >
                {/* start navbar */}
                {!isFullScreen && (
                  <div
                    style={{
                      ...styles.mbxl,
                    }}
                  >
                    <Navbar />
                  </div>
                )}
                {/* end navbar */}
                {/* start app  */}
                <div
                  style={{
                    display: isFullScreen ? "block" : "grid",
                    gridTemplateColumns: matches.mobile
                      ? "1fr"
                      : "1fr 3.5fr 1fr",
                    gridTemplateRows: matches.mobile ? "auto auto auto" : "1fr",
                    gridTemplateAreas: !matches.mobile
                      ? `'left content right'`
                      : hasRightColumn
                      ? `'right' 'content' 'left'`
                      : `'content' 'left' 'right' `,
                    marginBottom: !isFullScreen && 32,
                  }}
                >
                  {/* start left column */}
                  {!isFullScreen && (
                    <div
                      style={{
                        gridArea: "left",
                        marginTop: matches.mobile && 32,
                      }}
                    >
                      <CalendarCard activities={calendarActivity} />
                    </div>
                  )}
                  {/* end left column */}
                  {/* start center column */}
                  <div
                    style={{
                      paddingLeft: !isFullScreen && !matches.mobile && 32,
                      paddingRight: !isFullScreen && !matches.mobile && 32,
                      gridArea: "content",
                    }}
                  >
                    {children}
                  </div>
                  {/* end center column */}
                  {/* start right column */}
                  {hasRightColumn && (
                    <div
                      style={{
                        gridArea: "right",
                        marginBottom: matches.mobile && 24,
                      }}
                    >
                      {/* {
                    !loading && (<>
                      {!maba ? <TokenGenerator /> : <TokenInput />}
                    </>)
                  } */}
                      {loading
                          ? <Loading />
                          : (!maba
                            ? <TokenGenerator />
                            : <TokenInput />)}
                      {/* <h2>Add Friends</h2>
                      <EmptyCard text="Coming Soon" /> */}
                    </div>
                  )}
                  {/* end right column */}
                </div>
                {/* end app */}
                {/* start footer */}
              </div>
              {!isFullScreen && (
                <div>
                  <DummyFooterComponent />
                </div>
              )}
              {/* end footer */}
            </div>
          )}
        </Media>
      )}
    </>
  );
};

export default AppLayout;
