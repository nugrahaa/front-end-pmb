import React, { useState, useEffect, useRef } from 'react';
import {useSelector} from 'react-redux';
import {
  TimerWrapper
} from './style'
import moment from "moment-timezone";
import { getTime } from "../../selectors/user";

const Timer = ({ start, handleClose, endDate }) => {

  const [time, setTime] = useState({
    days:0,
    hours:0,
    minutes: 0,
    seconds: 0,
  });

  const timer= useRef(null);
  const serverTime = useSelector((state) => getTime(state));

  useEffect(() => {

    const setCountDown = (endDate) => {
        var startTime = moment(serverTime);
        var endTime = moment(endDate)
        startTime.add(1.1, "second");

      timer.current = setInterval(() => {

        startTime.add(1, "second");
        var days = endTime.diff(startTime, 'days')
        var hours = endTime.diff(startTime, 'hours')
        var minutes = endTime.diff(startTime, 'minutes')
        var seconds = endTime.diff(startTime, 'seconds') % 60

        if (days <= 0 && hours <= 0 && minutes <= 0 && seconds <= 0) {

          clearInterval(timer.current);
          setTime({
            days:0,
            hours:0,
            minutes: 0,
            seconds: 0,
          });
          handleClose(); /* eslint-disable */

        } else {

          setTime({
            days:days,
            hours:hours,
            minutes: minutes,
            seconds: seconds,
          });

        }
      }, 1000);
    };

    if (start) {

        if(endDate !== undefined && endDate !== "" ){

            setCountDown(endDate)
            return ()=>{

              clearInterval(timer.current);
            }
        }

    } else {

      setTime({
        days:0,
        hours:0,
        minutes: 0,
        seconds: 0,
      });

      clearInterval(timer.current);
    }
  }, [start, endDate]);

  return (
    <TimerWrapper>
      <h5>
         <span>Timer </span>: {time.minutes}m{" "}
        {time.seconds < 10 ? `0${time.seconds}` : time.seconds}s
      </h5>
    </TimerWrapper>
  );
};

export default Timer;