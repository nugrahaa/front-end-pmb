import React from "react";

import { LoadingContainer } from "./style";

function Loading(props) {
  return (
    <LoadingContainer>
      <div className="spinner" />
    </LoadingContainer>
  );
}

export default Loading;
