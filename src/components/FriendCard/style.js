import styled from "styled-components";

export const FriendCardStyle = styled.div`
    width: 100%;
    .FriendCard {
        width: 100%;

        background-color: #ffffff;
        mix-blend-mode: normal;
        box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
        border-radius: 10px;

        display: grid;

        margin: 25px auto;
    }

    .FriendCard_photo_and_profile_section {
        display: grid;
        grid-template-columns: 25% 75%;
        grid-gap: 10px;
    }

    .FriendCard_photo_section {
        display: flex;
        grid-row-start: 1;
        grid-row-end: 3;
    }

    .FriendCard_photo {
        border-radius: 50%;
        width: 110px;
        height: 110px;

        border: 1px solid #1b274b;

        box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);

        background-image: url(${(props) => props.profileImage});
        background-size: cover;
        background-position: center;

        margin: 30px 25px 25px 25px;
    }

    .FriendCard_status_edit_button {
        display: flex;
        flex-direction: row;
        align-items: center;
    }

    .FriendCard_name {
        font-family: Metropolis;
        font-style: normal;
        font-weight: bold;
        font-size: 18px;
        line-height: 16px;

        color: #1b274b;

        margin-top: 30px;
    }

    .FriendCard_edit_button {
        margin-left: 25px;
    }

    .FriendCard_status {
        font-family: Metropolis;
        font-style: normal;
        font-weight: bold;
        font-size: 14px;
        line-height: 16px;
    }

    .FriendCard_detail_section {
        display: grid;
        grid-template-columns: 1fr 1fr 1fr;

        .FriendCard_detail {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: clip;
        }

        p {
            width: 150px;
            white-space: wrap;
            overflow: hidden;
            text-overflow: clip;
        }
    }

    .FriendCard_detail_section > div {
        font-family: Metropolis;
        font-style: normal;
        font-weight: normal;
        font-size: 11px;
        line-height: 10px;

        color: #1b274b;

        display: flex;
    }

    .FriendCard_interest_section {
        display: grid;
        grid-template-columns: 1fr 1fr 1fr 1fr;
        grid-column-gap: 10px;
        grid-row-gap: 10px;
        margin-left: 25px;
    }

    .FriendCard_interest_title {
        font-family: Metropolis;
        font-style: normal;
        font-weight: bold;
        font-size: 14px;
        line-height: 16px;

        text-align: justify;

        color: #1b274b;

        margin-left: 25px;
        margin-top: -5px;
    }

    .FriendCard_interest {
        background: #e7f6fd;
        border-radius: 20px;

        padding-right: 15px;
        padding-left: 15px;

        margin-right: 10px;
    }

    .FriendCard_interest p {
        font-family: Metropolis;
        font-style: normal;
        font-weight: normal;
        font-size: 12px;

        color: #000000;
    }

    .FriendCard_description_section h3 {
        font-family: Metropolis;
        font-style: normal;
        font-weight: bold;
        font-size: 14px;
        line-height: 16px;

        text-align: justify;

        color: #1b274b;

        margin-left: 25px;
    }

    .FriendCard_description_section p {
        font-family: Metropolis;
        font-style: normal;
        font-size: 12px;
        line-height: 16px;

        text-align: justify;

        color: #1b274b;

        margin: -10px 25px 25px 25px;
    }

    .FriendCard_feedback_section h3 {
        font-family: Metropolis;
        font-style: normal;
        font-weight: bold;
        font-size: 14px;
        line-height: 16px;

        text-align: justify;

        color: #c81b1b;

        margin-left: 25px;
        margin-top: -5px;
    }

    .FriendCard_feedback_section p {
        font-family: Metropolis;
        font-style: normal;
        font-size: 12px;
        line-height: 16px;

        text-align: justify;

        color: #c81b1b;

        margin: -10px 25px 25px 25px;
    }

    @media (min-width: 1024px) and (max-width: 1279px) {
        .FriendCard {
            width: 100%;
        }

        .FriendCard_edit_button {
            margin-left: 10px;
        }

        .FriendCard_photo_section {
            margin: 0 15px;
            width: 100px;
            height: 100px;
        }

        .FriendCard_photo {
            border-radius: 50%;
            width: 100px;
            height: 100px;
            margin: 30px 0px 25px;

            display: inline-block;
            position: relative;
            overflow: hidden;
        }

        .FriendCard_profile_section {
            margin: 0 30px;
        }

        .FriendCard_status {
            margin-top: -5px;
        }

        .FriendCard_detail_section {
            display: flex;
            flex-direction: column;
        }

        .FriendCard_detail_section > div {
            margin-top: -10px;
        }

        .FriendCard_jurusan {
            display: grid;
            grid-template-column: 1fr 3fr;
        }
    }

    @media (min-width: 700px) and (max-width: 767px) {
        width: 100%;
        .FriendCard {
            width: 100%;
        }
    }

    @media (min-width: 481px) and (max-width: 699px) {
        .FriendCard {
            width: 100%;
        }

        .FriendCard_edit_button {
            margin-left: 10px;
        }

        .FriendCard_photo_section {
            margin: 0 20px;
            width: 125px;
            height: 125px;
        }

        .FriendCard_photo {
            border-radius: 50%;
            width: 125px;
            height: 125px;
            margin: 30px 0px 25px;

            display: inline-block;
            position: relative;
            overflow: hidden;
        }

        .FriendCard_profile_section {
            margin: 0 30px;
        }

        .FriendCard_status {
            margin-top: -5px;
        }

        .FriendCard_detail_section {
            display: flex;
            flex-direction: column;
        }

        .FriendCard_detail_section > div {
            margin-top: -10px;
        }

        .FriendCard_jurusan {
            display: grid;
            grid-template-column: 1fr 3fr;
        }
    }

    @media (min-width: 321px) and (max-width: 480px) {
        .FriendCard {
            width: 100%;
            height: auto;
        }

        .FriendCard_edit_button {
            margin-left: 10px;
            margin-bottom: 10px;
        }

        .FriendCard_photo_section {
            margin: 0 15px;
            width: 70px;
            height: 70px;
        }

        .FriendCard_photo {
            border-radius: 50%;
            width: 70px;
            height: 70px;
            margin-left: 10px;
            margin-right: -10px;

            display: inline-block;
            position: relative;
            overflow: hidden;
        }

        .FriendCard_profile_section {
            margin: 0 30px;
        }

        .FriendCard_status {
            margin-top: -5px;
        }

        .FriendCard_detail_section {
            display: grid;
            grid-template-columns: 1fr;
            /* width: 50%; */
        }

        .FriendCard_detail_section > div {
            margin-top: -10px;
        }

        .FriendCard_jurusan {
            display: grid;
            /* grid-template-column: 1fr 3fr; */
        }

        .FriendCard_interest_section {
            flex-direction: column;
            width: 50%;
        }

        .FriendCard_interest {
            width: 100%;
            margin-bottom: 1rem;
        }

        .FriendCard_interest_section {
            display: grid;
            grid-template-columns: 1fr 1fr;
            grid-column-gap: 30px;
        }

        /* .FriendCard_name {
      max-width: 5rem;
      font-size: 12px;
    } */
    }

    @media (max-width: 320px) {
        width: 100%;

        .FriendCard_photo_section {
            margin: 0 15px;
            width: 50px;
            height: 50px;
        }

        .FriendCard_photo {
            border-radius: 50%;
            width: 50px;
            height: 50px;
            margin: 30px 0px 25px;

            display: inline-block;
            position: relative;
            overflow: hidden;
        }

        .FriendCard_detail_section {
            grid-template-columns: 1fr;
        }

        .FriendCard_name {
            font-size: 15px;
        }

        .FriendCard_name_section {
            font-size: 15px;
            max-width: 5rem;
        }

        .FriendCard_interest_section {
            flex-direction: column;
        }
    }
`;
