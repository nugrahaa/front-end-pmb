import React from "react";
import PropTypes from "prop-types";

import { FriendCardStyle } from "./style";
import JurusanIcon from "./assets/jurusan_icon.svg";
import AsalSekolahIcon from "./assets/asal_sekolah_icon.svg";
import TanggalLahirIcon from "./assets/tanggal_lahir_icon.svg";
import EmailIcon from "./assets/email_icon.svg";
import WebIcon from "./assets/web_icon.svg";
import LineIcon from "./assets/line_icon.svg";

import Button from "../Button";

function FriendCard({
  friend,
  isUserMaba,
  handleApprove,
  handleReject,
  handleEdit,
}) {

  const elemen = isUserMaba ? Object.values(friend.user_elemen)[1] : Object.values(friend.user_maba)[1]

  const placeDateOfBirth = `${elemen?.birth_place}, ${elemen?.birth_date}`;


  const colorPicker = (status) => {
    let color = "";
    switch (status) {
      case "PENDING":
        color = "gray";
        break;
      case "APPROVED":
        color = "#F0CE0F ";
        break;
      case "REJECTED":
        color = "#C81B1B";
        break;
      case "SAVED":
        color = "#9ED7F2";
        break;
      default:
        color = "#999999";
    }
    return color;
  };

  const getStatusText = () => {
    const text = friend.status.charAt(0).toUpperCase() + friend.status.slice(1).toLowerCase();

    return text.replace("_", " ");
  };

  console.log(elemen)

  return (
    <div>
      <FriendCardStyle profileImage={elemen.photo} style={{ margin: "0 auto" }}>
        <div className="FriendCard">
          <div className="FriendCard_photo_and_profile_section">
            <div className="FriendCard_photo_section">
              <div className="FriendCard_photo" />
            </div>
            <div className="FriendCard_profile_section">
              <div className="FriendCard_name_edit">
                <div className="FriendCard_name_section">
                  <h1 className="FriendCard_name">{elemen.name}</h1>
                </div>
              </div>
              <div className="FriendCard_status_edit_button">
                {
                  friend.status !== undefined &&
                  <h1
                    className="FriendCard_status"
                    style={{ color: colorPicker(friend.status) }}
                  >
                    {getStatusText()}
                  </h1>
                }
                {
                  // eslint-disable-next-line no-mixed-operators
                  isUserMaba && friend.status === "REJECTED" || friend.status === "SAVED" ? (
                    <div className="FriendCard_edit_button">
                      <Button width="60px" onClick={() => handleEdit()}>Edit</Button>
                    </div>
                  ) : null
                }
              </div>
              <div className="FriendCard_detail_section">
                <div className="FriendCard_detail">
                  <img
                    src={JurusanIcon}
                    width="15px"
                    style={{ marginRight: "5px" }}
                    alt="Jurusan"
                  />
                  <p>FASILKOM {elemen.batch.name}</p>
                </div>
                <div className="FriendCard_detail">
                  <img
                    src={TanggalLahirIcon}
                    width="15px"
                    style={{ marginRight: "5px" }}
                    alt="Tanggal Lahir"
                  />
                  <p>{placeDateOfBirth}</p>
                </div>
                <div className="FriendCard_detail">
                  <img
                    src={LineIcon}
                    width="15px"
                    style={{ marginRight: "5px" }}
                    alt="LINE"
                  />
                  <p>{elemen.line_id}</p>
                </div>
                <div className="FriendCard_detail">
                  <img
                    src={AsalSekolahIcon}
                    width="15px"
                    style={{ marginRight: "5px" }}
                    alt="Asal Sekolah"
                  />
                  <p>{elemen.high_school}</p>
                </div>
                <div className="FriendCard_detail">
                  <img
                    src={EmailIcon}
                    width="15px"
                    style={{ marginRight: "5px" }}
                    alt="Email"
                  />
                  <p>{elemen.email}</p>
                </div>
                <div className="FriendCard_detail">
                  <img
                    src={WebIcon}
                    width="15px"
                    style={{ marginRight: "5px" }}
                    alt="Website"
                  />
                  <p>{elemen.website}</p>
                </div>
              </div>
            </div>
          </div>
          <h3 className="FriendCard_interest_title">Top Interest</h3>
          <div className="FriendCard_interest_section">
            {
              elemen.interests?.map((interest) => (
                <div className="FriendCard_interest">
                  <p>{interest.interest_name}</p>
                </div>
              )
              )
            }
          </div>
          <div className="FriendCard_description_section">
            <h3>Description</h3>
            {
              friend.description ?
                <p>{friend.description}</p> :
                <p>-</p>
            }
          </div>
          {
            friend.status === "REJECTED" ? (
              <div className="FriendCard_feedback_section">
                <h3>Feedback</h3>
                <p>{friend.rejection_message}</p>
              </div>
            ) :
              <div className="FriendCard_feedback_section">
                <p>{" "}</p>
              </div>
          }
          <div>
            {
              !isUserMaba &&
              <div style={{ display: "flex", float: "right", margin: "0 2em" }}>
                {
                  // eslint-disable-next-line no-mixed-operators
                  (friend.status === "PENDING") ?
                    <>
                      <div style={{ margin: "0 0.5em 1em" }}>
                        <Button onClick={() => handleApprove(friend.id, { "is_approved": true })} width="100px">Accept</Button>
                      </div>
                      <div style={{ margin: "0 0.5em 1em" }}>
                        <Button width="100px" borderColor="#F40000" onClick={() => handleReject()}>
                          Reject
                      </Button>
                      </div>
                    </> : null
                }
              </div>
            }
          </div>
        </div>
      </FriendCardStyle>
    </div>
  );
}

FriendCard.propTypes = {
  friend: PropTypes.shape({}),
  isUserMaba: PropTypes.bool,
  handleApprove: PropTypes.func,
  handleReject: PropTypes.func,
  handleEdit: PropTypes.func,
};

export default FriendCard;
