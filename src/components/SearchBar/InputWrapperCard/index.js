import React, { Component } from "react";
import PropTypes from "prop-types";

import { InputWrapperCardStyle } from "./style";

class InputWrapperCard extends Component {
  render() {
    return (
      <InputWrapperCardStyle>
        <div className="Card">{this.props.children}</div>
      </InputWrapperCardStyle>
    );
  }
}

InputWrapperCard.propTypes = {
  children: PropTypes.node.isRequired
};

export default InputWrapperCard;
