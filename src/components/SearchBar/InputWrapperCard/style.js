import styled from "styled-components";

export const InputWrapperCardStyle = styled.div`
  .Card {
    position: relative;
    padding: 1rem;
    margin-bottom: 1rem;
    box-shadow: unset;
    background-color: white;
    border-radius: 7px;
  }

  p,
  a {
    font-size: 16px;
  }

  h1 {
    color: blue;
    font-size: 30px;
  }

  h2 {
    font-size: 20px;
  }

  @media (max-width: 1280px) {
    h1 {
      font-size: 26px;
    }

    h2 {
      font-size: 18px;
    }

    p,
    a {
      font-size: 14px;
    }
  }

  @media (max-width: 880px) {
    h1 {
      font-size: 24px;
    }

    h2 {
      font-size: 16px;
    }

    p,
    a {
      font-size: 12px;
    }
  }

  @media (max-width: 440px) {
    h1 {
      font-size: 20px;
    }

    h2 {
      font-size: 14px;
    }

    p,
    a {
      font-size: 11px;
    }
  }

  @media (max-width: 360px) {
    h1 {
      font-size: 18px;
    }

    h2 {
      font-size: 12.5px;
    }

    p,
    a {
      font-size: 11px;
    }
  }
`;
