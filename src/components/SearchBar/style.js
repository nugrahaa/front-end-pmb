import styled from "styled-components";

export const SearchBarStyle = styled.div`
  font-size: 14px;
  font-family: 'Metropolis';
  font-style: normal;
  font-weight: normal;
  line-height: 15px;

  background: #FFFFFF;
  mix-blend-mode: normal;
  border: 1px solid #1B274B;
  box-sizing: border-box;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: 10px;

  .SearchBar__input {
    font-family: 'Metropolis';
    border: none;
    flex-grow: 1;
    height: 1rem;
    font-family: 'Metropolis';
    font-size: 14px;
  }

  .SearchBar__input::-webkit-input-placeholder {
    font-family: 'Metropolis';
    color: rgba(27, 39, 75, 0.7);
  }

  .SearchBar__input::-moz-placeholder {
    font-family: 'Metropolis';
    color: rgba(27, 39, 75, 0.7);
  }

  .SearchBar__input::-ms-placeholder {
    font-family: 'Metropolis';
    color: rgba(27, 39, 75, 0.7);
  }

  .SearchBar__input::placeholder {
    font-family: 'Metropolis';
    color: rgba(27, 39, 75, 0.7);
  }


  .SearchBar__input:focus {
    outline: none;
  }

  .Card {
    display: flex;
    flex-direction: row-reverse;
    align-items: center;
    margin-bottom: 0 !important;
    padding: 10px !important;
    border-radius: 10px !important;
  }

  .SearchBar__searchIcon {
    margin-right: 2px;
  }

  @media (max-width: 480px) {
    font-size: 12px;
  }
`;
