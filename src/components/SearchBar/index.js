import React from "react";
import { SearchBarStyle } from "./style";
import PropTypes from "prop-types";

import SearchIcon from "../../assets/icons/search.svg";
import InputWrapperCard from "./InputWrapperCard";

const SearchBar = ({onQueryChange = ()=>{}, ...props})=> {
  // constructor(props) {
  //   super(props);

  //   this.handleChange = this.handleChange.bind(this);
  // }

  const handleChange = (event)=> {
    onQueryChange(event.target.value);
  }

  
    return (
      <SearchBarStyle>
        <InputWrapperCard>
          <img
            className="SearchBar__searchIcon"
            src={SearchIcon}
            alt="search-icon"
          />
          <input
            className="SearchBar__input"
            type="text"
            value={props.query}
            onChange={handleChange}
            {...props}
   
          />
        </InputWrapperCard>
      </SearchBarStyle>
    );

}

SearchBar.propTypes = {
  placeholder: PropTypes.string,
  query: PropTypes.string,
  handleFocus: PropTypes.func,
  handleBlur: PropTypes.func,
};

export default SearchBar;
