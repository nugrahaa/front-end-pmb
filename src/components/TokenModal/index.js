import React from 'react';

import Button from '../Button';
import Modal from '../Modal';

import {
  ModalContainer,
  ButtonContainer,
  TokenField,
  TokenFieldWrapper
} from './style'


const TokenModal = ({
  open,
  handleClose,
  token,
  count,
  children,
  ...props
}) => {
  const handleDone = () => {
    handleClose();
  };

  return (
    <Modal
      open={open}
      preventClose={true}
      maxWidth="sm"
      fullWidth>
        <ModalContainer>
          <div className="generated-token-info-wrapper">
            <div className="token-num">
            <h5><span>Count</span> : {count}</h5>

            </div>
            <div className="timer">
              {children}
            </div>
          </div>
          <TokenFieldWrapper>
            <TokenField>
              <h2>Your Token:</h2>
              <h1>{token}</h1>
            </TokenField>
          </TokenFieldWrapper>
          <ButtonContainer>
            <Button
              width="100px"
              height="30px"
              textColor="#1B274B"
              onClick={handleDone}>
              Done
            </Button>
          </ButtonContainer>
        </ModalContainer>
    </Modal>

  )
}

export default TokenModal;