import styled from 'styled-components';

export const ModalContainer = styled.div`
  width: 100%;

  .generated-token-info-wrapper {
    display: flex;
    flex-direction: row;
  }

  .generated-token-info-wrapper .token-num {
    width: 50%;
    text-align: left;
  }

  .generated-token-info-wrapper .token-num h5 {
    font-family: 'Metropolis';
    font-weight: normal;
    margin: 0;
  }

  .generated-token-info-wrapper .token-num h5 span {
    font-weight: bold;
  }

  .generated-token-info-wrapper .timer {
    width: 50%;
    margin: 0;
  }
`

export const TokenFieldWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`

export const TokenField = styled.div`
  font-family: Metropolis;
  font-style: normal;
  // text-align: center;
  color: #1B274B;

  // display: block;
  // margin-left: auto;
  // margin-right: auto;
  width: fit-content;
  padding: 40px 50px 30px;

  h1 {
    font-weight: bold;
    font-size: 80px;
    margin: 0;
    // text-align: center;
  }

  h2 {
    font-weight: normal;
    margin: 0;
    line-height: 30px;
  }

  @media only screen and (max-width: 500px) {
    h1 {
      font-size: 50px;
    }
    h2 {
      font-size: 1em;
    }
  }
`

export const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
`
