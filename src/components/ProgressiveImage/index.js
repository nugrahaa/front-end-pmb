// A progressive image component that's purely written in hooks.
//
//
// Usecase:
// Let's say we want to render an image called 'big-src.png' and have 'small-src.png' as it's placeholder
// The code would be something like this:
//
// <ProgressiveImage src="/big-src.png" fallbackSrc="/small-src.png" alt="my image" />
//
// This component is fully customizable, so styling using className and inline styles e.g. styles={{..}} is fully supported
//
// Jonathan Filbert and Ristek Webdev 2020

import React, { useState, useEffect, useReducer } from "react";
import PropTypes from "prop-types";

const reducer = (currentSrc, action) => {
  if (action.type === "main image done") {
    return action.src;
  }
  if (!currentSrc) {
    return action.src;
  }
  return currentSrc;
};

const useProgressiveImage = ({ src, fallbackSrc }) => {
  const [currentSrc, dispatch] = useReducer(reducer, null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const mainImage = new Image();
    const fallbackImage = new Image();

    mainImage.onload = () => {
      dispatch({ type: "main image done", src });
      setIsLoading(false);
    };
    fallbackImage.onload = () => {
      dispatch({ type: "fallback image done", src: fallbackSrc });
    };

    mainImage.src = src;
    fallbackImage.src = fallbackSrc;
  }, [src, fallbackSrc]);

  return [currentSrc, isLoading];
};

const ProgressiveImage = ({ fallbackSrc, alt, style, className, ...props }) => {
  const [src, isLoading] = useProgressiveImage({
    src: props.src,
    fallbackSrc: fallbackSrc,
  });
  if (!src) return null;
  return (
    <img
      alt={alt}
      src={src}
      className={className}
      style={{
        transition: "1s filter ease",
        filter: `${isLoading ? "grayscale(0.5) blur(30px)" : ""}`,
        ...style,
      }}
    />
  );
};

ProgressiveImage.propTypes = {
  fallbackSrc: PropTypes.string,
  alt: PropTypes.string,
  style: PropTypes.object,
  className: PropTypes.string,
  src: PropTypes.string,
};

export default ProgressiveImage;
