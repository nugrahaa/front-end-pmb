import React, { useState, useEffect } from "react";
import { TooltipStyle } from "../CalendarCard/style";
import {
  horizontalAdjuster,
  verticalAdjuster,
} from "../CalendarCard/tooltipAdjuster";
import PropTypes from "prop-types";
import { Media } from "../../components/Layout/Media";

/*
  Tooltip not extracted to separate component, due to heavy customization
  and workarouds to work specifically with calendar

  https://codepen.io/andrewerrico/pen/OjbvvW?editors=0110
*/
const CalendarTooltip = ({ message, children }) => {
  const [displayTooltip, setDisplayTooltip] = useState(false);
  const [horizontalCalculated, setHorizontalCalculated] = useState(false);
  const [verticalCalculated, setVerticalCalculated] = useState(false);

  // reference: https://stackoverflow.com/questions/50970336/prevent-css-tooltip-from-going-out-of-page-window
  useEffect(() => {
    horizontalAdjuster(horizontalCalculated, setHorizontalCalculated);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [displayTooltip]);

  useEffect(() => {
    verticalAdjuster(
      verticalCalculated,
      horizontalCalculated,
      setVerticalCalculated
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [horizontalCalculated]);

  useEffect(() => {
    const hideTooltip = () => {
      setDisplayTooltip(false);
    };
    window.addEventListener("resize", hideTooltip);
    return () => {
      window.removeEventListener("resize", hideTooltip);
    };
  }, []);

  return (
    <Media>
      {(matches) => (
        <TooltipStyle isMobile={matches.mobile}>
          <div
            className="Tooltip"
            onMouseLeave={() => setDisplayTooltip(false)}
          >
            {displayTooltip && <div className="arrow"></div>}
            {displayTooltip && (
              <div className="Tooltip__bubble Tooltip__top">
                <div className="Tooltip__message">{message}</div>

                {(!horizontalCalculated || !verticalCalculated) && (
                  <div className="Tooltip__message_fetch">fetching...</div>
                )}
              </div>
            )}
            <div
              className="Tooltip__trigger"
              onMouseOver={() => setDisplayTooltip(true)}
              onClick={() => setDisplayTooltip(true)}
            >
              {children}
            </div>
          </div>
        </TooltipStyle>
      )}
    </Media>
  );
};

CalendarTooltip.propTypes = {
  children: PropTypes.node.isRequired,
  message: PropTypes.node.isRequired,
};

export default CalendarTooltip;
