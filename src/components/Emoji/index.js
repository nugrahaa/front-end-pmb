import React, { memo } from "react";
import twemoji from "twemoji";

const Emoji = ({ emoji }) => {
  return (
    <span
      style={{
        display: "inline-block",
        height: "100%",
        width: "100%",
      }}
      dangerouslySetInnerHTML={{
        __html: twemoji.parse(emoji, {
          folder: "svg",
          ext: ".svg",
        }),
      }}
    />
  );
};

export default memo(Emoji);
