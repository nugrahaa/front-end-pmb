import React from "react";
import PropTypes from "prop-types";
// import { push } from "connected-react-router";
// import { connect } from "react-redux";

import { EventListCardStyle } from "./style";

import EventDateIcon from "../../assets/icons/event-date-icon.svg";
import EventLocationIcon from "../../assets/icons/event-location-icon.svg";
import { timestampParser } from "../../utils/timestampParser";
import Button from "../../components/Button";

function EventListCard({
    title,
    date,
    location,
    onClick,
}) {
    const newDate = timestampParser(new Date(date));
    return (
        <div>
            <EventListCardStyle>
                <div className="EventListCard">
                    <h1 className="EventListCard_title">{title}</h1>
                    <div className="EventListCard_date">
                        <img
                            className="EventListCard_date_icon"
                            src={EventDateIcon}
                            alt="Calendar Icon"
                        />
                        <p className="EventListCard_date_text">{newDate}</p>
                    </div>
                    <div className="EventListCard_location">
                        <img
                            className="EventListCard_location_icon"
                            src={EventLocationIcon}
                            alt="Location Icon"
                        />
                        <p className="EventListCard_location_text">{location}</p>
                    </div>
                    <div className="EventListCard_button">
                        <Button onClick={onClick}>Details</Button>
                    </div>
                </div>
            </EventListCardStyle>
        </div>
    );
  }

  EventListCard.propTypes = {
    title: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    location: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
  };

//   const mapDispatchToProps = (dispatch) => ({
//     push: (url) => dispatch(push(url)),
//   });
  
export default EventListCard;