import styled from "styled-components";

export const EventListCardStyle = styled.div`
  .EventListCard {
    background: #ffffff;
    mix-blend-mode: normal;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    border-radius: 10px;

    width: 100%;
    height: 145px;
  }

  .EventListCard_title {
    font-family: Metropolis;
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 16px;

    color: #4a69ca;

    padding-left: 20px;
    padding-top: 10px;
  }

  .EventListCard_date {
    margin: 10px;
  }

  .EventListCard_date_icon {
    float: left;
    margin-right: 6px;
    margin-left: 10px;
  }

  .EventListCard_date_text {
    font-family: Metropolis;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 16px;

    color: #1b274b;
  }

  .EventListCard_location {
    margin: 10px;
  }

  .EventListCard_location_icon {
    float: left;
    margin-right: 10px;
    margin-left: 10px;
  }

  .EventListCard_location_text {
    font-family: Metropolis;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 16px;

    color: #1b274b;
  }

  .EventListCard_button {
    padding-right: 20px;
    float: right;
  }

  @media (min-width: 481px) and (max-width: 767px) {
    .EventListCard {
      width: 100%;
      height: 150px;
    }
  }

  @media (min-width: 321px) and (max-width: 480px) {
    .EventListCard {
      width: 100%;
      height: 150px;
    }
  }

  @media (max-width: 320px) {
    .EventListCard {
      width: 100%;
      height: 160px;
    }
  }
`;
