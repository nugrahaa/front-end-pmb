import React from "react";

import { LoadingContainer } from "./style";

function LoadingFullscreen() {
  return (
    <LoadingContainer>
      <div className="overlay">
        <div className="overlay-content">
          <div className="spinner" />
        </div>
      </div>
    </LoadingContainer>
  );
}

export default LoadingFullscreen;
