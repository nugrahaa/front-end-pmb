import React from "react";
import { DayPickerSingleDateController } from "react-dates";
import moment from "moment";
import { HORIZONTAL_ORIENTATION } from "react-dates/constants";
import range from "lodash/range";

const propsOptions = {
  initialDate: moment("2000-02-27"),
  orientation: HORIZONTAL_ORIENTATION,
  numberOfMonths: 1,
  enableOutsideDays: false,
};

const renderMonthElement = (month, onMonthSelect, onYearSelect) => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
      }}
    >
      <div>
        <select
          value={month.month()}
          onChange={(e) => {
            onMonthSelect(month, e.target.value);
          }}
        >
          {moment.months().map((label, value) => (
            <option value={value}>{label}</option>
          ))}
        </select>
      </div>
      <div>
        <select
          value={month.year()}
          onChange={(e) => {
            onYearSelect(month, e.target.value);
          }}
        >
          {range(1, 31).map((item) => (
            <option value={moment().year() - 30 + item}>
              {moment().year() - 30 + item}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};

const DateInputWrapper = ({ onChangeDate, onOutsideClick }) => {
  const handleChangeDate = (date) => {
    onChangeDate(date);
  };
  return (
    <div style={{ position: "absolute", left: 0, right: 0, zIndex: 900 }}>
      <DayPickerSingleDateController
        {...propsOptions}
        onDateChange={(date) => handleChangeDate(date)}
        onOutsideClick={() => onOutsideClick()}
        renderMonthElement={({ month, onMonthSelect, onYearSelect }) =>
          renderMonthElement(month, onMonthSelect, onYearSelect)
        }
        navPrev={<div />}
        navNext={<div />}
      />
    </div>
  );
};

export default DateInputWrapper;
