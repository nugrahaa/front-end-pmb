import React, { useState } from "react";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import DateInputWrapper from "./DateInputWrapper";
import Input from "../Input";
import moment from "moment";

const DateInput = ({ onSelectDate, value }) => {
  const [isDatePickerShown, setIsDatePickerShown] = useState(false);
  const [date, setDate] = useState(value);
  const handleFocus = () => {
    setIsDatePickerShown(true);
  };

  const handleChangeDate = (date) => {
    setDate(moment(date).format("DD MMM YYYY"));
    onSelectDate(moment(date).format("YYYY-MM-DD"));
    setIsDatePickerShown(false);
  };

  return (
    <div style={{ width: "100%", position: "relative" }}>
      <Input
        onFocus={() => handleFocus()}
        value={date}
        placeholder={moment("2000-02-27").format("DD MMM YYYY")}
        onChange={() => null}
      />
      {isDatePickerShown && (
        <DateInputWrapper
          onChangeDate={(date) => handleChangeDate(date)}
          onOutsideClick={() => setIsDatePickerShown(false)}
        />
      )}
    </div>
  );
};

export default DateInput;
