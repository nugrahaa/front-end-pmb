import styled from "styled-components";

export const StyledSidenav = styled.div`
  .sidenav-container {
    height: 100%;
    position: fixed;
    z-index: 9999;
    top: 0;
    left: 0;
    background: #f7fbff;
    overflow-x: hidden;
    padding-top: 2.25rem;
    transition: 0.5s;
    width: 100%;
    min-width: 19rem;

    .side-item {
      border-bottom: solid 1px #1b274b;
      padding: 0.75rem;
      margin: 0 1.25rem;
    }

    .side-profile {
      padding: 0.75rem 0 1.5rem 0.75rem;
      margin: 0 1.25rem;
      display: flex;
      justify-content: flex-start;
      align-items: center;
    }

    .logout {
      color: red;
      padding: 0.75rem;
      margin: 0 1.25rem;
      text-align: left;
      display: block;
      background: none !important;
      border: none;
      cursor: pointer;
      font-size: 1.25rem;
      color: red;
      width: 100%;
    }

    .logout:hover {
      color: rgba(255, 0, 0, 0.3);
    }

    .profile-content {
      margin-top: 1rem;
      margin-left: 0.5rem;

      h2 {
        font-weight: bold;
      }

      p {
        font-size: 0.9rem;
        margin: 6px 0 12px 0;
      }
    }

    .profile-image {
      align-self: center;
      width: 3.25rem;
      height: 3.25rem;
      border-radius: 50%;
      margin-right: 0.5rem;
      background-position: center;
      background-size: cover;
      box-shadow: 0 0.4rem 0.5rem -0.2rem #545454;
      background-image: url(${(props) => props.profileImage});
      background-color: #ffffff;
    }
  }

  .sidenav-container a {
    text-decoration: none;
    font-size: 1.25rem;
    color: rgba(27, 39, 75, 1);
    display: block;
    transition: 0.3s;
  }

  .sidenav-container a:hover {
    color: rgba(27, 39, 75, 0.3);
  }

  .sidenav-container .close-button {
    position: absolute;
    top: 12px;
    right: 12px;
    font-size: 36px;
    margin: 8px 8px 8px 58px;
    background: none !important;
    border: none;
    cursor: pointer;
  }
`;
