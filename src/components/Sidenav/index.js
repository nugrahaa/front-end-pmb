import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import {
  NAVBAR_ITEMS_MOBILE_MABA,
  NAVBAR_ITEMS_MOBILE_NON_MABA,
} from "../../constants/menuConstants";
import { StyledSidenav } from "./style";
import { logout } from "../../globalActions";
import { connect } from "react-redux";
import { loggingOutApi } from "../../api";

const Sidenav = ({
  isOpened,
  setIsOpened,
  userData,
  userPhoto,
  isUserMaba,
  logout,
}) => {
  const closeNav = () => {
    setIsOpened(!isOpened);
  };

  const getSideBarMenu = () => {
    if (isUserMaba) {
      return NAVBAR_ITEMS_MOBILE_MABA;
    }
    return NAVBAR_ITEMS_MOBILE_NON_MABA;
  };

  const menuItems = Object.keys(userData).length !== 0 ? getSideBarMenu() : [];

  const handleLogout = () => {
    const logoutWindow = window.open(loggingOutApi, "_self");
    const getUserDataInterval = setInterval(() => {
      if (logoutWindow.closed) {
        clearInterval(getUserDataInterval);
      }

      logoutWindow.postMessage("MadeByWebdevRistek2020", loggingOutApi);
    }, 1000);

    logout();
  };

  return (
    <StyledSidenav profileImage={userPhoto}>
      {isOpened && (
        <div className="sidenav-container">
          <button className="close-button" onClick={closeNav}>
            &times;
          </button>
          <div className="side-profile">
            <div className="profile-image" />
            <div className="profile-content">
              <h2>{Object.keys(userData).length !== 0 && userData.name}</h2>
              <p>
                {Object.keys(userData).length !== 0 &&
                  `${userData.batch.name} ${userData.batch.year}`}
              </p>
            </div>
          </div>
          {menuItems.map((item, index) => (
            <Link
              key={index}
              className="side-item"
              to={item.url}
              onClick={closeNav}
            >
              {item.name}
            </Link>
          ))}
          <button className="logout" onClick={() => handleLogout()}>
            Logout
          </button>
        </div>
      )}
    </StyledSidenav>
  );
};

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logout()),
});

Sidenav.propTypes = {
  isOpened: PropTypes.bool.isRequired,
  setIsOpened: PropTypes.func.isRequired,
  userData: PropTypes.shape().isRequired,
  userPhoto: PropTypes.string.isRequired,
};

export default connect(null, mapDispatchToProps)(Sidenav);
