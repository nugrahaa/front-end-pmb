import React from "react";
import PropTypes from "prop-types";
import { Base, Wrapper, Container } from "./style";

function TextArea({
  width = "100%",
  withError,
  ...props
}) {
  
  return (
    <Container>
      <Wrapper width={width} hasError={!!withError} >
        <Base  
          hasError={!!withError}
          {...props}
        />
        
        <br />
      </Wrapper>
      {withError && (
        <span style={{ color: "red", fontSize: "12px" }}>{withError}</span>
      )}
    </Container>
  );
}

TextArea.propTypes = {
 
  withError: PropTypes.string,
  width: PropTypes.string,
};

export default TextArea;
