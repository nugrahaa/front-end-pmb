import styled, { css } from "styled-components";
export const Wrapper = styled.div`
  border: 1px solid #1B274B;
  box-sizing: border-box;
  border-radius: 10px;
  background-color: #fff;
  width: ${({ width }) => width};
 
  position: relative;
  input:-webkit-autofill,
  input:-webkit-autofill:hover, 
  input:-webkit-autofill:focus,
  textarea:-webkit-autofill,
  textarea:-webkit-autofill:hover,
  textarea:-webkit-autofill:focus,
  select:-webkit-autofill,
  select:-webkit-autofill:hover,
  select:-webkit-autofill:focus {
 

    -webkit-box-shadow: 0 0 0px 1000px transparent inset;
    transition: background-color 5000s ease-in-out 0s;
  }

  img{
      width:18px;
      height:18px;
      position: absolute;
      top: 20%;
      right: 10px;
      cursor: pointer;
  }




    svg path:nth-of-type(1) {
        border: solid 1px #1B274B;

        svg path:nth-of-type(2) {
        fill: #1B274B;
        }

        :focus-within {
        box-shadow: 0px 0px 5px 0px #1B274B;
        border: solid 1px #1B274B;
        }
        fill: #1B274B;
    }

    :focus-within {
        box-shadow: 0px 0px 5px 0px #1B274B;
        border: solid 1px #1B274B;
    }

 
  /* handle props hasError */
  ${({ hasError }) =>
    hasError &&
    css`
      border: solid 1px red;

      :focus-within {
        box-shadow: 0px 0px 5px 0px red;
        border: solid 1px red;
      }
    `}
`;

export const Base = styled.textarea`
  padding:  0.5em 0.5em;
  width: calc(100% - 13px);
  height: 100%;
  outline: none;
  border: none;
  overflow: hidden;
  background: transparent;
  font-family: 'Metropolis';
  text-align: justify;

  color: #000000;
  font-size: 0.9em;

  ::placeholder {
    color: #bdbdbd;
    opacity: 1;
    font-size: 0.9em;
  }

  :disabled {
    cursor: not-allowed;
    background-color: #bdbdbd;
    opacity: 0.5;
  }
`;
export const Container = styled.div`
  height: auto;
  word-wrap: break-word;
`;
