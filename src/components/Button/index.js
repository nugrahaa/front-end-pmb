import React from "react";
import PropTypes from "prop-types";
import { Base } from "./style";

function Button({
  children,
  width = "125px",
  height = "32px",
  backgroundColor = "white",
  borderColor = "#4A69CA",
  textColor = "#000000",
  onClick = () => null,
  loading,
  disabled,
  ...props
}) {
  return (
    <Base
      backgroundColor={backgroundColor}
      borderColor={borderColor}
      textColor={textColor}
      width={width}
      height={height}
      onClick={onClick}
      disabled={disabled || loading}
      {...props}
    >
      {loading ? <>Loading...</> : children}
    </Base>
  );
}

Button.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  backgroundColor: PropTypes.string,
  textColor: PropTypes.string,
  borderColor: PropTypes.string,
};

export default Button;
