import styled from "styled-components";

const NotFoundPageWrapper = styled.div`
  .container {
    display: flex;
    width: 100%;
    height: 100%;
    justify-content: center;
    align-items: center;
  }
  .column {
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
  }

  .title {
    font-weight: bold;
  }

  .message {
    text-align: center;
  }

  .kite {
    margin: 0;
    cursor: pointer;
    transition: all 1s;
  }

  .kite:hover {
    transition: all 1s;
    margin-top: 20px;
  }

  .cta{
    font-weight:bold;
  }
`;

export default NotFoundPageWrapper;
