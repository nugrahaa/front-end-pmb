import React from "react";
import NotFoundPageWrapper from "./style";
import NotFoundPageAsset from "./assets/NotFoundPageAsset";
import { push } from "connected-react-router";
import { connect } from "react-redux";
import {Helmet} from 'react-helmet';

const NotFoundPage = ({ push }) => {
  return (
    <NotFoundPageWrapper>
      <Helmet>
          <title>PMB 2020 - Not Found</title>
        </Helmet>
      <div className="container">
        <div className="column">
          <h1 className="title">Are you lost?</h1>
          <h3 className="message">
            If you're ever feeling lost when studying in Fasilkom,
          </h3>
          <h3>Remember,</h3>
          <h3> We're all here for you.</h3>
          <h3>Everyone is here for you.</h3>
          <h3 className="cta" >Click on the kite to fly home.</h3>
          <div className="kite" onClick={() => push("/")}>
            <NotFoundPageAsset />
          </div>
        </div>
      </div>
    </NotFoundPageWrapper>
  );
};

const mapDispatchToProps = (dispatch) => ({
  push: (url) => dispatch(push(url)),
});

export default connect(null, mapDispatchToProps)(NotFoundPage);
