import React, { useState, useEffect, useRef } from 'react';

import {
  Timer
} from './style'


const TimerCountdown = ({ start, handleClose, timerDuration }) => {
  const [time, setTime] = useState({
    minutes: timerDuration,
    seconds: 0,
  });
  const timer= useRef(null);

  useEffect(() => {
    const setCountDown = (startingMinutes) => {
      let time = startingMinutes * 60;
      timer.current = setInterval(() => {
        var minutes = Math.floor(time / 60);
        var seconds = time % 60;
        if (time < 0) {
          clearInterval(timer.current);
          setTime({
            minutes: timerDuration,
            seconds: 0,
          });
          handleClose(); /* eslint-disable */
        } else {
          setTime({
            minutes: minutes,
            seconds: seconds,
          });
          time = time - 1;
        }
      }, 1000);
    };
    if (start) {
      setCountDown(timerDuration)
    } else {
      setTime({
        minutes: timerDuration,
        seconds: 0,
      });
      clearInterval(timer.current);
    }
  }, [start, timerDuration]);

  return (
    <Timer>
      <h5>
        <span>Timer </span>: {time.minutes}m{" "}
        {time.seconds < 10 ? `0${time.seconds}` : time.seconds}s
      </h5>
    </Timer>
  );
};

export default TimerCountdown;