import styled from 'styled-components';

export const Timer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  span:nth-child(1) {
    font-weight: bold;
  }
  h5 {
    font-family: "Metropolis";
    margin: 0;
    font-weight: normal;
  }
`;