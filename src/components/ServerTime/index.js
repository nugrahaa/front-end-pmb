import React from "react";
import PropTypes from "prop-types";

//not use servertime
const ServerTime = (props) => {
  return (
    <div
      style={{
        fontSize: "0.875em",
        lineHeight: "0.875em",
        color: "#1B274B",
        display: "grid",
        gridTemplateColumns: "0.8fr 1fr",
        justifyContent: "space-between",
        borderBottom: "1px solid rgba(27, 39, 75, 0.8)",
        paddingBottom: "2px",
      }}
    >
      <div>Server Time: </div>
      <div style={{ justifySelf: "end", fontWeight: "bold" }}>
        {props.dateTime ? props.dateTime : "-------"}
      </div>
    </div>
  );
};

ServerTime.propTypes = {
  dateTime: PropTypes.string,
};

export default ServerTime;
