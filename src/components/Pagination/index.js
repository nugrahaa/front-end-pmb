import React, { useEffect } from "react";
import { PaginationStyle } from "./style";
import PropTypes from "prop-types";
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';

function Pagination(props) {
    const {
      records,
      itemPerPage,
      currentPage,
      numberOfPagesShown,
      onPageChange,
      onPaginatedRecordsChange
    } = props;

    if (numberOfPagesShown % 2 === 0) {
      throw new Error("props numberOfPagesShown must be odd");
    }

    const numberOfItems = records.length;
    const lastPage = Math.ceil(numberOfItems / itemPerPage);
    const paginatedRecords = records.slice(
      itemPerPage * (currentPage - 1),
      itemPerPage * currentPage
    );
    const pageNumbersShown = [...Array(numberOfPagesShown).keys()]
      .map(index => index + currentPage - Math.floor(numberOfPagesShown / 2))
      .filter(index => index <= lastPage && index >= 1);
    const nextPageFunction = nextPagenumber => {
      (() => onPageChange(nextPagenumber))();
    };

    useEffect(() => {
      onPaginatedRecordsChange(paginatedRecords);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentPage])

    if (records.length === 0) {
      return (
        <PaginationStyle>
          <nav />
        </PaginationStyle>
      );
    }

    return (
      <PaginationStyle
        numberOfPagesShown={Math.min(lastPage, props.numberOfPagesShown)}
      >
        <nav className="Pagination">
          <ul className="Pagination__pager">
            {currentPage > 1 ? (
              <li
                className="Pagination__pager__item"
                onClick={() => nextPageFunction(currentPage - 1)}
              >
                <ArrowLeftIcon style={{marginTop: "8px"}}/>
              </li>
            ) : (
              <li className="Pagination__pager__item Pagination__disabled">
                <ArrowLeftIcon style={{marginTop: "8px"}}/>
              </li>
            )}
            <div className="Pagination__page-numbers">
              <li className="Pagination__pager__ellipsis">
                {currentPage > 1 + Math.floor(numberOfPagesShown / 2) ? (
                  <>...</>
                ) : (
                  <>&nbsp;</>
                )}
              </li>
              {pageNumbersShown.map(pageNumber => (
                <li
                  className={
                    pageNumber === currentPage
                      ? "Pagination__pager__item Pagination__active"
                      : "Pagination__pager__item"
                  }
                  key={pageNumber}
                  onClick={() => nextPageFunction(pageNumber)}
                >
                  {pageNumber}
                </li>
              ))}
              <li className="Pagination__pager__ellipsis">
                {currentPage < lastPage - Math.floor(numberOfPagesShown / 2) ? (
                  <>...</>
                ) : (
                  <>&nbsp;</>
                )}
              </li>
            </div>
            {currentPage < lastPage ? (
              <li
                className="Pagination__pager__item"
                onClick={() => nextPageFunction(currentPage + 1)}
              >
                <ArrowRightIcon style={{marginTop: "8px"}} />
              </li>
            ) : (
              <li className="Pagination__pager__item Pagination__disabled">
                <ArrowRightIcon style={{marginTop: "8px"}}/>
              </li>
            )}
          </ul>
        </nav>
      </PaginationStyle>
    );
  }

Pagination.propTypes = {
  records: PropTypes.array.isRequired,
  itemPerPage: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  numberOfPagesShown: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  onPaginatedRecordsChange: PropTypes.func.isRequired
};

export default Pagination;
