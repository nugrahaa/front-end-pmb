import styled from "styled-components";

export const PaginationStyle = styled.div`
font-family: 'Metropolis';
font-size: 8px;
font-weight: bold;
  .Pagination__pager {
    padding: 0;
    font-size: 0;
    display: flex;
    justify-content: space-around;
  }

  .Pagination__page-numbers {
    display: flex;
    min-width: calc(
      (${props => props.numberOfPagesShown} * 2.75rem) + 2 * 1.25rem
    );
  }

  .Pagination__pager__item,
  .Pagination__pager__ellipsis {
    cursor: pointer;
    display: inline-block;
    font-size: 1rem;
    position: relative;
    border-radius: 4px;
    display: block;
    text-align: center;
    width: 2.5rem;
    height: 2.5rem;
    line-height: 2.5rem;
    margin-left: -1px;
    color: #1B274B;
    text-decoration: none;
    transition: 0.3s;
    margin-right: 0.25rem;

    &:last-of-type {
      margin-right: 0;
    }
  }

  .Pagination__pager__ellipsis {
    cursor: default;
    width: 1rem;
  }

  .Pagination__pager__item.Pagination__active {
    cursor: default;
    background-color: #1B274B;
    border-color: #1B274B;
    color: white;
    text-decoration: none;
  }

  .Pagination__pager__item.Pagination__disabled {
    cursor: not-allowed;
    color: gray;
  }

  .Pagination__pager__link {
    position: relative;
    border-radius: 4px;
    display: block;
    text-align: center;
    width: 2.5rem;
    height: 2.5rem;
    line-height: 2.5rem;
    margin-left: -1px;
    color: #1B274B;
    text-decoration: none;
    transition: 0.3s;
  }

  .Pagination__pager__item:hover,
  .Pagination__pager__item:focus,
  .Pagination__pager__item:active {
    background-color: #1B274B;
    border-color: #1B274B;
    color: white;
    text-decoration: none;

    &.Pagination__disabled {
      background-color: initial;
      border-color: none;
      color: gray;
    }
  }
`;
