import React, { useEffect } from "react";
import { createPortal } from "react-dom";
import PropTypes from "prop-types";
import { AnimatePresence, motion } from "framer-motion";
import Success from "../../assets/img/success.png";
import Failed from "../../assets/img/failed.png";

import { Base } from "./style";

const ANIMATION_VARIANTS = {
  showed: {
    opacity: 1,
  },
  notShow: {
    opacity: 0,
    transition: { ease: "easeIn" },
  },
  exit: {
    opacity: 0,
    transition: { ease: "easeOut" },
  },
};

function Alert({ children, type = "success", show, handleClose, message }) {
  // automatically close
  useEffect(() => {
    if (show) {
      const timeoutClose = setTimeout(() => {
        handleClose();
        clearTimeout(timeoutClose);
      }, 3000);
    }
  }, [handleClose, show]);

  return createPortal(
    <AnimatePresence>
      {show && (
        <motion.div
          variants={ANIMATION_VARIANTS}
          animate="showed"
          initial="notShow"
          exit="exit"
        >
          <Base>
            <img src={type === "success" ? Success : Failed} alt="logo" />
            <h3>{message}</h3>
            <div>{children}</div>
          </Base>
        </motion.div>
      )}
    </AnimatePresence>,
    document.body
  );
}

Alert.propTypes = {
  show: PropTypes.bool.isRequired,
  message: PropTypes.string,
  handleClose: PropTypes.func.isRequired,
  type: PropTypes.oneOf(["success", "error"]),
};

export default Alert;
