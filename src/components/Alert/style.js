import styled from "styled-components";

export const Base = styled.div`
  position: fixed;
  top: 50%;
  display: flex;
  flex-direction: column;
  left: 50%;
  transform: translate(-50%, -50%);
  border-radius: 4px;
  box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.5);
  min-width: 249px;
  min-height: 239px;
  display: flex;
  align-items: center;
  padding: 40px 65px;
  justify-content: space-between;
  z-index: 1001;
  background-color: white;

  img {
    cursor: pointer;
    margin-left: 10px;

    path {
      fill: black;
    }
  }
  h3 {
    font-weight: normal;
    margin-top: 22px;
    font-family: "Metropolis";
    text-align: center;
  }
  @media only screen and (max-width:400px){
    padding: 20px 30px;

    img{
      width:200px;
    }
  }
`;
