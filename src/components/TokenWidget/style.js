import styled from "styled-components";

export const OutermostContainer = styled.div`
  width: 100%;
  height: fit-content;
  margin-top: 60px;
  h2.add-friends {
    font-family: 'Metropolis';
    font-style: normal;
    font-weight: normal;
  }

  @media only screen and (max-width: 991px) {
    margin-top: 0px;
  }
`

export const TokenCard = styled.div`
  width: calc(100% - 20px);
  height: fit-content;
  padding: 5px 10px;
  margin-left: 1px;

  background: #FFFFFF;
  mix-blend-mode: normal;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: 10px;

  // -webkit-touch-callout: none; /* iOS Safari */
  // -webkit-user-select: none; /* Safari */
  // -khtml-user-select: none; /* Konqueror HTML */
  // -moz-user-select: none; /* Old versions of Firefox */
  // -ms-user-select: none; /* Internet Explorer/Edge */
  // user-select: none; /* Non-prefixed version, currently
  //                     supported by Chrome, Edge, Opera and Firefox */

  h4.token-title {
    color: #1B274B;
    margin: 5px 0px;
    font-family: 'Metropolis';
    font-style: normal;
    font-weight: 600;
  }
`

export const TokenContentWrapper = styled.div`
  width: calc(100% - 20px);
  padding: 10px;
`