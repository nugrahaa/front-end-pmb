import React, { useState } from 'react';
import { useSelector } from "react-redux";

import SearchBar from '../SearchBar';
import Loading from '../Loading';
import { useHistory } from "react-router-dom";
import { isEventsLoading } from "../../selectors/events";

import {
  OutermostContainer,
  TokenCard,
  TokenContentWrapper
} from './style';

const TokenWidget = ({...props}) => {
  const [searchQuery, setSearchQuery] = useState("")
  const history = useHistory();
  const isLoadingEvents = useSelector((state) => isEventsLoading(state));

  const handleKeyPress = (event) => {
    if(event.key === 'Enter'){
      setSearchQuery(event.target.value)
      history.push(`/search?q=${event.target.value}`);
    }};

    // loading msh kehandle dua kali, di applayout sm di sini
    // will be fixed pas nanti di refactor sebelum masuk sprint 3

    return (
      <OutermostContainer>
        <SearchBar
          placeholder="Search friends or interests"
          onKeyPress = {handleKeyPress}
          defaultValue={searchQuery}
        />
        <h2 className="add-friends">Add Friends</h2>
          {!isLoadingEvents ?
            (<TokenCard>
              <h4 className="token-title">Token: </h4>
                <TokenContentWrapper>
                  {props.children}
                </TokenContentWrapper>
             </TokenCard>)
          : (<Loading />)}
      </OutermostContainer>
    )};

export default TokenWidget