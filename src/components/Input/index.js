import React, { useState } from "react";
import PropTypes from "prop-types";
import { Base, Wrapper, Container } from "./style";

function usePassword(originalType) {
  const isPasswordType = originalType === "password";
  const [showPass, setShowPass] = useState(false);
  const toggleShowPass = () => setShowPass((val) => !val);
  const type = showPass ? "text" : "password";

  return {
    type: isPasswordType ? type : originalType,
    toggleShowPass,
    isPasswordType,
    showPass,
  };
}

function Input({
  withIcon,
  width = "100%",
  iconPosition = "right",
  withError,
  ...props
}) {
  const { type, toggleShowPass, showPass, isPasswordType } = usePassword(
    props.type
  );

  return (
    <Container>
      <Wrapper width={width} hasError={!!withError} iconPosition={iconPosition}>
        <Base
          iconPosition={iconPosition}
          hasError={!!withError}
          {...props}
          type={type}
        />
        {withIcon && <img src={withIcon} alt="icon" />}
        {isPasswordType && !showPass && (
          <img
            src="https://cdn0.iconfinder.com/data/icons/ui-icons-pack/100/ui-icon-pack-14-512.png"
            alt="eye"
            onClick={toggleShowPass}
          />
        )}
        {isPasswordType && showPass && (
          <img
            src="https://icon-library.com/images/show-password-icon/show-password-icon-18.jpg"
            alt="eye-no"
            onClick={toggleShowPass}
          />
        )}
        <br />
      </Wrapper>
      {withError && (
        <span style={{ color: "red", fontSize: "12px" }}>{withError}</span>
      )}
    </Container>
  );
}

Input.propTypes = {
  withIcon: PropTypes.string,
  iconPosition: PropTypes.oneOf(["left", "right"]),
  withError: PropTypes.string,
  width: PropTypes.string,
};

export default Input;
