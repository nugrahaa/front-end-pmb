import styled from "styled-components";

export const DescriptionEditable = styled.textarea`
  width: 100%;
  border: 1px solid #bbb2c4;
  padding: 0.5rem;
  box-sizing: border-box;
  box-shadow: inset 2px 2px 4px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  height: 15rem;
`;

export const DescriptionStatic = styled.textarea`
  width: 100%;
  border: none;
`;
