import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { push } from "connected-react-router";

import iconBell from "../../assets/icons/bell.svg";
import arrowDown from "../../assets/icons/arrow_down.svg";
import iconHamburger from "../../assets/icons/hamburger.png";
import logoPmb from "../../assets/img/logo_pmb.png";
import logoRistek from "../../assets/img/ristekLogo.png";
import Sidenav from "../Sidenav";
import { loggingOutApi } from "../../api";
import { logout } from "../../globalActions";
import {
  NAVBAR_ITEM_STATISTICS,
  NAVBAR_ITEM_GALLERY,
  NAVBAR_ITEM_PROFILE,
  NAVBAR_ITEMS_MABA,
  NAVBARS_ITEMS_NON_MABA,
} from "../../constants/menuConstants";
import { fetchProfile } from "../../container/ProfilePage/actions";
import { StyledNavbarContainer } from "./style";
import { isMaba } from "../../selectors/user";

const Navbar = ({
  profile,
  fetchUserData,
  isUserMaba,
  batch,
  logout,
  push,
}) => {
  const [isDesktop, setIsDesktop] = useState(true);
  const [isOpened, setIsOpened] = useState(false);

  useEffect(() => {
    fetchUserData();
  }, [fetchUserData]);

  useEffect(() => {
    setIsDesktop(window.innerWidth >= 1024);
    setIsOpened(!isDesktop && isOpened);
  }, [isDesktop, isOpened]);

  const openNav = () => {
    setIsOpened(!isOpened);
  };

  const changeWindowSize = () => {
    setIsDesktop(window.innerWidth >= 1024);
  };

  const handleLogout = () => {
    const logoutWindow = window.open(loggingOutApi, "_self");
    const getUserDataInterval = setInterval(() => {
      if (logoutWindow.closed) {
        clearInterval(getUserDataInterval);
      }

      logoutWindow.postMessage("MadeByWebdevRistek2020", loggingOutApi);
    }, 1000);

    logout();
  };

  window.addEventListener("resize", changeWindowSize);

  const getSideBarMenu = () => {
    if (isUserMaba) {
      return NAVBAR_ITEMS_MABA;
    }
    return NAVBARS_ITEMS_NON_MABA;
  };

  const menuItems = Object.keys(profile).length !== 0 ? getSideBarMenu() : [];

  return (
    <StyledNavbarContainer isDesktop={isDesktop} profileImage={profile.photo}>
      <div className="navbar-container">
        <div className="left-container">
          <div className="logo-container">
            <a
              className="ristek-link"
              href="https://ristek.cs.ui.ac.id"
              target="__blank"
            >
              <div className="ristek-logo-container">
                <img
                  className="ristek-logo"
                  src={logoRistek}
                  alt="ristek logo"
                />
              </div>
            </a>
            <div className="pmb-logo-container" onClick={() => push("/")}>
              <img className="pmb-logo" src={logoPmb} alt="pmb logo" />
            </div>
          </div>
          {isDesktop ? (
            <div className="menu-container">
              {menuItems.map((item, index) =>
                item.name === "More" ? (
                  <div key={index} className="dropdown-wrapper menu-button">
                    <button className="more-button">
                      More&nbsp;
                      <img src={arrowDown} alt="down arrow" />
                    </button>
                    <div className="drop-menu">
                      <div className="white-bubble">
                        <Link
                          className="menu-button"
                          to={NAVBAR_ITEM_STATISTICS.url}
                        >
                          {NAVBAR_ITEM_STATISTICS.name}
                        </Link>
                        <Link
                          className="menu-button"
                          to={NAVBAR_ITEM_GALLERY.url}
                        >
                          {NAVBAR_ITEM_GALLERY.name}
                        </Link>
                      </div>
                    </div>
                  </div>
                ) : (
                  <Link key={index} to={item.url}>
                    {item.name}
                  </Link>
                )
              )}
            </div>
          ) : (
            <div />
          )}
        </div>
        <div className="right-container">
          <div className="config-container">
            <div className="notification-container">
              <img
                className="notification-image"
                src={iconBell}
                alt="notification icon"
              />
            </div>
            {isDesktop ? (
              <React.Fragment>
                <div className="profile-container">
                  <div className="profile-image" />
                  <div className="drop-down-profile" />
                </div>
                <div className="dropdown-wrapper menu-button dropdown-profile">
                  <button className="more-button more-button-profile">
                    <img src={arrowDown} alt="down arrow" />
                  </button>
                  <div className="drop-menu drop-menu-profile">
                    <div className="white-bubble">
                      <div className="profile-content">
                        <h2>
                          {Object.keys(profile).length !== 0 && profile.name}
                        </h2>
                        <p>
                          {Object.keys(profile).length !== 0 &&
                            `${batch.name} ${batch.year}`}
                        </p>
                      </div>
                      <Link
                        className="menu-button"
                        to={NAVBAR_ITEM_PROFILE.url}
                      >
                        {NAVBAR_ITEM_PROFILE.name}
                      </Link>
                      <button
                        className="menu-button logout"
                        onClick={() => handleLogout()}
                      >
                        Logout
                      </button>
                    </div>
                  </div>
                </div>
              </React.Fragment>
            ) : (
              <div className="notification-container">
                <img
                  className="hamburger icon"
                  src={iconHamburger}
                  alt="hamburger icon"
                  onClick={openNav}
                />
              </div>
            )}
          </div>
        </div>
      </div>
      {!isDesktop ? (
        <Sidenav
          isOpened={isOpened}
          setIsOpened={setIsOpened}
          userData={profile}
          userPhoto={profile.photo}
          isUserMaba={isUserMaba}
        />
      ) : (
        <div />
      )}
    </StyledNavbarContainer>
  );
};

Navbar.propTypes = {
  logout: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired,
  profile: PropTypes.shape().isRequired,
  fetchUserData: PropTypes.func.isRequired,
  // location: PropTypes.shape({
  //   pathname: PropTypes.string
  // }),
};

const mapStateToProps = (state) => ({
  profile: state.profileReducer.profile,
  isLoading: state.profileReducer.isLoading,
  isUserMaba: isMaba(state),
  batch: state.profileReducer.batch,
});

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logout()),
  push: (url) => dispatch(push(url)),
  fetchUserData: () => dispatch(fetchProfile()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);
