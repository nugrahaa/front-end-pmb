import styled from "styled-components";

export const StyledNavbarContainer = styled.div`
  a {
    cursor: pointer;
    text-decoration: none;
  }

  .navbar-container {
    width: 100%;
    display: flex;
    align-items: center;
    background: #f7fbff;
    padding: 0.3rem 0;
    z-index: 9999;
  }

  .left-container {
    display: flex;
    justify-content: flex-start;
    width: 75%;
  }

  .right-container {
    display: flex;
    justify-content: flex-end;
    width: 25%;
  }

  .logo-container {
    display: flex;
    justify-content: ${(props) => (props.isDesktop ? `center` : `flex-start`)};
    flex: 0.75;
    margin: 0 1rem;
  }

  .ristek-link {
    margin-top: 0.3rem;
  }

  .ristek-logo-container,
  .pmb-logo-container {
    display: flex;
    align-self: center;
    cursor: pointer;
  }

  .ristek-logo {
    max-width: 2.5rem;
    max-height: 2.5rem;
    margin-right: 1rem;
  }

  .pmb-logo {
    max-width: 7.25rem;
    max-height: 3.25rem;
  }

  .menu-container {
    width: 100%;
    margin: 0 1rem;
    height: 3.5rem;
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    overflow: visible;
    flex: 3;
  }

  .menu-container a {
    width: 100%;
    text-align: center;
    padding: 1rem 1rem;
    display: block;
    color: #222;
    font-size: 1.25rem;
  }

  .menu-container a:hover {
    color: rgba(27, 39, 75, 0.3);
  }

  .config-container {
    display: flex;
    justify-content: flex-end;
    flex: 0.75;
    overflow: visible;
    height: 3.5rem;
    width: 100%;
    margin: 0 3rem;

    .notification-container {
      display: flex;
      align-self: center;
      cursor: pointer;
    }

    .notification-image {
      height: auto;
      width: auto;
      max-width: 2.2rem;
      max-height: 2.2rem;
    }

    .profile-container {
      display: flex;
      justify-content: center;
    }

    .profile-image {
      align-self: center;
      width: 3rem;
      height: 3rem;
      border-radius: 50%;
      margin: 0 0.5rem 0 1rem;
      background-position: center;
      background-size: cover;
      box-shadow: 0 0.4rem 0.5rem -0.2rem #545454;
      background-image: url(${(props) => props.profileImage});
    }

    .dropdown-profile {
      width: 20%;
      z-index: 10;
    }

    .dropdown-profile a {
      width: 100%;
      margin: 0;
      padding: 0.75rem 0;
      display: block;
      color: #222;
      text-align: left;
      font-size: 1rem;
    }

    .drop-menu-profile {
      width: 16rem;
      transform: translate(-60%, 3%);

      a {
        font-size: 1rem;
        text-align: left;
        padding: 0.75rem 0rem;
      }

      a:hover {
        color: rgba(27, 39, 75, 0.3);
      }

      .logout {
        width: 100%;
        text-align: left;
        padding: 0.75rem 0rem;
        display: block;
        background: none !important;
        border: none;
        cursor: pointer;
        font-size: 1rem;
        color: red;
      }

      .logout:hover {
        color: rgba(255, 0, 0, 0.3);
      }

      .profile-content {
        margin-top: 14px;
        border-bottom: 1px solid #1b274b;

        h2 {
          font-weight: bold;
        }

        p {
          font-size: 0.9rem;
          margin: 6px 0 12px 0;
        }
      }
    }
  }

  .drop-menu {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 160%;
    transition: max-height 0.4s, opacity 0.5s;
    max-height: 0;
    opacity: 0;
    overflow: hidden;
    position: relative;
    z-index: 10;
    padding: 0 10px 10px 10px;
    transform: translateX(-24%);
    margin-top: -4px;
  }

  .drop-menu::before {
    content: "";
    transform: translate(140%, 0%);
    border: 0.75rem solid transparent;
    border-top: none;
    border-bottom-color: #ffffff;
  }

  .white-bubble {
    border-radius: 10px;
    background: white;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    width: 100%;
    padding: 0 10px;
  }

  .drop-menu a {
    font-size: 1rem;
    text-align: left;
    padding: 0.75rem 0;
  }

  .drop-menu a:first-child {
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
  }

  .drop-menu a:last-child {
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;

    img {
      max-width: 1rem;
      max-height: 1rem;
    }
  }

  .drop-menu a:not(:last-child) {
    border-bottom: 1px solid #1b274b;
  }

  .drop-menu > .menu-button {
    height: 100%;
    transition: transform 0.4s;
    transform: translateY(-200%);
    background: white;
  }

  .dropdown-wrapper:hover .drop-menu {
    max-height: 300px;
    opacity: 1;
  }

  .dropdown-wrapper:hover > .drop-menu .menu-button {
    transform: translateY(0%);
  }

  .more-button {
    display: flex;
    align-items: center;
    height: 95%;
    width: 100%;
    text-align: center;
    display: block;
    background: none !important;
    border: none;
    padding: 0 !important;
    cursor: pointer;
    font-size: 1.25rem;
  }

  .more-button-profile {
    text-align: left;
  }

  * {
    box-sizing: border-box;
    list-style-type: none;
    padding: 0;
    margin: 0;
    text-decoration: none;
    font-family: "Metropolis";
    color: #1b274b;
  }

  @media screen and (max-width: 73.75rem) {
    .navbar-container {
      height: 2.95rem;
      margin: 0.5rem 0;
    }

    .menu-container a {
      font-size: 1rem;
    }

    .more-button {
      height: 88%;
      font-size: 1rem;
    }

    .ristek-logo {
      max-width: 1.85rem;
      max-height: 1.85rem;
    }

    .pmb-logo {
      max-width: 5.25rem;
      max-height: 2.25rem;
    }

    .config-container {
      justify-content: ${(props) => (props.isDesktop ? `center` : `flex-end`)};
      margin: 0.5rem;

      .notification-image {
        max-width: 2rem;
        max-height: 2rem;
        margin-right: 0.75rem;
      }
    }

    .hamburger {
      width: 2rem;
      height: 2rem;
    }
  }
`;
