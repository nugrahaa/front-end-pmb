import styled from "styled-components";
import prevButton from "../../assets/img/prevButton.png";
import nextButton from "../../assets/img/nextButton.png";

export const CalendarCardStyle = styled.div`
  .CalendarCard__title {
    font-family: Metropolis;
    font-size: 1.25rem;
    color: #1b274b;
    margin-top: 0.9rem;
  }

  .CalendarCard__tile {
    color: "darkGray";
  }

  #Calendar {
    .react-calendar {
      border: none;
      font-family: inherit;
      width: 100%;
    }

    /* navigation */
    .react-calendar__navigation__label {
      font-size: 1rem;
      font-weight: bold;
      font-family: inherit;
      line-height: 1.33rem;
      text-align: center;
      color: #1b274b;
      padding: 0;
      flex-grow: 0 !important;
    }
    .react-calendar__navigation__prev2-button {
      display: none;
    }
    .react-calendar__navigation__next2-button {
      display: none;
    }
    .react-calendar__navigation button[disabled] {
      background-color: inherit;
    }
    .react-calendar__navigation__prev-button {
      background-image: url(${prevButton});
      background-repeat: no-repeat;
      background-position: 50% 50%;
      font-size: 0;
      padding: 0;
      margin-right: 1rem;
    }
    .react-calendar__navigation__next-button {
      background-image: url(${nextButton});
      background-repeat: no-repeat;
      background-position: 50% 50%;
      font-size: 0;
      padding: 0;
      margin-left: 1rem;
    }
    .react-calendar__navigation {
      justify-content: center;
    }

    /* days of week */
    abbr[title] {
      font-size: 0.875rem;
      text-decoration: none;
      text-align: center;
      font-weight: 500;
      color: #1b274b;
      text-transform: capitalize;
      font-family: inherit;
      color: #1b274b;
    }

    /* calendar day tile style */
    .react-calendar__tile {
      font-size: 0.875rem;
      text-align: center;
      font-family: inherit;
      box-sizing: border-box;
      display: flex;
      position: relative;
      align-items: center;
      padding: 0;
      height: 0;
      overflow: visible !important;
      /* ensure 1:1 ratio */
      padding-bottom: 14.2857%;
      border-radius: 50%;
      color: #1b274b;

      /* centers day vertically and horizontally */
      & > abbr {
        position: absolute;
        width: 100%;
        top: 50%;
        transform: translateY(-50%);
      }
    }

    /* calendar day tile today */
    .react-calendar__tile--now {
      background-color: inherit;

      & > abbr {
        color: #1b274b;
      }
    }

    /* calendar day tile selected */
    .react-calendar__tile--active,
    .react-calendar__tile:hover {
      /* very light blue */
      background-color: rgba(231, 246, 253, 0.8);

      & > abbr {
        color: #1b274b;
      }
    }

    /* calendar day tile with events */
    .CalendarCard__tile__activities abbr::after {
      content: "";
      position: absolute;
      left: 50%;
      top: 100%;
      transform: translateX(-50%);
      border-radius: 50%;
      width: 5px;
      height: 5px;
    }

    .event abbr::after {
      background-color: #4a69ca;
    }

    .task abbr::after {
      background-color: #ba4141;
    }

    .event__task abbr::after {
      background-image: linear-gradient(to right, #4a69ca 51%, #ba4141 50%);
    }

    /* custom tile content for tooltip */
    .Calendar__tileContent {
      position: absolute;
      width: 100%;
      height: 100%;
    }

    @media (max-width: 960px) {
      .react-calendar__month-view {
        font-size: 0.75rem;
      }
    }

    .Calendar__Tooltip__messages {
      display: flex;
      flex-direction: column;
      justify-content: flex-start;

      & > * {
        display: inline-block;
        text-align: left;
      }
    }
  }
`;

export const TooltipStyle = styled.div`
  .Tooltip {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    font-family: inherit;
  }

  .Tooltip__trigger {
    display: inline-block;
    text-decoration: underline;
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
  }

  .Tooltip__bubble {
    position: absolute;
    z-index: 10;
    visibility: hidden;
    max-width: ${(props) => !props.isMobile && "30em"};
  }

  .Tooltip__top {
    bottom: 100%;
    left: 50%;
    transform: translateX(-50%);
  }

  .Tooltip__message {
    width: 100%;
    color: black;
    font-size: 0.875rem;
    text-align: center;
    position: relative;
    box-sizing: border-box;
    background-color: white;
    border-radius: 1rem;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    padding: 0.75rem;
    a {
      color: inherit;
      text-decoration: none;
      white-space: nowrap;
      &:hover {
        text-decoration: underline;
      }
    }
  }

  .Tooltip__message_fetch {
    left: 50%;
    transform: translateX(-50%);
    visibility: visible;
    color: black;
    font-size: 0.875rem;
    text-align: center;
    position: relative;
    max-width: 100px;
    background-color: white;
    border-radius: 1rem;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    padding: 0.75rem;
    a {
      color: inherit;
      text-decoration: none;

      &:hover {
        text-decoration: underline;
      }
    }
  }

  .arrow {
    content: "";
    visibility: hidden;
    position: absolute;
    width: 0;
    height: 0;
    left: 50%;
    transform: translateX(-50%) rotate(180deg);
    border: 0.75rem solid transparent;
    border-top: none;
    border-bottom-color: white;
    z-index: 11;
  }
`;
