import moment from "moment-timezone";

export default () => moment().tz("Asia/Jakarta").format();
