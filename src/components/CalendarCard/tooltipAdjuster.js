const isOutToRight = (correction, lastPosition, innerWidth) => {
  return correction + lastPosition > innerWidth;
};

const isOutToLeft = (correction, position) => {
  return correction > position;
};

export function horizontalAdjuster(
  horizontalCalculated,
  setHorizontalCalculated
) {
  if (!horizontalCalculated) {
    // Get active tooltip
    let tooltip = document.getElementsByClassName("Tooltip__message")[0];

    // Get tooltip coordinate and size
    if (tooltip) {
      const tooltipOuter = document.getElementsByClassName("Tooltip__top")[0];
      let tooltipRect = tooltip.getBoundingClientRect();
      const positionSumWidth = tooltipRect.x + tooltipRect.width;
      const link = tooltip.getElementsByTagName("a");
      let correction = 0;

      if (tooltipRect.x < 0) {
        // Out on the left
        correction = Math.ceil(Math.abs(tooltipRect.x));

        if (
          isOutToRight(correction, positionSumWidth, document.body.clientWidth)
        ) {
          tooltipOuter.style.left = "50%";
          tooltip.style.width = document.body.clientWidth + "px";
          tooltip = document.getElementsByClassName("Tooltip__message")[0];
          tooltipRect = tooltip.getBoundingClientRect();
          correction = -tooltipRect.x;
          for (const item of link) {
            item.style.whiteSpace = "normal";
          }
        }
        tooltipOuter.style.left = `calc(50% + ${correction}px)`;
      } else if (positionSumWidth > document.body.clientWidth) {
        // Out on the right
        correction = positionSumWidth - document.body.clientWidth;
        tooltipOuter.style.left = `calc(50% - ${correction}px)`;

        if (isOutToLeft(correction, tooltipRect.x)) {
          tooltipOuter.style.left = "50%";
          tooltip.style.width = document.body.clientWidth + "px";
          tooltip = document.getElementsByClassName("Tooltip__message")[0];
          tooltipRect = tooltip.getBoundingClientRect();
          correction = -tooltipRect.x;

          for (const item of link) {
            item.style.whiteSpace = "normal";
          }
          tooltipOuter.style.left = `calc(50% + ${correction}px)`;
        }
      }
      setHorizontalCalculated(true);
    }
  } else {
    setHorizontalCalculated(false);
  }
}

export function verticalAdjuster(
  verticalCalculated,
  horizontalCalculated,
  setVerticalCalculated
) {
  if (!verticalCalculated && horizontalCalculated) {
    // Get active tooltip
    const tooltip = document.getElementsByClassName("Tooltip__message")[0];

    // Get tooltip coordinate and size
    if (tooltip) {
      const arrow = document.getElementsByClassName("arrow")[0];
      const tooltipOuter = document.getElementsByClassName("Tooltip__top")[0];
      const tooltipRect = tooltip.getBoundingClientRect();

      if (tooltipRect.y < 0) {
        // Out on the top
        const tooltipCalendarTile = tooltip.parentElement.parentElement;
        const tooltipCalendarTileRect = tooltipCalendarTile.getBoundingClientRect();
        tooltipOuter.style.top = -tooltipCalendarTileRect.y + 42 + "px";
        tooltip.style.visibility = "visible";
      } else {
        tooltip.style.visibility = "visible";
        arrow.style.visibility = "visible";
      }
      setVerticalCalculated(true);
    }
  } else {
    setVerticalCalculated(false);
  }
}
