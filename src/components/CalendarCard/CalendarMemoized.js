import React, { useState, useEffect } from "react";
import moment from "moment-timezone";
import Calendar from "react-calendar";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import "react-calendar/dist/Calendar.css";
import PropTypes from "prop-types";
import CalendarTooltip from "../CalendarTooltip";
import Loading from "../Loading";
import { isEventsLoading } from "../../selectors/events";
import Emoji from "../../components/Emoji";
import styles from "../../utils/styles";

//reference PMB 2019

function calendarPropsAreEqual(prevProps, nextProps) {
  return (
    prevProps.date === nextProps.date &&
    prevProps.activities === nextProps.activities
  );
}

// month year format at top of calendar
function formatDate(date) {
  return date.toLocaleDateString("default", { month: "long", year: "numeric" });
}

// format event time for tooltip
function formatTime(date) {
  return date.toLocaleTimeString("default", {
    hour: "2-digit",
    minute: "2-digit",
    hour12: false,
  });
}

function getActivities(dateUnix, activities) {
  const event = activities.get(`${dateUnix};event`) || [];
  const task = activities.get(`${dateUnix};task`) || [];
  return [...event, ...task];
}

function checkActivitiesType(dateUnix, activities) {
  let activityType = "";
  if (activities.get(`${dateUnix};event`)) {
    activityType = "event";
  }
  if (activities.get(`${dateUnix};task`)) {
    activityType += "task";
  }
  return activityType;
}

// uses npm package https://www.npmjs.com/package/react-calendar
const CalendarMemoized = React.memo((props) => {
  const [date, setDate] = useState(new Date());
  const isLoadingEvents = useSelector((state) => isEventsLoading(state));

  useEffect(() => {
    if (props.date) {
      setDate(new Date(props.date));
    } else {
      setDate(new Date());
    }
  }, [props.date]);

  return (
    <div id="Calendar">
      {isLoadingEvents ? (
        <Loading />
      ) : (
        <Calendar
          value={date}
          onChange={(dateSelected) => setDate(dateSelected)}
          minDetail="month"
          maxDetail="month"
          showNeighboringMonth={false}
          formatMonthYear={(locale, date) => formatDate(date)}
          tileContent={(tile) => {
            const activities = getActivities(
              moment(tile.date).unix(),
              props.activities
            );

            return activities.length === 0 ? (
              <></>
            ) : (
              <CalendarTooltip
                message={
                  <div className="Calendar__Tooltip__messages">
                    {activities.map((activity) => {
                      return (
                        <Link
                          key={activity.link}
                          to={activity.link}
                          style={{ paddingBottom: 8 }}
                        >
                          <div
                            style={{
                              fontWeight: "bold",
                              paddingBottom: 3,
                            }}
                          >
                            [{formatTime(new Date(activity.date))}]{" "}
                          </div>
                          <div
                            style={{
                              ...styles.flexDirRow,
                              alignItems: "center",
                            }}
                          >
                            <div
                              style={{
                                minWidth: 15,
                                minHeight: 15,
                                ...styles.prxs,
                                ...styles.mtxs,
                              }}
                            >
                              {activity.activityType === "event" ? (
                                <Emoji emoji="📅" />
                              ) : (
                                <Emoji emoji="📝" />
                              )}
                            </div>
                            <div
                              style={{
                                overflow: "hidden",
                                textOverflow: "ellipsis",
                              }}
                            >
                              {activity.title}
                            </div>
                          </div>
                        </Link>
                      );
                    })}
                  </div>
                }
              >
                <div className="Calendar__tileContent" />
              </CalendarTooltip>
            );
          }}
          tileClassName={(tile) => {
            let activitiesOfDay = checkActivitiesType(
              moment(tile.date).unix(),
              props.activities
            );

            return activitiesOfDay === ""
              ? "CalendarCard__tile"
              : activitiesOfDay === "event"
              ? "CalendarCard__tile CalendarCard__tile__activities event"
              : activitiesOfDay === "task"
              ? "CalendarCard__tile CalendarCard__tile__activities task"
              : "CalendarCard__tile CalendarCard__tile__activities event__task";
          }}
        />
      )}
    </div>
  );
}, calendarPropsAreEqual);

CalendarMemoized.propTypes = {
  date: PropTypes.string,
  activities: PropTypes.object,
};

export default CalendarMemoized;
