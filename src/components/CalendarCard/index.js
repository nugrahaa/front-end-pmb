import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment-timezone";
import PropTypes from "prop-types";

import CalendarMemoized from "./CalendarMemoized";
import Card from "../Card";
import ServerTime from "../ServerTime";
import { CalendarCardStyle } from "./style";
import { getTime } from "../../selectors/user";
import { getServerTime } from "../../globalActions";
import Emoji from "../Emoji";
import styles from "../../utils/styles";

function CalendarCard(props) {
  const [dateTime, setLocalTime] = useState(null);
  const [timeFormatted, setTimeFormatted] = useState(null);

  const serverTime = useSelector((state) => getTime(state));

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getServerTime());
  }, [dispatch]);

  useEffect(() => {
    if (serverTime) {
      setLocalTime(moment(serverTime));
    }
  }, [serverTime]);

  useEffect(() => {
    if (dateTime) {
      dateTime.add(1.1, "second");
      setTimeFormatted(dateTime.format("ddd HH:mm:ss"));
      const timer = setInterval(() => {
        setTimeFormatted(() => {
          dateTime.add(1, "second");
          return dateTime.format("ddd HH:mm:ss");
        });
      }, 1000);

      return () => {
        clearInterval(timer);
      };
    }
  }, [dateTime]);

  return (
    <CalendarCardStyle>
      <h2>Calendar</h2>
      <Card
        containerStyle={{
          padding: "15px",
          marginTop: "0.9em",
          alignItems: "flex-start",
        }}
      >
        <ServerTime dateTime={timeFormatted} />
        <CalendarMemoized
          date={dateTime && dateTime.format("YYYY/MM/DD")}
          activities={props.activities}
        />
        <div style={{ ...styles.flexDirCol, ...styles.pas }}>
          <div style={{ ...styles.flexDirRow }}>
            <div style={{ width: 15, height: 15, ...styles.prs }}>
              <Emoji emoji="📅" />
            </div>
            = Event
          </div>
          <div style={{ ...styles.flexDirRow }}>
            <div style={{ width: 15, height: 15, ...styles.prs }}>
              <Emoji emoji="📝" />
            </div>
            = Task
          </div>
        </div>
      </Card>
    </CalendarCardStyle>
  );
}

CalendarCard.propTypes = {
  activities: PropTypes.object,
};

export default CalendarCard;
