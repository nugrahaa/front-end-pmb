import styled from "styled-components";

export const TaskListCardStyle = styled.div`
  .TaskListCard {
    background: #ffffff;
    mix-blend-mode: normal;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    border-radius: 10px;
    padding-bottom: 20px;
    width: 100%;
    min-height: auto;
  }

  .TaskListCard_title {
    font-family: Metropolis;
    font-style: normal;
    font-weight: bold;
    font-size: 1.25rem;
    line-height: 16px;

    color: #4a69ca;

    padding-left: 20px;
    padding-top: 20px;
  }

  .TaskListCard_date {
    margin: 0 20px;
  }

  .TaskListCard_date_icon {
    float: left;
    margin-right: 0.5rem;
  }

  .TaskListCard_date_text {
    font-family: Metropolis;
    font-style: normal;
    font-weight: normal;
    font-size: 1rem;
    line-height: 16px;

    color: #1b274b;
  }

  .TaskListCard_status {
    margin-top: 10px;
    margin-left: 20px;
  }

  .TaskListCard_status_text_red {
    font-family: Metropolis;
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 16px;

    color: #c81b1b;
  }

  .TaskListCard_status_text_green {
    font-family: Metropolis;
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 16px;

    color: #32c64a;
  }

  .TaskListCard_button {
    padding-right: 20px;
    text-align: right;
  }

  @media (min-width: 481px) and (max-width: 767px) {
    .TaskListCard {
      width: 100%;
      min-height: auto;
    }
  }

  @media (min-width: 321px) and (max-width: 480px) {
    .TaskListCard {
      width: 100%;
      min-height: auto;
    }
  }

  @media (max-width: 320px) {
    .TaskListCard {
      width: 100%;
      min-height: auto;
    }
  }

  @media screen and (max-width: 73.75rem) {
    .TaskListCard_date_text {
      font-size: 0.75rem;
    }

    .TaskListCard_title {
      font-size: 1rem;
    }
  }
`;
