import React from "react";
import PropTypes from "prop-types";

import { TaskListCardStyle } from "./style";

import TaskDateIcon from "../../assets/icons/task-date-icon.svg";

import { timestampParser } from "../../utils/timestampParser";

import Button from "../../components/Button";

import { push } from "connected-react-router";
import { connect } from "react-redux";

function TaskListCard({ id, title, date, submissionStatus, onOpen }) {
  const newDate = timestampParser(new Date(date));

  const handleOpenTask = (id) => {
    onOpen(id);
  };

  return (
    <TaskListCardStyle>
      <div className="TaskListCard">
        <h1 className="TaskListCard_title">{title}</h1>
        <div className="TaskListCard_date">
          <img
            className="TaskListCard_date_icon"
            src={TaskDateIcon}
            alt="Calendar Icon"
          />
          <p className="TaskListCard_date_text">{newDate}</p>
        </div>
        {submissionStatus ? (
          <div className="TaskListCard_status">
            <p className="TaskListCard_status_text_green">Submitted</p>
          </div>
        ) : (
          <div className="TaskListCard_status">
            <p className="TaskListCard_status_text_red">Not Submitted</p>
          </div>
        )}
        <div className="TaskListCard_button">
          <div
            style={{ display: "inline-block" }}
            onClick={() => handleOpenTask(id)}
          >
            <Button>
              <div
                style={{
                  textDecoration: "none",
                  color: "inherit",
                  height: "100%",
                  width: "100%",
                  textAlign: "center",
                  margin: 0,
                }}
              >
                Details
              </div>
            </Button>
          </div>
        </div>
      </div>
    </TaskListCardStyle>
  );
}

TaskListCard.propTypes = {
  title: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  submissionStatus: PropTypes.bool.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  push: (url, data) => dispatch(push(url, { ...data })),
});

export default connect(null, mapDispatchToProps)(TaskListCard);
