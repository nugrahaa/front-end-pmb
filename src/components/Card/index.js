import React from "react";
import { Base } from "./style";

const Card = ({ children, containerStyle }) => {
  return <Base style={{ ...containerStyle }}>{children}</Base>;
};

export default Card;
