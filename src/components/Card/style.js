import styled from "styled-components";

export const Base = styled.div`
  background: #ffffff;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: 10px;
  font-family: "Metropolis";

  h1 {
    font-size: 1.25em;
    color: "#1B274B";
  }

  p,
  ol,
  li {
    color: #000000;
    font-size: 0.875em;
  }
`;
