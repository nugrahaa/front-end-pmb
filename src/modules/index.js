/* eslint-disable */
import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import taskPageReducer from "../container/TaskListPage/reducer";
import announcementPageReducer from "../container/AnnouncementPage/reducer";
import eventPageReducer from "../container/EventPage/reducer";
import eventDetailPageReducer from "../container/EventDetailPage/reducer";
import puzzleReducer from "../container/Puzzle/reducer";
import globalReducer from "../globalReducer";
import profilePageReducer from "../container/ProfilePage/reducer";
import searchMahasiswaReducer from "../container/SearchMahasiswaPage/reducer";
import editInterestPageReducer from "../container/EditInterestPage/reducer";
import {
  fetchTokenReducer,
  checkTokenReducer,
} from "../container/TokenGenerator/reducer";
import postTokenReducer from "../container/TokenInput/reducer";
import {
  getKenalanByIdReducer,
  patchKenalanByIdReducer,
} from "../container/FormFriendPage/reducer";
import taskDetailPageReducer from "../container/TaskDetailPage/reducer";
import friendPageReducer from "../container/FriendPage/reducer";

export default (history) =>
  combineReducers({
    router: connectRouter(history),
    taskPage: taskPageReducer,
    eventReducer: eventPageReducer,
    eventDetailReducer: eventDetailPageReducer,
    announcementReducer: announcementPageReducer,
    puzzleReducer: puzzleReducer,
    searchMahasiswaReducer: searchMahasiswaReducer,
    profileReducer: profilePageReducer,
    editInterestPageReducer: editInterestPageReducer,
    fetchTokenReducer: fetchTokenReducer,
    checkTokenReducer: checkTokenReducer,
    postTokenReducer: postTokenReducer,
    getKenalanByIdReducer: getKenalanByIdReducer,
    patchKenalanByIdReducer: patchKenalanByIdReducer,
    fetchTokenReducer: fetchTokenReducer,
    checkTokenReducer: checkTokenReducer,
    postTokenReducer: postTokenReducer,
    getKenalanByIdReducer: getKenalanByIdReducer,
    patchKenalanByIdReducer: patchKenalanByIdReducer,
    friendReducer: friendPageReducer,
    global: globalReducer,
    taskDetailPageReducer: taskDetailPageReducer,
  });
