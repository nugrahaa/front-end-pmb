/* eslint-disable sort-keys, valid-jsdoc */
/*
 * Actions describe changes of state in your application
 */
import auth from "./auth";
import axios from "axios";
import { push } from "connected-react-router";

import {
  SET_AUTH,
  SET_USER,
  SET_USER_SUCCESS,
  SET_USER_FAILURE,
  SENDING_REQUEST,
  REQUEST_ERROR,
  CLEAR_ERROR,
  SET_SERVER_TIME,
} from "./globalConstants";

import { fetchServerTime, fetchProfileApi } from "./api";

export function login(user) {
  return (dispatch) => {
    dispatch(sendingRequest(true));

    dispatch(setUser());
    dispatch(setAuthState(true));

    auth.login(user);
    dispatch(sendingRequest(false));
    dispatch(push("/"));
  };
}

export function logout() {
  return (dispatch) => {
    dispatch(sendingRequest(true));

    auth.logout();

    dispatch(setAuthState(false));
    dispatch(setUser({}));
    dispatch(sendingRequest(false));
  };
}

export function setLoginData(user) {
  return (dispatch) => {
    dispatch(setUser());
    dispatch(setAuthState(true));
  };
}

/**
 * Sets the authentication state of the application
 * @param  {boolean} newAuthState True means a user is logged in, false means no user is logged in
 */
export function setAuthState(newAuthState) {
  return {
    type: SET_AUTH,
    newAuthState,
  };
}

export function setUser() {
  return (dispatch) => {
    dispatch({
      type: SET_USER,
    });
    axios
      .get(fetchProfileApi)
      .then((response) => {
       
        dispatch({
          user: response.data,
          type: SET_USER_SUCCESS,
        });
        if (
          (response.data.birth_place === '' ||
            response.data.birth_date === '' ||
            response.data.high_school === ''||
            response.data.line_id === ''||
            (response.data.is_maba && response.data.kelompok === null)) ) {
              dispatch(push("/edit-profile"));
            }
      })
      .catch((error) => {
        dispatch({
          payload: error,
          type: SET_USER_FAILURE,
        });
      });
  };
}

/**
 * Sets the `currentlySending` state, which displays a loading indicator during requests
 * @param  {boolean} sending True means we're sending a request, false means we're not
 */
export function sendingRequest(sending) {
  return {
    type: SENDING_REQUEST,
    sending,
  };
}

/**
 * Sets the `error` state to the error received
 * @param  {object} error The error we got when trying to make the request
 */
export function requestError(error) {
  return {
    type: REQUEST_ERROR,
    error,
  };
}

/**
 * Sets the `error` state as empty
 */
export function clearError() {
  return { type: CLEAR_ERROR };
}

export function setServerTime(payload) {
  return {
    type: SET_SERVER_TIME,
    payload,
  };
}

export function getServerTime() {
  return (dispatch) => {
    dispatch(sendingRequest(true));
    axios
      .get(fetchServerTime)
      .then((response) => {
        dispatch(setServerTime(response.data.server_time));
      })
      .catch((error) => {
        dispatch(requestError(error.response));
      })
      .finally(dispatch(sendingRequest(false)));
  };
}
