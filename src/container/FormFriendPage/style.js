import styled from 'styled-components';

export const Container = styled.div`
    display:flex;
    flex-direction:column;
`;

export const WrapperTitle = styled.div`
    h3{
        font-style: normal;
        font-weight: normal;
        font-size: 1.3em;
        line-height: 20px;
    }
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: start;
  align-items: center;
  width:50%;
  margin-right: 2em;

  img {
    margin-right: 0.5em;
    width:22px;
  }

  h5 {
    font-weight: normal;
    margin:0.5rem;
    overflow-wrap:anywhere;
  }
`;

export const WrapperCard = styled.div`
    background: #FFFFFF;
    mix-blend-mode: normal;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    border-radius: 10px;

    padding: 2rem;
    @media only screen and (max-width:410px){
      width: 330px;
      display: block;
      margin-left: auto;
      margin-right: auto;
      padding: 0.7rem;
    }

    @media only screen and (max-width:375px){
      font-size: 14px;
      width: 285px;
    }

    @media only screen and (max-width:336px){
      width: 265px;
    }
`;

export const Profile = styled.div`
  display: flex;
  flex-direction: row;

  img.avatar {
    width: 120px;
    height: 120px;
    border-radius: 50%;
  }

  @media only screen and (max-width: 767px) {
    margin-top: 1em;
  }

  @media only screen and (max-width:375px){
    img.avatar {
      width: 90px;
      height: 90px;
    }
  }
`;

export const Detail = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 1.75em;
  justify-content: center;
  width:100%;

  h4 {
    margin: 0;
    font-weight:bold;
  }

  @media only screen and (max-width:425px){
    font-size: 11px;
  }
`;

export const DetailWrapper = styled.div`
  display: flex;
  flex-direction: row;


  @media only screen and (max-width: 767px) {
    div {
      width: 100%
    }
    h5 {
      margin: 1em 0em 0em 0em;
    }
    img {
      margin: 1em 0.5em 0em 0em;
    }
  }

  @media only screen and (max-width: 628px) {
    flex-direction: column;
  }
`;

export const Interests = styled.div`
  margin-top: 1.75em;
  display: flex;
  flex-direction: column;
  h5 {
    font-weight: bold;
    margin: 0;
  }
`;

export const InterestItemWrapper = styled.div`
  margin-right: 0.75em;
  margin-bottom: 0.75em;
  background: #e7f6fd;
  opacity: 0.8;
  width: fit-content;
  height: fit-content;
  border-radius: 20px;
  h6 {
    margin: 0.5em 1em 0.5em 1em;
    color: black;
    font-weight: normal;
  }
`;

export const ItemContainer = styled.div`
  margin-top: 1rem;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const WrapperDescription = styled.div`
    h5 {
        font-weight: bold;
        margin: 0 0 1rem 0;
    }
`;

export const WrapperAction = styled.div`
    margin-top:1.5rem;
    display:flex;
    flex-direction:row;
    justify-content:flex-end;

    @media only screen and (max-width: 425px) {
      // justify-content: center;
      .button-action {
        font-size: 11px;
        padding: 0;
        width: 80px;
        height: 26px;
      }
    }
`;