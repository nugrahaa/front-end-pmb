import axios from "axios";
import {
  GET_KENALAN_BYID_SUCCESS,
  GET_KENALAN_BYID_FAILED,
  GET_KENALAN_BYID,
  PATCH_KENALAN_BYID_SUCCESS,
  PATCH_KENALAN_BYID_FAILED,
  PATCH_KENALAN_BYID
} from "./constants";

import { getKenalanByIdApi, patchKenalanByIdApi } from "../../api";
import { push } from "connected-react-router";



export function getKenalanById(id) {

  return (dispatch) => {

    dispatch({ type: GET_KENALAN_BYID });
    axios
      .get(getKenalanByIdApi(id))
      .then((response) => {
        // console.log(response.data)
       
       
        dispatch({
          payload: response.data,
          type: GET_KENALAN_BYID_SUCCESS,
        });
      })
      .catch((error) => {
        
        dispatch({
          payload: error,     
          type: GET_KENALAN_BYID_FAILED,
        });
      });
  };
}

export function patchKenalanById(id, data) {

  return (dispatch) => {

    dispatch({ type: PATCH_KENALAN_BYID });
    axios
      .patch(patchKenalanByIdApi(id),data)
      .then((response) => {
        dispatch(push(`/friends`));
        dispatch({
          payload: response.data,
          type: PATCH_KENALAN_BYID_SUCCESS,
        });
      })
      .catch((error) => {
        alert("Failed" ,"error")
        dispatch({
          payload: error,     
          type: PATCH_KENALAN_BYID_FAILED,
        });
      });
  };
}