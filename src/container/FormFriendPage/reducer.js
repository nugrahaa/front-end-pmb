// import { fromJS } from "immutable";
import {
    GET_KENALAN_BYID,
    GET_KENALAN_BYID_SUCCESS,
    GET_KENALAN_BYID_FAILED,
    PATCH_KENALAN_BYID,
    PATCH_KENALAN_BYID_FAILED,
    PATCH_KENALAN_BYID_SUCCESS
  
  } from "./constants";
  
  const initialState = {
    data: [],
    error: null,
    isLoaded: false,
    isLoading: false,
  };
  
  function getKenalanByIdReducer(state = initialState, action) {
    switch (action.type) {
      case GET_KENALAN_BYID:
        
        return {
          ...state,
          isLoading: true,
        };
  
      case GET_KENALAN_BYID_SUCCESS:
       
        return {
          ...state,
          data: action.payload,
          isLoading: false,
          isLoaded: true,
        };
  
      case GET_KENALAN_BYID_FAILED:
       
        return {
          ...state,
          data:action.payload,
          error: action.payload,
          isLoading: false,
          isLoaded: false,
        };
      
      default:
        return state;
    }
  }
  function patchKenalanByIdReducer(state = initialState, action) {
    switch (action.type) {
      case PATCH_KENALAN_BYID:
        
        return {
          ...state,
          isLoading: true,
        };
  
      case PATCH_KENALAN_BYID_SUCCESS:
       
        return {
          ...state,
          data: action.payload,
          isLoading: false,
          isLoaded: true,
        };
  
      case PATCH_KENALAN_BYID_FAILED:
       
        return {
          ...state,
          data:action.payload,
          error: action.payload,
          isLoading: false,
          isLoaded: false,
        };
      
      default:
        return state;
    }
  }
  
  
  
  export { getKenalanByIdReducer, patchKenalanByIdReducer};
  