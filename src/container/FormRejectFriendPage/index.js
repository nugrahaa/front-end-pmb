import React,{useState,useEffect} from 'react';
import { Helmet } from 'react-helmet';
import {
  Container,
  Profile,
  WrapperFeedback,
  WrapperAction,
  Detail,
  DetailWrapper,
  Wrapper,
  WrapperTitle,
  WrapperCard,
  InterestItemWrapper,
  ItemContainer,
  Interests,
  WrapperDescription
} from './style.js';
import Loading from "../../components/Loading";
import Email from "../../assets/icons/email.svg"
import Education from "../../assets/icons/education.svg";
import LineMediaSocial from "../../assets/icons/line.svg";
import Web from "../../assets/icons/web.svg";
import Birth from "../../assets/icons/birth.svg";
import School from "../../assets/icons/school.svg";
import TextArea from "../../components/TextArea";
import Button from "../../components/Button";
import { connect } from "react-redux";
import { getKenalanById,patchKenalanById} from "../FormFriendPage/actions";

const DetailUser = ({ icon, text }) => {
  
  return (
    <Wrapper>
      <img src={icon} alt="icon"></img>
      <h5>{text}</h5>
    </Wrapper>
  );
};

const InterestItem = ({ text }) => {
    return (
      <InterestItemWrapper>
        <h6>{text}</h6>
      </InterestItemWrapper>
    );
  };

const FormRejectFriendPage = ({ data, getKenalanById, patchKenalanById, isLoading, submitLoading,...props })=>{
  const [feedback, setFeedback]= useState('')
  const idKenalan = props.match.params.id
  useEffect(()=>{
    getKenalanById(idKenalan);
  },[getKenalanById, idKenalan])

  useEffect(()=>{
    setFeedback(data.rejection_message);
  },[data])

  const handleSubmit = ()=>{
    
   
      patchKenalanById(idKenalan,{
        "rejection_message": feedback,
        "is_approved":false
      })

    
  }
    return(
      <Container>
        <Helmet>
          <title>PMB 2020 - Reject Friend</title>
        </Helmet>
        <WrapperTitle>
            <h3>Reject Friend Request</h3>
        </WrapperTitle>
        {
          isLoading ? 
          <Loading/>:
          <WrapperCard>
          <Profile>
        <img
          className="avatar"
          alt="avatar"
          src={data?.user_elemen?.profile.photo || "https://github.com/identicons/jonathanfilbert.png"}
        ></img>
        <Detail>
          <h4>{data?.user_elemen?.profile.name || "-"}</h4>
          <DetailWrapper>
            <DetailUser
              icon={Education}
              text={data?.user_elemen?.profile.major || "-"}
            />
            <DetailUser
              icon={Birth}
              text={data?.user_elemen?.profile.birth_date !==null  ? new Date(data?.user_elemen?.profile.birth_date ).toDateString() : "-"}
            />
            <DetailUser
              icon={LineMediaSocial}
              text={data?.user_elemen?.profile.line_id || "-"}
            />
          </DetailWrapper>
          <DetailWrapper>
            <DetailUser
              icon={School}
              text={data?.user_elemen?.profile.high_school || "-"}
            />
            <DetailUser
              icon={Email}
              text={data?.user_elemen?.profile.email || "-"}
            />
            <DetailUser
              icon={Web}
              text={data?.user_elemen?.profile.website || "-"}
            />
          </DetailWrapper>
        </Detail>
      </Profile>
      <Interests>
        <h5>Top Interest</h5>
        <ItemContainer>
          {
            data?.user_elemen?.profile.interests.length ===0 ?
 
             <h5 style={{marginLeft:'1rem',marginBottom:'1rem'}}>-</h5>
  
            :

            data?.user_elemen?.profile.interests.map((el, idx)=>{
              return(
                <InterestItem text={el.interest_name} key={idx} />
              )
            }) 
          }
           
          
        </ItemContainer>
      </Interests>
          <WrapperDescription>
            <h5>Description</h5>
            <h6 style={{marginLeft:'1rem'}}>
              {data?.description}
            </h6>
          </WrapperDescription>
            <WrapperFeedback>
              <h5>Feedback</h5>
              <TextArea
                type="text"
                name="feedback"
                value={feedback}
                onChange={(e)=>setFeedback(e.target.value)}
                rows="8">
              </TextArea>
            </WrapperFeedback>
            <WrapperAction>
                <Button textColor="#4A69CA" borderColor="#4A69CA" onClick={handleSubmit}>
                    Send
                </Button>
            </WrapperAction>
        </WrapperCard>
      

        }
       </Container>
    )
}

const mapStateToProps = (state) => ({
  data: state.getKenalanByIdReducer.data,
  isLoading: state.getKenalanByIdReducer.isLoading,
  submitLoading:state.patchKenalanByIdReducer.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
 getKenalanById: (id) => dispatch(getKenalanById(id)),   
 patchKenalanById: (id,data) => dispatch(patchKenalanById(id,data)), 
});

export default connect(mapStateToProps, mapDispatchToProps)(FormRejectFriendPage);

