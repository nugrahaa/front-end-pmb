/* eslint-disable array-callback-return */
export const processInterest = (interests) => {
  let result = [];
  let technologyData = {
    name: "Technology",
    data: [],
  };
  let nonTechnologyData = {
    name: "Non-Technology",
    data: [],
  };
  let entertainmentData = {
    name: "Entertainment",
    data: [],
  };
  interests.map((interest) => {
    if (interest.interest_category === "Technology") {
      technologyData.data.push(interest.interest_name);
    } else if (interest.interest_category === "Non-Tech") {
      nonTechnologyData.data.push(interest.interest_name);
    } else if (interest.interest_category === "Entertainment") {
      entertainmentData.data.push(interest.interest_name);
    }
  });
  result.push(technologyData);
  result.push(nonTechnologyData);
  result.push(entertainmentData);
  return result;
};
