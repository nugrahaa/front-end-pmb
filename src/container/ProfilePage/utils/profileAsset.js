import React from "react";
import ProfileBirthdayAsset from "../assets/ProfileBirthdayAsset";
import ProfileLineAsset from "../assets/ProfileLineAsset";
import ProfileEmailAsset from "../assets/ProfileEmailAsset";
import ProfileSchoolAsset from "../assets/ProfileSchoolAsset";
import ProfileProdiAsset from "../assets/ProfileProdiAsset";
import ProfileWebsiteAsset from "../assets/ProfileWebsiteAsset";
import ProfilePrivateAsset from "../assets/ProfilePrivateAsset";

const ProfileAssets = {
  birthday: <ProfileBirthdayAsset />,
  line: <ProfileLineAsset />,
  email: <ProfileEmailAsset />,
  highschool: <ProfileSchoolAsset />,
  prodi: <ProfileProdiAsset />,
  website: <ProfileWebsiteAsset />,
  private: <ProfilePrivateAsset />,
};

export const getProfileAssetByType = (type) => ProfileAssets[type];
