import axios from "axios";

import {
  FETCH_PROFILE,
  FETCH_PROFILE_SUCCESS,
  FETCH_PROFILE_FAILED,
} from "./constants";

import { fetchProfileApi } from "../../api";

export const fetchProfile = () => {
  return (dispatch) => {
    dispatch({
      type: FETCH_PROFILE,
    });
    axios
      .get(fetchProfileApi)
      .then((response) => {
        dispatch({
          payload: response.data,
          type: FETCH_PROFILE_SUCCESS,
        });
      })
      .catch((error) => {
        dispatch({
          payload: error,
          type: FETCH_PROFILE_FAILED,
        });
      });
  };
};
