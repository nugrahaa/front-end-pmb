import {
  FETCH_PROFILE,
  FETCH_PROFILE_SUCCESS,
  FETCH_PROFILE_FAILED,
  UPDATE_PROFILE,
  UPDATE_PROFILE_SUCCESS,
  UPDATE_PROFILE_FAILED,
  UPLOAD_PHOTO,
} from "./constants";
import { processInterest } from "./utils/processInterest";

const initialState = {
  error: null,
  isLoading: false,
  isUserMaba: false,
  profile: {},
  about: "",
  batch: {},
  group: {
    big_group: {
      name: "",
    },
    small_group: {
      name: "",
    },
  },
  interests: [],
};

const profilePageReducer = (state = initialState, action) => {
  // console.log("ini profile", action.payload);
  switch (action.type) {
    case UPLOAD_PHOTO:
      return { ...state, isLoading: true };
    case FETCH_PROFILE:
      return { ...state, isLoading: true };
    case FETCH_PROFILE_SUCCESS:
      return Object.assign({}, state, {
        isUserMaba: action.payload.profile.is_maba,
        profile: {
          ...action.payload.profile,
          photo: action.payload.profile.photo
            ? action.payload.profile.photo
            : "https://github.com/identicons/jonathanfilbert.png",
        },
        group: action.payload.group,
        interests: processInterest(action.payload.profile.interests),
        batch: action.payload.profile.batch,
        about:
          action.payload.profile.about !== ""
            ? action.payload.profile.about
            : action.payload.is_maba
            ? `Kelompok ${action.payload.group.big_group.name} - ${action.payload.small_group.name}`
            : `${action.payload.profile.batch.name} ${action.payload.profile.batch.year}`,
        isLoading: false,
      });
    case FETCH_PROFILE_FAILED:
      return Object.assign({}, state, {
        isLoading: false,
        error: action.payload,
      });
    case UPDATE_PROFILE:
      return Object.assign({}, state, {
        isLoading: true,
      });
    case UPDATE_PROFILE_SUCCESS:
      return Object.assign({}, state, {
        profile: action.payload.profile,
        isLoading: false,
      });
    case UPDATE_PROFILE_FAILED:
      return Object.assign({}, state, {
        isLoading: false,
        error: action.payload,
      });
    default:
      return state;
  }
};

// function profilePageReducer(state = initialState, action) {
//   switch (action.type) {
//     case FETCH_PROFILE:
//       return state.set("isLoading", true);
//     case FETCH_PROFILE_SUCCESS:
//       return state
//         .set("profilId", action.payload.id)
//         .set("profil", fromJS(action.payload.profil))
//         .set(
//           "title",
//           action.payload.title
//             ? action.payload.title
//             : `${getNamaAngkatan(action.payload.profile.batch.name)} ${
//                 action.payload.profile.batch.year
//               }`
//         )
//         .set(
//           "kelompok",
//           action.payload.group
//             ? {
//                 id: action.payload.group.id,
//                 value: `${action.payload.group.big_group.name} -
//               ${action.payload.group.small_group.name}`,
//               }
//             : ""
//         )
//         .set("angkatan", fromJS(action.payload.profile.batch))
//         .set("isLoading", false)
//         .set("isUserMaba", action.payload.profile.is_maba);
//     case FETCH_PROFILE_FAILED:
//       return state.set("error", action.payload).set("isLoading", false);
//     case UPDATE_PROFILE:
//       return state
//         .set("profilId", action.payload.id)
//         .set("profil", action.payload.profil)
//         .set(
//           "title",
//           action.payload.title
//             ? action.payload.title
//             : `Fasilkom ${action.payload.profil.angkatan.tahun}`
//         )
//         .set(
//           "kelompok",
//           action.payload.kelompok
//             ? {
//                 id: action.payload.kelompok.id,
//                 value: `${action.payload.kelompok.kelompok_besar.nama} -
//               ${action.payload.kelompok.kelompok_kecil.nama}`,
//               }
//             : {}
//         )
//         .set("angkatan", fromJS(action.payload.profil.angkatan))
//         .set("isUserMaba", action.payload.profil.angkatan.tahun === "2019");
//     case FETCH_INTEREST:
//       return state.set("isLoading", true);
//     case FETCH_INTEREST_SUCCESS:
//       return state.set("interest", action.payload).set("isLoading", false);
//     case FETCH_INTEREST_FAILED:
//       return state.set("error", action.payload).set("isLoading", false);
//     default:
//       return state;
//   }
// }

export default profilePageReducer;
