import React, { useEffect, useState } from "react";
import styles from "../../../utils/styles";
import Card from "../../../components/Card";
import { Media } from "../../../components/Layout/Media";
import VerifiedBadge from "../components/VerifiedBadge";
import PropTypes from "prop-types";
import Skeleton from "react-loading-skeleton";

const ProfileHeader = ({
  photoUrl,
  fullName,
  isLoading,
  title,
  editable = false,
  onPhotoChange,
}) => {
  const [verifiedStatus, setVerifiedStatus] = useState(false);
  const [profilePicture, setProfilePicture] = useState(null);
  useEffect(() => {
    if (localStorage.getItem("pmb-2020-secret-verified") !== null) {
      setVerifiedStatus(true);
    }
  }, []);

  useEffect(() => {
    setProfilePicture(photoUrl);
  }, [photoUrl]);

  const handleChangePhoto = () => {
    document.getElementById("photo-upload").click();
  };

  const handleUploadPhoto = (event) => {
    const imageMaxSize = 1000000;
    const imageFile = event.target.files[0];
    if (imageFile.size > imageMaxSize) {
      alert(`File size exceeds ${imageMaxSize / 1000000} mb`);
    } else {
      setProfilePicture(URL.createObjectURL(imageFile));
      onPhotoChange(imageFile);
    }
  };

  return (
    <Media>
      {(matches) => (
        <Card
          containerStyle={{
            ...styles.pvl,
            ...styles.phl,
            backgroundColor: editable ? "white" : "#4A69CA",
            ...styles.aic,
            height: "100%",
            display: "flex",
            flexDirection: matches.mobile ? "column" : "row",
            justifyContent: matches.mobile && "center",
          }}
        >
          <div style={{ ...styles.mrl, width: matches.mobile ? "30%" : "15%" }}>
            {isLoading ? (
              <Skeleton circle width={100} height={100} duration={0.5} />
            ) : (
              <>
                <div
                  style={{
                    width: "100%",
                    height: "auto",
                    paddingTop: "100%",
                    borderRadius: "50%",
                    backgroundImage: `url(${profilePicture})`,
                    backgroundRepeat: "no-repeat",
                    backgroundPosition: "center center",
                    backgroundSize: "cover",
                    ...styles.shadow,
                    cursor: editable ? "pointer" : "auto",
                  }}
                  onClick={() => (editable ? handleChangePhoto() : null)}
                />
                <input
                  id="photo-upload"
                  type="file"
                  accept="image/*"
                  style={{ display: "none" }}
                  onChange={(event) => handleUploadPhoto(event)}
                />
              </>
            )}
          </div>
          <div
            style={{
              ...styles.flexDirCol,
              alignItems: matches.mobile && "center",
              marginTop: matches.mobile && 12,
            }}
          >
            <div
              style={{
                ...styles.fcw,
                ...styles.fwb,
                fontSize: matches.mobile ? 18 : 22,
                ...styles.flexDirRow,
                ...styles.aic,
              }}
            >
              <div
                style={{
                  ...styles.mrm,
                  color: editable && "black",
                }}
              >
                {isLoading ? <Skeleton width={120} duration={0.5} /> : fullName}
              </div>
              <div title="Verified">
                {isLoading ? (
                  <Skeleton width={30} duration={0.4} />
                ) : (
                  verifiedStatus && <VerifiedBadge width="30" height="50%" />
                )}
              </div>
            </div>
            <div
              style={{
                fontSize: matches.mobile ? 14 : 18,
                ...styles.fcw,
                ...styles.fwr,
                ...styles.mts,
                color: editable ? "black" : "white",
                textAlign: matches.mobile && "center",
                width: "20em",
                maxWidth: "100%",
              }}
            >
              {isLoading ? (
                <Skeleton width={60} duration={0.3} />
              ) : editable ? (
                "Click on the avatar to upload a custom one from your files."
              ) : (
                <div
                  style={{
                    wordWrap: "break-word",
                  }}
                >
                  {title}
                </div>
              )}
            </div>
          </div>
        </Card>
      )}
    </Media>
  );
};

ProfileHeader.propTypes = {
  fullName: PropTypes.string,
  title: PropTypes.string,
  photoUrl: PropTypes.string,
  isVerified: PropTypes.bool,
  isLoading: PropTypes.bool,
  editable: PropTypes.bool,
};

export default ProfileHeader;
