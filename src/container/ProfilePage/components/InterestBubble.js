import React from "react";
import styles from "../../../utils/styles";
import { Media } from "../../../components/Layout/Media";

const Interest = ({ text, isDeletable, onDelete, moreStyles, className }) => {
  const handleDelete = () => (isDeletable ? onDelete(text) : null);

  return (
    <Media>
      {(matches) => (
        <div
          onClick={() => handleDelete()}
          className={className}
          style={{
            ...styles.pas,
            ...styles.brl,
            ...styles.mrm,
            ...styles.mtm,
            ...styles.flexDirRow,
            ...styles.aic,
            ...moreStyles,
            cursor: isDeletable ? "pointer" : "auto",
            fontSize: matches.mobile ? 12 : 14,
            backgroundColor: "#E7F6FD",
            WebKitTouchCallout: "none",
            WebKitUserSelect: "none",
            kHtmlUserSelect: "none",
            MozUserSelect: "none",
            msUserSelect: "none",
            userSelect: "none",
          }}
        >
          {isDeletable && (
            <div style={{ ...styles.mrm, userSelect: "none" }}>{"X "}</div>
          )}
          <div>{text}</div>
        </div>
      )}
    </Media>
  );
};

export default Interest;
