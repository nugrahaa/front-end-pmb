import React from "react";
import Card from "../../../components/Card";
import styles from "../../../utils/styles";
import Button from "../../../components/Button";
import InterestBubble from "./InterestBubble";

const ProfileInterest = ({ onEdit, interests }) => {
  return (
    <Card containerStyle={{ ...styles.pal }}>
      {/* section starts */}
      {interests.map((interest, index) => (
        <div
          style={{ ...styles.flexDirCol, ...styles.mbl }}
          key={`interest section ${index}`}
        >
          <h3 style={{ margin: 0, ...styles.mbxs }}>{interest.name}</h3>
          <div
            style={{
              ...styles.flexDirRow,
              borderTop: "2px solid black",
              flexWrap: "wrap",
            }}
          >
            {interest.data.length > 0 ? (
              interest.data.map((interestItem) => (
                <InterestBubble text={interestItem} key={interestItem} />
              ))
            ) : (
              <div style={{ ...styles.ptl, width: "100%", ...styles.tac }}>
                Empty
              </div>
            )}
          </div>
        </div>
      ))}
      {/* section ends */}
      <div
        style={{
          width: "100%",
          ...styles.flexDirRow,
          justifyContent: "flex-end",
        }}
      >
        <Button onClick={() => onEdit()}>Edit</Button>
      </div>
    </Card>
  );
};

export default ProfileInterest;
