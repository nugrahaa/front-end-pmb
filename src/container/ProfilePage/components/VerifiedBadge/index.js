import * as React from "react";

const VerifiedBadge = ({ props, width, height }) => {
  return (
    <div className="button-one">
      <svg
        width={width}
        height={height}
        viewBox="0 0 36 37"
        fill="none"
        {...props}
      >
        <path
          d="M13 0l-2 4.5H5v6l-5 3 2.5 5-2.5 4L5 25v6.5h6l3 5 4-4v-29L13 0zM23 0l2 4.5h6v6l5 3-2.5 5 2.5 4-5 2.5v6.5h-6l-3 5-4-4v-29L23 0z"
          fill="#3897F0"
        />
        <path d="M11 17.2l5.04 4.8L25 14" stroke="#fff" strokeWidth={3} />
      </svg>
    </div>
  );
};

export default VerifiedBadge;
