import React from "react";
import styles from "../../../utils/styles";
import Card from "../../../components/Card";
import { getProfileAssetByType } from "../utils/profileAsset";
import Button from "../../../components/Button";
import Skeleton from "react-loading-skeleton";

const ProfileRow = ({ icon, text, isLoading }) => {
  return (
    <div style={{ ...styles.flexDirRow, ...styles.aic, ...styles.mvs }}>
      <div>{getProfileAssetByType(icon)}</div>
      <div style={{ ...styles.mlm }}>
        {isLoading ? <Skeleton width={200} duration={0.4} /> : text}
      </div>
    </div>
  );
};

const ProfileDetail = ({
  prodi,
  highschool,
  birthday,
  email,
  line,
  website,
  isLoading,
  onEdit,
  isPrivate,
}) => {
  return (
    <Card containerStyle={{ ...styles.pal, ...styles.mtl }}>
      <ProfileRow icon="prodi" text={prodi} isLoading={isLoading} />
      <ProfileRow icon="highschool" text={highschool} isLoading={isLoading} />
      <ProfileRow icon="birthday" text={birthday} isLoading={isLoading} />
      <ProfileRow icon="email" text={email} isLoading={isLoading} />
      <ProfileRow icon="line" text={line} isLoading={isLoading} />
      <ProfileRow icon="website" text={website} isLoading={isLoading} />
      <ProfileRow icon="private" text={isPrivate} isLoading={isLoading} />
      <div
        style={{
          width: "100%",
          ...styles.flexDirRow,
          justifyContent: "flex-end",
        }}
      >
        <Button onClick={() => onEdit()}>Edit</Button>
      </div>
    </Card>
  );
};

export default ProfileDetail;
