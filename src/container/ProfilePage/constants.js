export const FETCH_PROFILE = "src/ProfilePage/FETCH_PROFILE";

export const FETCH_PROFILE_SUCCESS = "src/ProfilePage/FETCH_PROFILE_SUCCESS";

export const FETCH_PROFILE_FAILED = "src/ProfilePage/FETCH_PROFILE_FAILED";

export const UPDATE_PROFILE = "src/ProfilePage/UPDATE_PROFILE";

export const UPDATE_PROFILE_SUCCESS = "src/ProfilePage/UPDATE_PROFILE_SUCCESS";

export const UPDATE_PROFILE_FAILED = "src/ProfilePage/UPDATE_PROFILE_FAILED";

export const POST_INTEREST = "src/ProfilePage/POST_INTEREST";

export const POST_INTEREST_SUCCESS = "src/ProfilePage/POST_INTEREST_SUCCESS";

export const POST_INTEREST_FAILED = "src/ProfilePage/POST_INTEREST_FAILED";

export const UPLOAD_PHOTO = "src/ProfilePage/UPLOAD_PHOTO";

export const UPLOAD_PHOTO_SUCCESS = "src/ProfilePage/UPLOAD_PHOTO_SUCCESS";

export const UPLOAD_PHOTO_FAILED = "src/ProfilePage/UPLOAD_PHOTO_FAILED";
