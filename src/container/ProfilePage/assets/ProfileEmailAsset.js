import * as React from "react"

function ProfileEmailAsset(props) {
  return (
    <svg width={18} height={14} viewBox="0 0 18 14" fill="none" {...props}>
      <path
        d="M16.418.151H1.582C.711.151 0 .861 0 1.733V12.28c0 .869.708 1.582 1.582 1.582h14.836c.869 0 1.582-.708 1.582-1.582V1.733c0-.87-.708-1.582-1.582-1.582zM16.2 1.206L9.034 8.372 1.806 1.206h14.393zM1.055 12.062V1.946l5.079 5.036-5.08 5.08zm.745.745l5.083-5.082 1.78 1.765a.527.527 0 00.745-.001l1.737-1.737 5.055 5.055H1.8zm15.145-.745L11.89 7.006l5.055-5.055v10.11z"
        fill="#1B274B"
      />
    </svg>
  )
}

export default ProfileEmailAsset
