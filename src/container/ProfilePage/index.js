/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import ProfileHeader from "./components/ProfileHeader";
import ProfileDetail from "./components/ProfileDetail";
import ProfileInterest from "./components/ProfileInterest";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import { fetchProfile } from "./actions";
import moment from "moment";
import { Helmet } from "react-helmet";
import { isMaba } from "../../selectors/user";

const ProfilePage = ({
  fetchProfile,
  profile,
  isLoading,
  batch,
  push,
  about,
  interests,
  isUserMaba,
}) => {
  useEffect(() => {
    fetchProfile();
  }, []);

  const handleEditProfileDetail = () => {
    push("/profile/edit-profile");
  };

  const handleEditInterest = () => {
    push("/profile/edit-interest");
  };

  return (
    <div>
      <Helmet>
        <title>PMB 2020 - {`${profile.name}`}</title>
      </Helmet>
      <h2>Profile</h2>
      <ProfileHeader
        isLoading={isLoading}
        fullName={profile.name}
        title={about}
        photoUrl={profile.photo}
      />
      <ProfileDetail
        isLoading={isLoading}
        prodi={`FASILKOM ${batch.name}`}
        highschool={profile.high_school === "" ? "-" : profile.high_school}
        birthday={
          profile.birth_date === null
            ? "-"
            : moment(profile.birth_date).format("DD MMMM YYYY")
        }
        email={profile.email}
        line={profile.line_id === "" ? "-" : profile.line_id}
        website={profile.website === "" ? "-" : profile.website}
        isPrivate={
          profile.is_private
            ? "Private mode is active"
            : "Private mode is not active"
        }
        onEdit={() => handleEditProfileDetail()}
      />
      <h2 style={{ marginBottom: 5 }}>Interest</h2>
      <ProfileInterest
        onEdit={() => handleEditInterest()}
        interests={interests}
      />
    </div>
  );
};

const mapStateToProps = (state) => ({
  profile: state.profileReducer.profile,
  isLoading: state.profileReducer.isLoading,
  about: state.profileReducer.about,
  batch: state.profileReducer.batch,
  interests: state.profileReducer.interests,
  isUserMaba: isMaba(state),
});

const mapDispatchToProps = (dispatch) => ({
  push: (url) => dispatch(push(url)),
  fetchProfile: () => dispatch(fetchProfile()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
