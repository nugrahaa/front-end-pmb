import { fromJS } from "immutable";

import { FETCH_EVENT, FETCH_EVENT_SUCCESS, FETCH_EVENT_FAILED } from "./constants";

const initialState = fromJS({
  internalEvents: [],
  externalEvents: [],
  allEvents: [],
  error: null,
  isLoaded: false,
  isLoading: false,
});

const eventPageReducer = (state, action) => {
  if (typeof state === "undefined") {
    return initialState;
  }

  switch (action.type) {
    case FETCH_EVENT:
      return state.set("isLoading", true);
    case FETCH_EVENT_SUCCESS:
      const internalEvents = [];
      const externalEvents = [];

      action.payload.forEach((event) => {
        if (event.organizer) {
          externalEvents.push(event);
        } else {
          internalEvents.push(event);
        }
      });

      return state
        .set("internalEvents", internalEvents)
        .set("externalEvents", externalEvents)
        .set("allEvents", action.payload)
        .set("isLoading", false)
        .set("isLoaded", true);
    case FETCH_EVENT_FAILED:
      return state.set("error", action.payload).set("isLoading", false).set("isLoaded", false);
    default:
      return state;
  }
};

export default eventPageReducer;
