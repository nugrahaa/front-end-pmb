import React from "react";
import PropTypes from "prop-types";

import Button from "../../../components/Button";
import Card from "../../../components/Card";
import iconDate from "../../../assets/icons/calendar.svg";
import iconLocation from "../../../assets/icons/location.svg";
import { StyledEventCard } from "./style";

const EventCard = ({ title, date, location, onClick, image, organizer }) => {
  return (
    <StyledEventCard image={image} organizer={organizer}>
      <Card
        containerStyle={{
          padding: 0,
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
          marginBottom: "1rem",
        }}
      >
        {image && <div className="cover-image" />}
        {organizer && (
          <div className="organizer-image-container">
            <div className="orgnizer-image-position">
              <div className="organizer-image" />
            </div>
          </div>
        )}
        <div className="event-detail-container">
          <div className="event-detail-margin">
            <h3 className="title-container">{title}</h3>
            <div className="detail-container">
              <div className="row margin">
                <img className="date-icon" src={iconDate} alt={"date"} />
                <p>{date}</p>
              </div>
              <div className="row">
                <img
                  className="location-icon"
                  src={iconLocation}
                  alt={"location"}
                />
                <p>{location}</p>
              </div>
            </div>
            <div className="button-container">
              <Button onClick={onClick}>Details</Button>
            </div>
          </div>
        </div>
      </Card>
    </StyledEventCard>
  );
};

EventCard.propTypes = {
  title: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  image: PropTypes.string.isRequired,
  organizer: PropTypes.string.isRequired,
};

export default EventCard;
