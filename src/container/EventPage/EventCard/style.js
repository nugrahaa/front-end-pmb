import styled from "styled-components";

export const StyledEventCard = styled.div`
  .cover-image {
    width: 100%;
    height: 7.5rem;
    border-radius: 10px;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    background-color: gray;
    background-image: url(${(props) => props.image});
    background-repeat: no-repeat;
    background-position: center center;
    background-size: cover;
  }

  .organizer-image {
    height: 4.5rem;
    width: 4.5rem;
    border-radius: 50%;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    background-color: gray;
    background-image: url(${(props) => props.organizer});
    background-repeat: no-repeat;
    background-position: center center;
    background-size: contain;
    margin-right: 1rem;
  }

  .orgnizer-image-position {
    transform: translate(-50%, -220%);
    -ms-transform: translate(-50%, -50%);
  }

  .organizer-image-container {
    width: 100%;
    height: 1rem;
    display: flex;
    justify-content: flex-end;
  }

  .event-detail-container {
    width: 100%;
  }

  .event-detail-margin {
    margin: 20px;
  }

  .title-container {
    font-weight: bold;
    color: #4a69ca;
    font-size: 1.25rem;
    margin-top: 0px;
  }

  .detail-container {
    margin-top: 0.2rem;
    color: black;
  }

  .detail-container .row {
    display: flex;
    flex-direction: row;
    height: 1rem;
    align-items: center;

    .date-icon {
      width: 1rem;
      margin-right: 0.5rem;
    }

    .location-icon {
      width: 0.85rem;
      margin-right: 0.5rem;
    }

    p {
      font-size: 1rem;
    }
  }

  .detail-container .margin {
    margin-bottom: 0.75rem;
  }

  .button-container {
    width: 100%;
    display: flex;
    justify-content: flex-end;
  }

  @media screen and (max-width: 73.75rem) {
    .title-container {
      font-size: 1rem;
    }

    .detail-container .row {
      p {
        font-size: 0.75rem;
      }
    }

    .orgnizer-image-position {
      transform: translate(-10%, -220%);
      -ms-transform: translate(-50%, -50%);
    }
  }
`;
