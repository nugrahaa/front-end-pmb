import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Helmet } from "react-helmet";

import EventSection from "./EventSection";
import { getInternalEvents, getExternalEvents, isEventsLoading } from "../../selectors/events";
import { StyledEventPage } from "./style";

const EventPage = () => {
  const [isInternal, setIsInternal] = useState(true);
  const internalEvents = useSelector((state) => getInternalEvents(state));
  const externalEvents = useSelector((state) => getExternalEvents(state));
  const isLoadingEvents = useSelector((state) => isEventsLoading(state));

  const changeEvent = () => {
    setIsInternal(!isInternal);
  };

  return (
    <StyledEventPage isInternal={isInternal}>
      <Helmet>
        <title>PMB 2020 - Event</title>
      </Helmet>
      <div className="header-button-container">
        <div className="header-button internal-active" onClick={changeEvent}>
          Internal Event
        </div>
        <div className="header-button external-active" onClick={changeEvent}>
          External Event
        </div>
      </div>
      {isInternal ? (
        <EventSection eventData={internalEvents} isLoadingEvents={isLoadingEvents} />
      ) : (
        <EventSection eventData={externalEvents} isLoadingEvents={isLoadingEvents} />
      )}
    </StyledEventPage>
  );
};

export default EventPage;
