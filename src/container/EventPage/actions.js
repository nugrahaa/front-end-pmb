import axios from "axios";

import { FETCH_EVENT, FETCH_EVENT_SUCCESS, FETCH_EVENT_FAILED } from "./constants";
import { fetchEventsApi } from "../../api";

export const fetchEvent = () => {
  return (dispatch) => {
    dispatch({ type: FETCH_EVENT });
    axios
      .get(fetchEventsApi)
      .then((response) => {
        dispatch({
          payload: response.data,
          type: FETCH_EVENT_SUCCESS,
        });
      })
      .catch((error) => {
        dispatch({
          payload: error,
          type: FETCH_EVENT_FAILED,
        });
      });
  };
};
