import styled from "styled-components";

export const StyledEventPagination = styled.div`
  margin-bottom: 2rem;

  .pagination-header {
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
  }

  .title-pagination {
    margin-bottom: 0px;
  }

  .arrow-button {
    background: none !important;
    border: none;
    padding: 0 !important;
    font-size: 1rem;
  }

  .arrow-button:hover {
    cursor: pointer;
  }

  .left {
    margin-right: 0.5rem;
  }

  .right {
    margin-left: 0.5rem;
  }

  .clickable {
    background: none !important;
    border: none;
    padding: 0 !important;
    font-size: 1rem;
    margin: 0 0.75rem;
    font-weight: bold;
    color: #1b274b;
  }

  .page-number-active {
    color: #8ba6b6;
  }

  .page-number-inactive {
    cursor: pointer;
  }
`;
