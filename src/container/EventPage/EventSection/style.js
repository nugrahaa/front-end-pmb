import styled from "styled-components";

export const StyledEventSection = styled.div`
  .event-card-container {
    margin: 1rem 0;
  }
`;
