import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { push } from "connected-react-router";

import { timestampParser } from "../../../utils/timestampParser";
import EventCard from "../EventCard";
import EventPagination from "../EventPagination";
import { StyledEventSection } from "./style";

const EventSection = ({ eventData, isLoadingEvents, push }) => {
  const [upcomingEvents, setUpcomingEvents] = useState([]);
  const [pastEvents, setPastEvents] = useState([]);

  useEffect(() => {
    const now = new Date();
    const newUpcomingEvents = [];
    const newPastEvents = [];

    if (eventData) {
      eventData.forEach((event) => {
        const eventDate = new Date(event.date);
        const eventCard = (
          <div className="event-card-container" key={event.id}>
            <EventCard
              title={event.title}
              date={timestampParser(new Date(event.date))}
              location={event.place}
              onClick={() => push(`/event/${event.id}`)}
              image={event.cover_image_link}
              organizer={event.organizer_image_link}
            />
          </div>
        );

        if (eventDate < now) {
          newPastEvents.push(eventCard);
        } else {
          newUpcomingEvents.push(eventCard);
        }
      });
    }

    setUpcomingEvents(newUpcomingEvents);
    setPastEvents(newPastEvents);
  }, [eventData, push]);

  return (
    <StyledEventSection>
      <EventPagination
        title="Upcoming Event"
        emptyText="There's no upcoming event"
        isLoadingEvents={isLoadingEvents}
      >
        {upcomingEvents}
      </EventPagination>
      <EventPagination
        title="Past Event"
        emptyText="There's no past event"
        isLoadingEvents={isLoadingEvents}
      >
        {pastEvents}
      </EventPagination>
    </StyledEventSection>
  );
};

EventSection.propTypes = {
  eventData: PropTypes.array.isRequired,
  isLoadingEvents: PropTypes.bool,
  push: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  push: (url) => dispatch(push(url)),
});

export default connect(null, mapDispatchToProps)(EventSection);
