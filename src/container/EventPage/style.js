import styled from "styled-components";

export const StyledEventPage = styled.div`
  .header-button-container {
    display: flex;
  }

  .header-button {
    width: 100%;
    height: 2.5rem;
    border-radius: 10px 10px 0px 0px;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
  }

  .internal-active {
    color: ${(props) => (props.isInternal ? `white` : `#4A69CA`)};
    background: ${(props) => (props.isInternal ? `#4A69CA` : `white`)};
    box-shadow: ${(props) => (props.isInternal ? `0px 4px 4px rgba(0, 0, 0, 0.25)` : `none`)};
  }

  .external-active {
    color: ${(props) => (props.isInternal ? `#4A69CA` : `white`)};
    background: ${(props) => (props.isInternal ? `white` : `#4A69CA`)};
    box-shadow: ${(props) => (props.isInternal ? `none` : `0px 4px 4px rgba(0, 0, 0, 0.25)`)};
  }
`;
