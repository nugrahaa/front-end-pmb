import * as React from "react";

function TaskDownloadAsset(props) {
  return (
    <svg width={16} height={16} viewBox="0 0 16 16" {...props}>
      <path
        fillRule="evenodd"
        d="M14 9a1 1 0 011 1v3a2 2 0 01-2 2H3a2 2 0 01-2-2v-3a1 1 0 012 0v3h10v-3a1 1 0 011-1zM8 1a1 1 0 011 1v4.586l1.293-1.293a1 1 0 111.414 1.414L8 10.414 4.293 6.707a1 1 0 011.414-1.414L7 6.586V2a1 1 0 011-1z"
      />
    </svg>
  );
}

export default TaskDownloadAsset;
