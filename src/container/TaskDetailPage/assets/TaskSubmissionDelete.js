import * as React from "react";

function TaskSubmissionDelete(props) {
  return (
    <svg width={16} height={16} viewBox="0 0 16 16" {...props} fill="white">
      <path
        fillRule="evenodd"
        d="M11.293 3.293a1 1 0 111.414 1.414L9.414 8l3.293 3.293a1 1 0 01-1.414 1.414L8 9.414l-3.293 3.293a1 1 0 01-1.414-1.414L6.586 8 3.293 4.707a1 1 0 011.414-1.414L8 6.586l3.293-3.293z"
      />
    </svg>
  );
}

export default TaskSubmissionDelete;
