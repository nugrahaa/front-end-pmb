import { useQuery } from "react-query";
import axios from "axios";
import { taskSubmissionsApi } from "../../../api";

const useQueryTasks = (id) => {
  return useQuery(`getTaskSubmissionList/${id}`, () =>
    axios.get(taskSubmissionsApi(id))
  );
};

export default useQueryTasks;
