import {
  FETCH_TASK_DETAIL,
  FETCH_TASK_DETAIL_SUCCESS,
  FETCH_TASK_DETAIL_FAILED,
  UPLOAD_SUBMISSION_FILE,
  UPLOAD_SUBMISSION_FILE_SUCCESS,
  UPLOAD_SUBMISSION_FILE_FAILED,
  POST_TASK,
  POST_TASK_SUCCESS,
  POST_TASK_FAILED,
} from "./constants";

const initialState = {
  isLoading: false,
  task: {},
  isDeadlineExceeded: false,
};

const taskDetailPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_TASK:
      return {
        ...state,
        isLoading: true,
      };
    case POST_TASK_SUCCESS:
      return {
        ...state,
        isLoading: false,
      };
    case POST_TASK_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case UPLOAD_SUBMISSION_FILE:
      return {
        ...state,
        isLoading: true,
      };
    case UPLOAD_SUBMISSION_FILE_SUCCESS:
      return {
        ...state,
        isLoading: false,
      };
    case UPLOAD_SUBMISSION_FILE_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case FETCH_TASK_DETAIL:
      return {
        ...state,
        isLoading: true,
      };
    case FETCH_TASK_DETAIL_SUCCESS:
      return {
        ...state,
        isLoading: false,
        task: {
          ...action.payload,
        },
        isDeadlineExceeded:
          new Date(action.payload.end_time).getTime() - new Date().getTime() <=
          0,
      };
    case FETCH_TASK_DETAIL_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return { ...state };
  }
};

export default taskDetailPageReducer;
