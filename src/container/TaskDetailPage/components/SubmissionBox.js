import React from "react";
import styles from "../../../utils/styles";

const SubmissionBox = ({ onSubmitFile }) => {
  const handleFileUpload = (event) => {
    onSubmitFile(event.target.files[0]);
  };
  const triggerInput = () => {
    document.getElementById("submission-upload").click();
  };
  return (
    <div>
      <div
        style={{
          width: "100%",
          height: "5em",
          ...styles.brl,
          border: "1px solid black",
          ...styles.flexMiddle,
          cursor: "pointer",
        }}
        onClick={() => triggerInput()}
      >
        <div style={{ ...styles.fwb, color: "#4A69CA" }}>
          Click here to submit task
        </div>
        <input
          id="submission-upload"
          type="file"
          accept="application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf,zip,application/octet-stream,application/zip,application/x-zip,application/x-zip-compressed"
          style={{ display: "none" }}
          onChange={(event) => handleFileUpload(event)}
        />
      </div>
    </div>
  );
};

export default SubmissionBox;
