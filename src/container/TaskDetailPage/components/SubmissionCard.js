import React from "react";
import styles from "../../../utils/styles";
import TaskDownloadAsset from "../assets/TaskDownloadAsset";
import { Media } from "../../../components/Layout/Media";
import moment from "moment";

const SubmissionCard = ({ order, date, url }) => {
  return (
    <Media>
      {(matches) => (
        <div
          style={{
            ...styles.w100,
            ...styles.pal,
            ...styles.flexDirRow,
            ...styles.mtm,
            alignItems: "center",
            justifyContent: "space-between",
            ...styles.fcw,
            ...styles.brm,
            background: "linear-gradient(145deg, #4f70d8, #435fb6)",
            ...styles.shadow,
            wordBreak: "break-all",
          }}
        >
          <div
            style={{
              ...styles.flexDirRow,
              alignItems: "center",
            }}
          >
            <div style={{ ...styles.fwb, fontSize: "2rem" }}>#{order}</div>
            <div
              style={{
                ...styles.flexDirCol,
                ...styles.mlm,
              }}
            >
              <div
                style={{
                  fontSize: matches.mobile ? "0.75em" : "1.25em",
                  ...styles.fwb,
                  ...styles.mbxs,
                }}
              >
                {moment(date).locale("id").format("D MMM")}
              </div>
              <div style={{ fontSize: "0.75em" }}>
                {moment(date).locale("id").format("HH:mm")}
              </div>
            </div>
          </div>
          <a href={url} target="__blank">
            <div
              style={{
                ...styles.pam,
                background: "#d88a4f",
                borderRadius: 200,
                marginRight: "-30px",
                ...styles.shadow,
              }}
            >
              <TaskDownloadAsset fill="white" />
            </div>
          </a>
        </div>
      )}
    </Media>
  );
};

export default SubmissionCard;
