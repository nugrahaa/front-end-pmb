/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable react/jsx-no-duplicate-props */
import React, { useState, useEffect } from "react";
import isEmpty from "lodash/isEmpty";
import { useParams } from "react-router-dom";
import Card from "../../components/Card";
import styles from "../../utils/styles";
import { Media } from "../../components/Layout/Media";
import TaskDetailAttachmentIcon from "./assets/TaskDetailAttachmentAsset";
import SubmissionBox from "./components/SubmissionBox";
import Button from "../../components/Button";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import { isMaba } from "../../selectors/user";
import { fetchTaskDetail, uploadFile, postSubmission } from "./actions";
import LoadingFullScreen from "../../components/LoadingFullscreen";
import moment from "moment";
import "moment/locale/id";
import useQuerySubmissionList from "./hooks/useQuerySubmissionList";
import TaskSubmissionDelete from "./assets/TaskSubmissionDelete";
import SubmissionCard from "./components/SubmissionCard";

const TaskDetailPage = ({
  push,
  fetchTaskDetail,
  task,
  isLoading,
  isUserMaba,
  isDeadlineExceeded,
  uploadFile,
  postSubmission,
}) => {
  const { id } = useParams();
  const [isValid, setIsValid] = useState(false);
  const [file, setFile] = useState(null);
  const {
    data: taskSubmissionData,
    isLoading: isQuerySubmissionLoading,
  } = useQuerySubmissionList(id);

  const handleSubmitFile = (file) => {
    const MAX_FILE_SUBMISSION_SIZE = 3000000;
    if (file.size > MAX_FILE_SUBMISSION_SIZE) {
      alert("File size exceeds 3mb", { type: "error" });
    } else {
      setFile(file);
    }
  };

  useEffect(() => {
    if (file) {
      setIsValid(true);
    }
  }, [file]);

  useEffect(() => {
    fetchTaskDetail(id);
  }, [fetchTaskDetail, id]);

  useEffect(() => {
    if (!isUserMaba) {
      push("/");
    }
  }, [isUserMaba, push]);

  // handle if the requested id does not exist
  useEffect(() => {
    if (!isLoading) {
      if (isEmpty(task)) {
        push("/task");
      }
    }
  }, [isLoading, push, task]);

  const handleCancelSubmission = () => {
    setIsValid(false);
    setFile(null);
  };

  const handleUploadFile = () => {
    uploadFile(file)
      .then((fileUrl) =>
        postSubmission(id, fileUrl)
          .then((success) => {
            alert(success);
            push("/task");
          })
          .catch((err) => {
            alert(err, { type: "error" });
          })
      )
      .catch((err) => alert("Error uploading file.", { type: "error" }));
  };

  if (isLoading || isQuerySubmissionLoading) {
    return <LoadingFullScreen />;
  } else {
    return (
      <Media>
        {(matches) => (
          <div>
            <Helmet>
              <title>PMB 2020 - Task Detail</title>
            </Helmet>
            {matches.mobile ? (
              <h4 style={{ opacity: 0.5, ...styles.flexDirRow }}>
                <span
                  style={{ ...styles.fwb, cursor: "pointer" }}
                  onClick={() => push("/task")}
                >
                  {isDeadlineExceeded ? "Previous Tasks" : "Ongoing Tasks"}
                </span>
                <div style={{ ...styles.mhxs }}>{`>`}</div>
                <span>{task.title}</span>
              </h4>
            ) : (
              <h2 style={{ opacity: 0.5, ...styles.flexDirRow }}>
                <span
                  style={{ ...styles.fwb, cursor: "pointer" }}
                  onClick={() => push("/task")}
                >
                  {isDeadlineExceeded ? "Previous Tasks" : "Ongoing Tasks"}
                </span>
                <div style={{ ...styles.mhs }}>{`>`}</div>
                <span>{task.title}</span>
              </h2>
            )}
            <Card containerStyle={{ ...styles.pal, ...styles.mts }}>
              <div style={{ ...styles.fwb, color: "#4A69CA" }}>
                {task.title}
              </div>
              <div
                style={
                  matches.mobile
                    ? { ...styles.flexDirCol, ...styles.mtl }
                    : { ...styles.flexDirRow, ...styles.mtl }
                }
              >
                <div style={{ ...styles.mrm }}>Due date: </div>
                <div style={{ ...styles.fwb }}>
                  {moment(task.end_time)
                    .locale("id")
                    .format("dddd, D MMMM YYYY HH:mm")}{" "}
                  WIB
                </div>
              </div>
              <div style={{ ...styles.mtl }}>
                {/* Message section */}
                {task.description === "" ? "Semangat yaa!" : task.description}
              </div>
              {/* attachment section */}
              <div
                style={{ ...styles.flexDirRow, ...styles.aic, ...styles.mtl }}
              >
                <TaskDetailAttachmentIcon />
                <a
                  href="https://google.com"
                  target="__blank"
                  style={{
                    color: "black",
                    ...styles.fwb,
                    textDecoration: "none",
                    ...styles.mls,
                  }}
                >
                  Task Attachment
                </a>
              </div>
              {/* Submission Section */}
              {!isDeadlineExceeded && !isValid && task.can_have_submission && (
                <div style={{ ...styles.mtl }}>
                  <SubmissionBox
                    onSubmitFile={(file) => handleSubmitFile(file)}
                  />
                  <div style={{ ...styles.mts, ...styles.flexDirCol }}>
                    You can upload a file up to 3mb.
                    <div>Allowed file types: .pdf, .docx, .doc, .zip</div>
                  </div>
                </div>
              )}
              {/* Submission section ends */}
              {/* File to be uploaded section starts */}
              {isValid && (
                <div
                  style={{
                    ...styles.paxl,
                    ...styles.mtl,
                    ...styles.brl,
                    backgroundColor: "#4A69CA",
                  }}
                >
                  <div
                    style={{
                      ...styles.fcw,
                      ...styles.fwb,
                      ...styles.flexDirRow,
                      ...styles.mbm,
                    }}
                  >
                    {file.name}
                    <div onClick={() => handleCancelSubmission()}>
                      <TaskSubmissionDelete
                        style={{ ...styles.mlm, cursor: "pointer" }}
                      />
                    </div>
                  </div>
                  <div style={{ ...styles.fcw }}>{`File size: ${(
                    file.size / 1000000
                  ).toFixed(2)} MB`}</div>
                </div>
              )}
              {/* File to be uploaded section ends */}
              {/* Previous submisssion section ends */}
              <div
                style={{
                  ...styles.flexDirRow,
                  ...styles.mtl,
                  justifyContent: "flex-end",
                }}
              >
                <div style={{ ...styles.mrm }}>
                  <Button onClick={() => push("/task")}>Back</Button>
                </div>
                {!isDeadlineExceeded && (
                  <Button
                    disabled={!isValid}
                    onClick={() => handleUploadFile()}
                  >
                    Submit
                  </Button>
                )}
              </div>
              {/* Previous Submission section ends */}
              {/* Previous submission section starts */}
              <div
                style={{
                  ...styles.w100,
                  ...styles.flexDirCol,
                  ...styles.mtxxxl,
                }}
              >
                <div style={{ ...styles.fwb, ...styles.tac }}>
                  Previous Submission
                </div>
                {/* Previous submission urls start */}
                <div style={{ ...styles.phxxl }}>
                  {taskSubmissionData.data.length > 0 ? (
                    <div
                      style={{
                        ...styles.flexDirCol,
                        ...styles.w100,
                        ...styles.flexMiddle,
                      }}
                    >
                      {taskSubmissionData.data.map((submission, index) => (
                        <SubmissionCard
                          order={Math.abs(
                            index - taskSubmissionData.data.length
                          )}
                          filename={submission.file_link}
                          date={submission.submit_time}
                          url={submission.file_link}
                        />
                      ))}
                    </div>
                  ) : (
                    <div style={{ ...styles.flexMiddle, ...styles.flexDirCol }}>
                      <div
                        style={{
                          ...styles.fwb,
                          ...styles.mbxxl,
                          ...styles.fcd,
                          ...styles.tac,
                        }}
                      >
                        You haven't submitted anything :(
                      </div>
                      <div style={{ ...styles.tac }}>
                        "A week early is better than a second late."
                      </div>
                      <div>- Every Fasilkom Student</div>
                    </div>
                  )}
                </div>
                {/* Previous submission urls end */}
              </div>
            </Card>
          </div>
        )}
      </Media>
    );
  }
};

const mapDispatchToProps = (dispatch) => ({
  push: (url) => dispatch(push(url)),
  fetchTaskDetail: (id) => dispatch(fetchTaskDetail(id)),
  uploadFile: (file) => dispatch(uploadFile(file)),
  postSubmission: (id, fileUrl) => dispatch(postSubmission(id, fileUrl)),
});

const mapStateToProps = (state) => ({
  isUserMaba: isMaba(state),
  isLoading: state.taskDetailPageReducer.isLoading,
  task: state.taskDetailPageReducer.task,
  isDeadlineExceeded: state.taskDetailPageReducer.isDeadlineExceeded,
});

export default connect(mapStateToProps, mapDispatchToProps)(TaskDetailPage);
