import {
  FETCH_TASK_DETAIL,
  FETCH_TASK_DETAIL_SUCCESS,
  UPLOAD_SUBMISSION_FILE,
  UPLOAD_SUBMISSION_FILE_SUCCESS,
  UPLOAD_SUBMISSION_FILE_FAILED,
  POST_TASK,
  POST_TASK_FAILED,
  POST_TASK_SUCCESS,
  FETCH_TASK_DETAIL_FAILED,
} from "./constants";
import axios from "axios";
import {
  getTaskDetail,
  uploadFileApi,
  taskSubmissionsApi,
  getMediaUrl,
} from "../../api";

export const uploadFile = (file) => {
  const formData = new FormData();
  formData.append("file", file);
  const config = {
    headers: {
      "Content-type": "multipart/form-data",
    },
  };
  return (dispatch) => {
    dispatch({
      type: UPLOAD_SUBMISSION_FILE,
    });
    return new Promise((resolve, reject) => {
      axios
        .post(uploadFileApi, formData, config)
        .then((res) => {
          dispatch({
            type: UPLOAD_SUBMISSION_FILE_SUCCESS,
          });
          resolve(res.data.file);
        })
        .catch((err) => {
          // console.log("ini error", err);
          dispatch({
            type: UPLOAD_SUBMISSION_FILE_FAILED,
          });
          reject(err.message);
        });
    });
  };
};

export const postSubmission = (taskId, fileUrl) => {
  return (dispatch) => {
    dispatch({
      type: POST_TASK,
    });
    return new Promise((resolve, reject) => {
      axios({
        method: "post",
        url: taskSubmissionsApi(taskId),
        data: {
          file_link: getMediaUrl(fileUrl),
          submit_time: new Date(),
        },
      })
        .then((res) => {
          dispatch({
            type: POST_TASK_SUCCESS,
          });
          resolve("Submitted!");
        })
        .catch((err) => {
          dispatch({
            type: POST_TASK_FAILED,
          });
          reject("An error occured.");
        });
    });
  };
};

export const fetchTaskDetail = (id) => {
  return (dispatch) => {
    dispatch({
      type: FETCH_TASK_DETAIL,
    });
    axios({
      url: getTaskDetail(id),
      method: "get",
    })
      .then((response) => {
        dispatch({
          type: FETCH_TASK_DETAIL_SUCCESS,
          payload: response.data,
        });
      })
      .catch((err) =>
        dispatch({
          type: FETCH_TASK_DETAIL_FAILED,
        })
      );
  };
};
