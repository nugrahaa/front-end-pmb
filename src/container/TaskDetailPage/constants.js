export const FETCH_TASK_DETAIL = "src/TaskDetailPage/FETCH_TASK_DETAIL";

export const FETCH_TASK_DETAIL_SUCCESS =
  "src/TaskDetailPage/FETCH_TASK_DETAIL_SUCCESS";

export const FETCH_TASK_DETAIL_FAILED =
  "src/TaskDetailPage/FETCH_TASK_DETAIL_FAILED";

export const UPLOAD_SUBMISSION_FILE =
  "src/TaskDetailPage/UPLOAD_SUBMISSION_FILE";

export const UPLOAD_SUBMISSION_FILE_SUCCESS =
  "src/TaskDetailPage/UPLOAD_SUBMISSION_FILE_SUCCESS";

export const UPLOAD_SUBMISSION_FILE_FAILED =
  "src/TaskDetailPage/UPLOAD_SUBMISSION_FILE_FAILED";

export const POST_TASK = "src/TaskDetailPage/POST_TASK";

export const POST_TASK_SUCCESS = "src/TaskDetailPage/POST_TASK_SUCCESS";

export const POST_TASK_FAILED = "src/TaskDetailPage/POST_TASK_FAILED";
