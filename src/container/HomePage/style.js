import styled from "styled-components";
import desktop from "../../assets/img/Desktop-Background.png";
import mobile from "../../assets/img/Mobile-Background.png";

export const HomePageStyle = styled.div`
    .HomePage_background {
        /* background: url(${desktop}); */
        -webkit-background-size:cover;
        -moz-background-size:cover;
        -o-background-size:cover;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        padding-bottom: 200px;

        .HomePage {
          width: 100%;
          display: flex;
          flex-direction: column;
          justify-content: flex-start;
        }
    }

    @media (min-width: 481px) and (max-width: 767px) {

        .HomePage_background {
            /* background: url(${mobile}); */
        }

      }

      /*
        ##Device = Most of the Smartphones Mobiles (Portrait)
        ##Screen = B/w 320px to 479px
      */

      @media (min-width: 321px) and (max-width: 480px) {
        .HomePage_background {
            /* background: url(${mobile}); */
        }
      }

      @media (max-width: 320px) {
        .HomePage_background {
            /* background: url(${mobile}); */
        }
      }
`;