import React from "react";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import { setUser } from "../../globalActions";
import PropTypes from "prop-types";

import { getAllEvents, isEventsLoading } from "../../selectors/events";
import { getTasks, isTasksLoading } from "../../selectors/tasks";
import { isMaba, getUser } from "../../selectors/user";

import Loading from "../../components/Loading";
import EmptyCard from "../../components/EmptyCard";
import TaskListCard from "../../components/TaskListCard";
import EventListCard from "../../components/EventListCard";
// import StatisticListCard from "../../components/StatisticListCard";
import { Helmet } from "react-helmet";

import { HomePageStyle } from "./style";

function HomePage(props) {
  const getNearestUpcomingItems = (
    arrayOfItems,
    funcToGetDatetimeString,
    numberOfItemsToGet
  ) => {
    const mockServerTime = new Date();
    const serverTimeInMilli = new Date(mockServerTime).getTime();

    return JSON.parse(JSON.stringify(arrayOfItems))
      .filter(
        (elem) =>
          new Date(funcToGetDatetimeString(elem)).getTime() -
            serverTimeInMilli >= //harusnya >= tapi sementara biar keliatan eventnya, kalau >= task muncul, event tidak
          0
      )
      .sort(
        (elem1, elem2) =>
          new Date(funcToGetDatetimeString(elem1)).getTime() -
          new Date(funcToGetDatetimeString(elem2)).getTime()
      )
      .slice(0, numberOfItemsToGet);
  };

  const getEvent = () =>
    getNearestUpcomingItems(props.events, (elem) => elem.date, 1);

  const getTask = () =>
    getNearestUpcomingItems(props.tasks, (elem) => elem.end_time, 2);

  return (
    <HomePageStyle>
      <Helmet>
        <title>PMB 2020 - Dashboard</title>
      </Helmet>
      <div
        className="HomePage_background"
        style={{ display: "flex", justifyContent: "flex-start" }}
      >
        <div className="HomePage">
          {props.isMaba && (
            <div className="TaskList">
              <h1 className="TaskList_title">Ongoing Task</h1>
              {props.isLoadingTasks ? (
                <Loading />
              ) : getTask().length !== 0 ? (
                getTask().map((task) => (
                  <TaskListCard
                    key={task.id}
                    title={task.title}
                    date={task.end_time}
                    submissionStatus={task.is_submitted}
                    onOpen={() => props.push(`/task/${task.id}`)}
                  />
                ))
              ) : (
                <EmptyCard text={"There isn't any Ongoing Task"} />
              )}
            </div>
          )}
          <div className="EventList">
            <h1>Upcoming Event</h1>
            {props.isLoadingEvents ? (
              <Loading />
            ) : getEvent().length !== 0 ? (
              getEvent().map((event) => (
                <EventListCard
                  key={event.id}
                  title={event.title}
                  date={event.date}
                  location={event.place}
                  onClick={() => props.push(`/event/${event.id}`)}
                />
              ))
            ) : (
              <EmptyCard text={"There isn't any Upcoming Event"} />
            )}
          </div>
          <div className="StatisticList">
            <h1>Statistics</h1>
            <EmptyCard text={"Coming Soon"} />
          </div>
        </div>
      </div>
    </HomePageStyle>
  );
}

HomePage.propTypes = {
  push: PropTypes.func.isRequired,
  isLoadingEvents: PropTypes.bool.isRequired,
  isLoadingTasks: PropTypes.bool.isRequired,
  events: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  tasks: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

function mapStateToProps(state) {
  return {
    events: getAllEvents(state),
    tasks: getTasks(state),
    isLoadingEvents: isEventsLoading(state),
    isLoadingTasks: isTasksLoading(state),
    isMaba: isMaba(state),
    getUser: getUser(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    push: (url, data) => dispatch(push(url, { ...data })),
    setUser: (url) => dispatch(setUser()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
