import styled from "styled-components";

export const GalleryPageWrapper = styled.div`
  width:
    ${props => props.empty
      ? 'calc(100%)'
      : 'calc(100% - 10vw)'};
  padding:
    ${props => props.empty
        ? '0'
        : '0px 5vw 1vh'};

  @media (max-width: 769px) {
    width:
      ${props => props.empty
        ? 'calc(100%)'
        : 'calc(100% - 1vw)'};
    padding:
      ${props => props.empty
        ? '0'
        : '0px 0.5vw 0.5vh'};
  }

  h1 {
    @media (max-width: 429px) {
      font-size: 28px;
    }
  }
`