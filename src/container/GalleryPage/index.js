import React from "react";
import { Helmet } from "react-helmet";

import Albums from "./dummy";
import GallerySection from "./components/GallerySection";
import EmptyCard from "../../components/EmptyCard";
import {
  GalleryPageWrapper,
} from "./style"

/*
   Using PMB 2019 gallery carousel as reference
*/

const GalleryPage = (props) => {
  return (
    <GalleryPageWrapper
      empty={Albums.length === 0}
    >
      <Helmet>
        <title>PMB 2020 - Gallery</title>
      </Helmet>

      <h1>
        Gallery
      </h1>

      {Albums.length !== 0
        ?
          Albums.map((el,i) => (
            <GallerySection
              key={i}
              collections={Albums}
              album={el}
              title={el.title}
              photos={el.photos}/>
          ))
        :
          <EmptyCard text={"There aren't any photos to be displayed yet"} />
      }
    </GalleryPageWrapper>
  );
};

export default GalleryPage;