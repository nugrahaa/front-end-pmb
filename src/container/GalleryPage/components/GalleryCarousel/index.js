import React, { useState, useEffect } from 'react';

import SlideLeft from './assets/slide-left.svg';
import SlideRight from './assets/slide-right.svg';
import CloseButton from './assets/gallery-close-button.svg';
import {
  GalleryCarouselWrapper,
  GalleryCarouselImage
} from './style';

/*
   Using PMB 2019 gallery carousel as reference
*/

const GalleryCarousel = ({
  open,
  handleClose,
  album,
  photos,
  photoIndex,
}) => {
  const [carousel, setCarousel] = useState({
    currentIndex: photoIndex,
  });

  useEffect(() => {
    const resizeListener = () => {
      // set carousel photo index
      if (open)
      setCarousel({...carousel});
    }
    // set resize listener
    window.addEventListener('resize', resizeListener);

    // clean up function
    return () => {
      // remove resize listener
      window.removeEventListener('resize', resizeListener);
    }
  }, [carousel, open])

  // store index to state everytime photoIndex from props changes
  useEffect(() => {
    setCarousel({currentIndex: photoIndex});
  }, [photoIndex])

  // handle close carousel
  const handleCarouselClose = () => {
    handleClose();
  };

  // handle close carousel using escape key
  const handleCarouselKeypress = (event) => {
    if (event.keyCode === 27) {
      handleCarouselClose();
    } else if (event.keyCode === 37) {
      handleSlideLeft();
    } else if (event.keyCode === 39) {
      handleSlideRight();
    };
  };

  // add event listener for esc keypress on closing modal
  useEffect(() => {
    if (open) {
      document.addEventListener(
        "keydown", handleCarouselKeypress, false
        );
    };
    return () => {
      document.removeEventListener(
        "keydown", handleCarouselKeypress, false
        );
    };
  });

  // handle carousel slide to the left
  const handleSlideLeft = () => {
    getSlideWidth()
    setCarousel({
      ...carousel,
      currentIndex: (
        carousel.currentIndex > 0
          ? carousel.currentIndex - 1
          : photos.length - 1
        )
    });
  };

  // handle carousel slide to the right
  const handleSlideRight = () => {
    setCarousel({
      ...carousel,
      currentIndex: (
        carousel.currentIndex < photos.length - 1
          ? carousel.currentIndex + 1
          : 0
        )
    });
  };

  // handle clicking photo to carousel
  const handleCarouselMenuClick = (event) => {
    // photoindex is acquired as string
    let index = event.target.dataset.photoIndex;
    // index needs to be casted to int/num
    // so the addition slide l/r won't be treated as concatenation
    setCarousel({
      ...carousel,
      currentIndex: Number(index)
    });
  };

  // get value of translateX css property for the main slider
  const getTranslateValue = () => {
    return -1 * carousel.currentIndex * getSlideWidth();
  }

  // get value of translateX css property for bottom menu slider
  const getMenuTranslateValue = () => {
    return -1 * carousel.currentIndex * getMenuSlideWidth();
  };

  // get distance of one slide for main slider
  const getSlideWidth = () => {
    const elements = document.getElementsByClassName(
      "carousel-image-img"
    );

    const elements2 = document.getElementsByClassName(
      "carousel-image-inner-wrapper"
    );

    return elements.length !== 0
      // ? elements[carousel.currentIndex]?.getBoundingClientRect().width
      ? elements2[album.id].getBoundingClientRect().width + 10
      : 0;
  };

  // get distance of one slide for bottom menu slider
  const getMenuSlideWidth = () => {
    const elements = document.getElementsByClassName(
      "carousel-menu-image-img"
    );

    if (elements.length !== 0) {
      const element = elements[album.id];
      const style = element.currentStyle || window.getComputedStyle(element);

      // console.log("elmenu",element.clientWidth)
      // console.log("elmenu",element.getBoundingClientRect().width)

      return element.offsetWidth
             + stringPixelToInteger(style.marginRight)
             + stringPixelToInteger(style.marginLeft);
    }

    return 0;
  };

  // converts string "10px" to number 10
  const stringPixelToInteger = (string) => {
    return parseInt(string.slice(0, -2), 10);
  };

  const GalleryCarouselImageComponent = ({ className, src, alt }) => {
    return (
      <GalleryCarouselImage
        className="carousel-image-img"
        id={"carousel-image-img " + String(album.id) + String(alt)}
        image={src}
        alt={alt}
      />
    )
  };

  return (
    <GalleryCarouselWrapper
      carouselOpen={open}
      translateValue={open ? getTranslateValue() : 0}
      menuTranslateValue={open ? getMenuTranslateValue() : 0}
    >
      <div
        className={
          "gallery-carousel-wrapper" +
            (!open
             ? " carousel-hide"
             : " ")
        }
      >
        <div
          className="carousel-content"
          onKeyDown={open ? handleCarouselKeypress : () => {}}
        >
          <div className="carousel-slide-wrapper left">
            <img
              className="carousel-slide"
              onClick={handleSlideLeft}
              src={SlideLeft}
              alt="slide-left-button"
            />
          </div>
          <div className="carousel-image-content">
            <div className="carousel-image">
              <div className="carousel-image-wrapper">
                <div className="carousel-image-inner-wrapper">
                  { open
                    ? photos.map((el, i) => (
                        <GalleryCarouselImageComponent
                          key={i}
                          src={el}
                          alt={i}
                        />))
                    : ""
                  }
                </div>
              </div>
            </div>
            <div className="carousel-menu-image">
              <div className="carousel-menu-image-inner-wrapper">
                {photos.map((el, i) =>
                  <img
                    key={i}
                    className="carousel-menu-image-img"
                    src={el}
                    alt={i}
                    data-photo-index={i}
                    onClick={handleCarouselMenuClick}
                  />
                )}
              </div>
            </div>
          </div>
          <div className="carousel-slide-wrapper right">
            <img
              className="carousel-close-button"
              onClick={handleCarouselClose}
              src={CloseButton}
              alt="carousel-close-button"
            />
            <img
              className="carousel-slide"
              onClick={handleSlideRight}
              src={SlideRight}
              alt="slide-right-button"
            />
          </div>
        </div>
      </div>
    </GalleryCarouselWrapper>
  )
};

export default GalleryCarousel;