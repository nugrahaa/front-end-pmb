import styled from "styled-components";

export const GalleryCarouselWrapper = styled.div`
  .gallery-carousel-wrapper {
    width: 100%;
    height: 100%;
    display: block;
    position: fixed;
    background: rgba(0, 0, 0, 0.7);
    top: 0;
    left: 0;
    z-index: 20;

    margin-left: auto;
    margin-right: auto;
  }

  .carousel-hide {
    display: none;
  }

  // ============ Content ============ //
  .carousel-content {
    z-index: 10;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }

  .carousel-content {
    width: 100%;
    height: 100%;
  }

  // ============ Slider ============ //
  .carousel-slide-wrapper {
    width: fit-content;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .carousel-close-button {
    position: absolute;
    width: 40px;
    cursor: pointer;
    top: 5vh;

    &:hover {
      filter: opacity(0.85);
    }

    &:active {
      transform: scale(0.95);
    }

    @media (max-width: 769px) {
      width: 35px;
    }

    @media (max-width: 525px) {
      width: 25px;
    }
  }

  .carousel-slide {
    width: 70px;
    cursor: pointer;

    &:hover {
      filter: opacity(0.85);
    }

    &:active {
      transform: scale(0.95);
    }

    @media (max-width: 769px) {
      width: 50px;
    }

    @media (max-width: 525px) {
      width: 40px;
    }
  }

  .carousel-slide-wrapper.left {
    margin-left: 3.5%;

    @media (max-width: 769px) {
      margin-left: 2.5%;
    }
  }

  .carousel-slide-wrapper.right {
    margin-right: 3.5%;

    @media (max-width: 769px) {
      margin-right: 2.5%;
    }
  }

  // ============ Image & Menu Wrapper ============ //
  .carousel-image-content {
    width: 60%;
    display: flex;
    flex-direction: column;
    overflow: hidden;

    @media (max-width: 769px) {
      width: 65%;
    }
  }

  // ============ Image ============ //
  .carousel-image {
    width: 100%;
    height: calc(80% - 60px);
    margin: 40px 0px 20px;

    @media (max-width: 1025px) {
      margin: 0px 0px;
      height: calc(100% - 0px);
    }
  }

  .carousel-image-wrapper {
    position: relative;
    width: 100%;
    height: 100%;
    white-space: nowrap;
  }

  .carousel-image-inner-wrapper {
    width: 100%;
    // max-width: 100%;
    height: 100%;
    position: relative;

    // display: flex;
    // align-items: center;
    // justify-content: center;

    transform: translateX(${props => props.translateValue}px);
    ${props => true && "transition: transform ease-out 0.6s;"}
  }


  // ============ Menu Image ============ //
  .carousel-menu-image {
    width: 80%;
    height: 20%;
    overflow: hidden;
    position: relative;

    display: block;
    margin-left: auto;
    margin-right: auto;

    @media (max-width: 1025px) {
      display: none;
      height: 0%;
    }
  }

  .carousel-menu-image-inner-wrapper {
    display: flex;
    align-items: center;
    height: 100%;
    transform: translateX(${props => props.menuTranslateValue}px);
    ${props => true && "transition: transform ease-out 0.6s;"}

    &:first-child {
      margin-left: calc(50% - 55px + 5px);
    }
  }

  img.carousel-menu-image-img {
    width: 90px;
    height: 90px;
    object-fit: cover;
    margin: 0px 10px;
    cursor: pointer;
    display: block;
  }

  img.carousel-menu-image-img:hover {
    transform: scale(1.05);
  }
`;

export const GalleryCarouselImage = styled.div`
&.carousel-image-img {
    height: 100%;
    width: 100%;
    // min-width: 100%;

    margin: 0 5px;

    background-size: contain;
    display: inline-block;

    background-image: url(${props => props.image});
    background-position: center center;
    background-repeat: no-repeat;
    background-size: contain;
  }
`;