import React, { useState, useRef } from 'react';

import Loading from '../../../../components/Loading';
import GalleryCarousel from '../GalleryCarousel';
import {
  GallerySectionWrapper
} from './style';

const GallerySection = ({
  collections,
  album,
  title,
  photos,
  ...props
}) => {
  const [carousel, setCarousel] = useState({
    open: false,
    albumIndex: 0,
    photoIndex: 0,
  });
  const [loading, setLoading] = useState(true);

  const counter = useRef(0);
  const loadImage = () => {
    counter.current += 1;
    if (counter.current >= photos.length) {
      setLoading(false);
    };
  };

  const handleOpen = (event) => {
    // get indexes of clicked image from event.target
    let albumIndex = parseInt(event.target.dataset.albumIndex, 10)
    let photoIndex = parseInt(event.target.alt, 10);
    // update each attributes of state
    let newCarousel = {...carousel};
    newCarousel.open = true;
    newCarousel.albumIndex = albumIndex;
    newCarousel.photoIndex = photoIndex;
    // set carousel to open
    setCarousel(newCarousel);
  };

  const handleClose = () => {
    setCarousel({
      ...carousel,
      open: false,
      albumIndex: 0,
      photoIndex: 0,
    });
  };

  return (
    <>
      <GallerySectionWrapper
        loading={loading}
        numOfPhotos={photos.length}>
          <h2 className="gallery-section-title">
            {title}
          </h2>
          {/* <hr/> */}
          <div style={{display: loading ? "block" : "none"}}>
            <Loading />
          </div>
          <div
            className="gallery-section-img-wrapper"
            style={{display: loading ? "none" : "block"}}
          >
            {photos.map((el, i) => (
                <img
                  key={i}
                  data-album-index={album.id}
                  className="gallery-section-img"
                  src={el}
                  alt={i}
                  onClick={handleOpen}
                  onLoad={loadImage}
                />
              ))}
          </div>
      </GallerySectionWrapper>
      <GalleryCarousel
        open={carousel.open}
        handleClose={() => handleClose()}
        album={collections[carousel.albumIndex]}
        photos={collections[carousel.albumIndex].photos}
        photoIndex={carousel.photoIndex}>
      </GalleryCarousel>
    </>
  );
};

export default GallerySection;