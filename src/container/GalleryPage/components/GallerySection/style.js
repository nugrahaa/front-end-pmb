import styled from "styled-components";

export const GallerySectionWrapper = styled.div`
  width: calc(100% - 30px);
  min-height: ${props => props.loading ? '280px' : 'fit-content'};
  height: fit-content;
  margin-bottom: 50px;
  background: #FFFFFF;
  mix-blend-mode: normal;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: 20px;
  padding: 20px 15px 5px;

  @media (max-width: 769px) {
    margin-bottom: 30px;
  }

  @media (max-width: 429px) {
    margin-bottom: 20px;
    width: calc(100% - 20px);
    padding: 15px 10px 0px;
  }

  @media (max-width: 385px) {
    width: calc(100% - 10px);
    border-radius: 15px;
    padding: 10px 5px 0px;
  }

  h2.gallery-section-title {
    font-weight: bold;
    padding-left: 5px;
    margin: 0;
    margin-bottom: 10px;

    @media (max-width: 429px) {
      font-size: 20px;
      margin-bottom: 5px;
    }

    @media (max-width: 385px) {
      font-size: 18px;
    }
  }

  // hr {
  //   width: calc(100% - 8px);
  //   margin: 0px 5px 15px 3px;
  //   height: 1px;
  //   background-color: #1B274B;
  // }

  div.gallery-section-img-wrapper {
    width: 100%;
    ${props => props.numOfPhotos > 2
      ? "column-count: 2; column-gap: 5px;"
      : ""}

      @media (max-width: 768px) {
        column-gap: 2.5px;
      }
    }

  img.gallery-section-img {
    width: calc(100% - 10px);
    height: auto;
    max-height: 500px;
    object-fit: cover;

    margin: 0px 0px 5px;
    padding: 3px;
    border: 2px solid rgba(0, 0, 0, 0);
    border-radius: 15px;

    transition: border-radius 1s;
    transition: max-height 0.6s ease-in;
    cursor: pointer;

    ${props => props.numOfPhotos > 2
                ? "display: inline-block; max-height: 300px;"
                : ""}

    @media (max-width: 385px) {
      margin: 0px;
      border-radius: 15px;
    }

    &:hover {
      border: 2px solid #1B274B;
      border-radius: 18px;
      // max-height: 1200px;
      object-fit: fit;
    }
  }
`