import styled from "styled-components";
import desktop from "./assets/landingPageDesktop.png";
import footerDesktop from "./assets/landingPageFooter.png";
import mobile from "./assets/mobileLandingPage.png";

export const LandingPageElement = styled.div`
  width: 100%;
  height: 100%;
  font-family: "Metropolis";

  /* media for Iphone 5  or smaller phones*/
  .container {
    width: 100%;
    height: 100%;
    min-height: 100%;
    position: fixed;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    align-content: center;
    background-image: url(${desktop});
  }

  .logoContainer {
    display: flex;
    justify-content: center;
    width: 100%;
    top: 0.5rem;
    left: 0;
    position: absolute;

    img {
      max-height: 35px;
      padding-right: 1rem;
    }
  }

  .menu {
    height: 100%;
    width: 100vw;
    top: 30vh;
    display: flex;
    justify-content: left;
    flex-direction: column;
    position: relative;

    h1 {
      margin: 0;
      font-style: normal;
      font-weight: 500;
      font-size: 3rem;
      text-align: left;
    }

    .ssoButton {
      border: 2px solid lightblue;
      border-radius: 10px;
      width: 9rem;
      padding: 0.25rem 0.1rem;
      position: relative;
      display: block;
      background: none;
      overflow: hidden;
      font-size: 14px;
      font-weight: 500;
      cursor: pointer;
      transition: all 0.3s;
      display: flex;
      justify-content: left;
      flex-direction: column;

      &:hover {
        color: white;
      }

      &::before,
      &::after {
        content: "";
        position: absolute;
        background: lightblue;
        z-index: -1;
      }

      &::after {
        width: 0;
        height: 100%;
        left: -35%;
        top: 0;
        transform: skew(60deg);
        transition-duration: 0.5s;
        transform-origin: top left;
        transition: all 0.25s;
      }

      &:hover:after {
        width: 150%;
        height: 100%;
      }
    }
  }

  .ssoButton {
    margin: 1.5rem 0 0 0;
  }

  .homeImage {
    width: 100%;
    height: 100%;
    display: block;
    background-image: url(${mobile});
    background-repeat: no-repeat;
    background-position: 100% 100%;
    background-size: 100%;
    z-index: -9999;
  }

  /* Media for average smartphones*/
  @media (min-height: 600px) {
    .logoContainer {
      margin-right: 10px;
    }
    .menu {
      margin-left: 2rem;
      h1 {
        font-size: 2rem;
      }

      .buttonContainer {
        margin-top: 1rem;
        .ssoButton {
          width: 11rem;
          padding: 0.5rem 0.3rem;
          font-size: 16px;
        }
      }
    }
  }

  /* Media for average smartphones*/
  @media (max-width: 400px) {
    .menu {
      margin-left: 2rem;
      h1 {
        font-size: 2rem;
      }

      .buttonContainer {
        margin-top: 1rem;
        .ssoButton {
          width: 11rem;
          padding: 0.5rem 0.3rem;
          font-size: 16px;
        }
      }
    }
  }

  /* Media for smaller screen laptops (touchscreen etc) */
  @media (min-width: 64em) {
    .container {
      flex-direction: row;
      justify-content: flex-start;
      overflow: hidden;
    }

    .logoContainer {
      display: flex;
      justify-content: center;
      img {
        max-height: 50px;
      }
    }

    .buttonContainer {
      flex-direction: column;
      margin-top: 1rem;
    }

    .menu {
      height: 100%;
      margin: 0;
      top: 18vh;
      position: relative;
      margin-left: 10rem;
      display: flex;
      justify-content: left;
      flex-direction: column;

      h1 {
        font-size: 4rem;
        text-align: left;
        font-weight: 500;
      }

      .ssoButton {
        padding: 1rem 0.5rem;
        font-style: normal;
        font-weight: 500;
        font-size: 18px;
        border-radius: 0.8rem;
        width: 15rem;
      }

      .ssoButton {
        margin-top: 2rem;
      }
    }

    .homeImage {
      background-image: url(${footerDesktop});
      z-index: -9999;
      position: absolute;
    }
  }

  /* Media for above 1600 in width */
  @media (min-width: 1601px) and (min-height: 700px) {
    .container {
      flex-direction: row;
      justify-content: flex-start;
      overflow: hidden;
    }

    .logoContainer {
      img {
        max-height: 72px;
      }
    }

    .menu {
      height: 100%;
      top: 5vw;

      h1 {
        font-size: 6rem;
        font-weight: 500;
      }

      .ssoButton {
        width: 15rem;
        padding: 1rem 0.5rem;
        margin-left: 5.75rem;
        font-weight: 500;
        font-size: 18px;
      }

      .ssoButton {
        margin-top: 3.3rem;
      }
    }

    .homeImage {
      background-image: url(${desktop});
    }
  }
`;
