import React, { useEffect } from "react";
import RistekLogo from "./assets/ristekLogo.png";
import PmbLogo from "./assets/pmbLogo.png";
import { LandingPageElement } from "./style";
import Button from "../../components/Button";
import PropTypes from "prop-types";

import { connect } from "react-redux";
import { push } from "connected-react-router";

import { login } from "../../globalActions";

import { loggingInApi } from "../../api";
import { isLoggedIn } from "../../selectors/user";
import { Helmet } from "react-helmet";

function LandingPage(props) {
  const handleLogin = () => {
    const loginWindow = window.open(
      loggingInApi,
      "_blank",
      "width=800,height=800",
      "toolbar=0,location=0,menubar=0"
    );

    const getUserDataInterval = setInterval(() => {
      if (loginWindow.closed) {
        clearInterval(getUserDataInterval);
      }
      loginWindow.postMessage("MadeByWebdevRistek2020", loggingInApi);
    }, 1000);
  };

  const receiveLoginData = (event) => {
    const origin = event.origin || event.originalEvent.origin;
    const user = event.data;

    if (loggingInApi.startsWith(origin)) {
      props.login(user);
    }
  };

  useEffect(() => {
    window.addEventListener("message", receiveLoginData, false);
    if (props.loggedIn) {
      props.push("/");
    }
  });

  return (
    <LandingPageElement>
      <Helmet>
        <title>PMB 2020 - Login</title>
      </Helmet>
      <div className="container">
        <div className="logoContainer">
          <img src={RistekLogo} alt="ristek" />
          <img src={PmbLogo} alt="pmb" />
        </div>
        <div className="menu">
          <h1>Kaitkan Angan,</h1>
          <br />
          <h1>Layangkan Mimpi</h1>
          <div className="buttonContainer">
            <Button onClick={() => handleLogin()}>SSO Login</Button>
          </div>
        </div>
        <div className="homeImage"></div>
      </div>
    </LandingPageElement>
  );
}

LandingPage.propTypes = {
  push: PropTypes.func.isRequired,
  login: PropTypes.func.isRequired,
  loggedIn: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    loggedIn: isLoggedIn(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    push: (url) => dispatch(push(url)),
    login: (user) => dispatch(login(user)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LandingPage);
