import React from "react";
import styles from "../../utils/styles";
import { Helmet } from "react-helmet";

const Dummy = () => {
  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Helmet>
        <title>Layout Component Visualization</title>
      </Helmet>
      <div
        style={{
          ...styles.flexMiddle,
          backgroundColor: "green",
          height: "80vh",
        }}
      >
        Content goes here
      </div>
    </div>
  );
};

export default Dummy;
