import React,{useEffect, useState} from 'react';

import { connect } from "react-redux";

import { WrapperSearch } from "./style";

import { fetchMahasiswa } from "./actions";
import SearchCard from "./components/SearchCard";
import ViewCard from "./components/ViewCard";
import qs from 'query-string'
import {Helmet} from 'react-helmet';

function SearchMahasiswaPage({data,isLoaded,isLoading,fetchMahasiswa,...props}){
    let query = props.location.search;
    const parsedQuery = qs.parse(query);
    const [ dataView, setDataView] = useState({
        data:{},
        view:false
    })

  
    useEffect(()=>{  
        setDataView({
            data:{},
        })
        
       fetchMahasiswa(parsedQuery.q)      
    },[parsedQuery.q, fetchMahasiswa])

    return(
        <WrapperSearch>
        <Helmet>
          <title>PMB 2020 - Search</title>
        </Helmet>
            {
                dataView.view ? 
                <>
               
                    <ViewCard data={dataView.data} setDataView={setDataView}/>
               </>  
               :
               <>
               <h3>Search result for “{parsedQuery.q}”</h3>
         
               {
                   data.map((el,idx)=>{
                       return(
                        <SearchCard data={el} key={idx} setDataView={setDataView} />
                       )
                   })
               }
           
              </>

            }
           
        </WrapperSearch>
    )
}
const mapStateToProps = (state) => ({
    data: state.searchMahasiswaReducer.data,
    isLoaded: state.searchMahasiswaReducer.isLoaded,
    isLoading: state.searchMahasiswaReducer.isLoading,
  });
  
const mapDispatchToProps = (dispatch) => ({
    fetchMahasiswa: (query) => dispatch(fetchMahasiswa(query)),   
});
  
export default connect(mapStateToProps, mapDispatchToProps)(SearchMahasiswaPage);
  