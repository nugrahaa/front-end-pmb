import styled from 'styled-components';

export const WrapperSearch = styled.div `
    display:flex;
    flex-direction:column;
    padding-top:1rem;
`