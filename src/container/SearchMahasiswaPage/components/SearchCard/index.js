import React from 'react';
import {Profile,WrapperAction,  WrapperCard, InterestItemWrapper, ItemContainer, Interests, } from './style.js';
import Button from "../../../../components/Button";


  const InterestItem = ({ text }) => {
      return (
        <InterestItemWrapper>
          <h6>{text}</h6>
        </InterestItemWrapper>
      );
    };
  
  function SearchCard({data,setDataView, ...props}){
      const handleView = ()=>{
        setDataView((prev)=>({...prev, data:data, view:true}))
      }

      return(
         
              <WrapperCard>
                  <Profile>
                  <h4>{data.user_detail.profile.name}</h4>

                  </Profile>
                  <Interests>
                      <h5>Top Interest</h5>
                      <ItemContainer>
                          {data.user_detail.profile.interests.map((el,idx)=>{
                            return(
                              <InterestItem text={el.interest_name} key={idx} />
                            )
                          })}
                         
                       
                  
                      </ItemContainer>
                  </Interests>

                  <WrapperAction>
                    <Button textColor="#4A69CA" borderColor="#4A69CA" onClick={handleView}>
                        View
                    </Button>
                  </WrapperAction>
              </WrapperCard>
            
         
      )
  }
  
  export default SearchCard;