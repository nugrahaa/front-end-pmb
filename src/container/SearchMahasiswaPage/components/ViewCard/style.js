import styled from 'styled-components';


export const WrapperTitle = styled.div`
    h3{
        font-style: normal;
        font-weight: normal;
        font-size: 1.3em;
        line-height: 20px;
    }
`


export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: start;
  align-items: center;
  width:50%;
  
  img {
    margin-right: 0.5em;
    width:22px;
  }
  h5 {
    font-weight: normal;
    margin:0.5rem;
    overflow-wrap:anywhere;
  }
  margin-right: 2em;
`
export const WrapperCard = styled.div`
    background: #FFFFFF;
    mix-blend-mode: normal;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    border-radius: 10px;
    padding:2rem 2rem;
    @media only screen and (max-width:450px){
      width:275px;
    }
`
export const Profile = styled.div`
  display: flex;
  flex-direction: row;
  img.avatar {
    width: 120px;
    height: 120px;
    border-radius: 50%;
  }
  @media only screen and (max-width: 767px) {
    margin-top: 1em;
  }
`;


export const Detail = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 1.75em;
  justify-content: center;
  width:100%;
 
  h4 {
    margin: 0;
    font-weight:bold;
  }
`;

export const DetailWrapper = styled.div`
  display: flex;
  flex-direction: row;


  @media only screen and (max-width: 767px) {
    flex-direction: column;
    h5 {
      margin: 1em 0em 0em 0em;
    }
    img {
      margin: 1em 0.5em 0em 0em;
    }
  }
`;

export const Interests = styled.div`
  margin-top: 1.75em;
  display: flex;
  flex-direction: column;
  h5 {
    font-weight: bold;
    margin: 0;
  }

`;
export const InterestItemWrapper = styled.div`
  margin-right: 0.75em;
  margin-bottom: 0.75em;
  background: #e7f6fd;
  opacity: 0.8;
  width: fit-content;
  height: fit-content;
  border-radius: 20px;
  h6 {
    margin: 0.5em 1em 0.5em 1em;
    color: black;
    font-weight: normal;
  }
`;
export const ItemContainer = styled.div`

  margin-top: 1rem;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const WrapperDescription = styled.div`
    h5 {
        font-weight: bold;
        margin: 0 0 1rem 0;
    }
 
`
export const WrapperAction = styled.div`
    margin-top:1.5rem;    
    display:flex;
    flex-direction:row;
    justify-content:flex-end;
`