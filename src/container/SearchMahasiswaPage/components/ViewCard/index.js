import React from 'react';
import {Profile,WrapperAction, Detail, DetailWrapper, Wrapper,  WrapperCard, InterestItemWrapper, ItemContainer, Interests, } from './style.js';
import Email from "../../../../assets/icons/email.svg"
import Education from "../../../../assets/icons/education.svg";
import LineMediaSocial from "../../../../assets/icons/line.svg";
import Web from "../../../../assets/icons/web.svg";
import Birth from "../../../../assets/icons/birth.svg";
import School from "../../../../assets/icons/school.svg";
import Button from "../../../../components/Button"
const DetailUser = ({ icon, text }) => {
    return (
      <Wrapper>
        <img src={icon} alt="icon"></img>
        <h5>{text}</h5>
      </Wrapper>
    );
  };
  const InterestItem = ({ text }) => {
      return (
        <InterestItemWrapper>
          <h6>{text}</h6>
        </InterestItemWrapper>
      );
    };
  
  const ViewCard = ({data,setDataView,  ...props})=>{
      const handleBack = ()=>{
          setDataView((prev)=>({...prev, view:false}))
      }
  
    
      return(
         
              <WrapperCard>
                  <Profile>
                      <img
                          className="avatar"
                          alt="avatar"
                          src={data.user_detail.profile.photo || "https://github.com/identicons/jonathanfilbert.png"}
                      ></img>
                      <Detail>
                          <h4>{data.user_detail.profile.name}</h4>
                          <DetailWrapper>
                              <DetailUser
                                  icon={Education}
                                  text={data.user_detail.profile.major || "-"}
                              />
                              <DetailUser icon={Birth} text={data.user_detail.profile.birth_date !==null  ? new Date(data.user_detail.profile.birth_date ).toDateString() : "-"} />
                              <DetailUser
                                  icon={LineMediaSocial}
                                  text={data.user_detail.profile.line_id || "-"}
                              />
                          </DetailWrapper>
                          <DetailWrapper>
                              <DetailUser
                                  icon={School}
                                  text={data.user_detail.profile.high_school || "-"}
                              />
                              <DetailUser icon={Email} text={data.user_detail.profile.email || "-"} />
                              <DetailUser
                                  icon={Web}
                                  text={data.user_detail.profile.website || "-"}
                              />
                          </DetailWrapper>
                      </Detail>
                   
                  </Profile>
                  <Interests>
                      <h5>Top Interest</h5>
                      <ItemContainer>
                      
                      {
           data.user_detail.profile.interests.length ===0 ?
 
             <h5 style={{marginLeft:'1rem',marginBottom:'1rem'}}>-</h5>
  
            :

            data.user_detail.profile.interests.map((el, idx)=>{
              return(
                <InterestItem text={el.interest_name} key={idx} />
              )
            }) 
          }
                      </ItemContainer>
                  </Interests>

                  <WrapperAction>
                    <Button textColor="#4A69CA" borderColor="#4A69CA" onClick={handleBack}>
                        Back
                    </Button>
                  </WrapperAction>
              </WrapperCard>
            
         
      )
  }
  
  export default ViewCard