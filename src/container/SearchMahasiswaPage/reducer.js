// import { fromJS } from "immutable";
import {
  FETCH_MAHASISWA,
  FETCH_MAHASISWA_SUCCESS,
  FETCH_MAHASISWA_FAILED,

} from "./constants";

const initialState = {
  data: [],
  error: null,
  isLoaded: false,
  isLoading: false,
};

function searchMahasiswaReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_MAHASISWA:
      
      return {
        ...state,
        isLoading: true,
      };

    case FETCH_MAHASISWA_SUCCESS:
     
      return {
        ...state,
        data: action.payload,
        isLoading: false,
        isLoaded: true,
      };

    case FETCH_MAHASISWA_FAILED:
     
      return {
        ...state,
        data:[],
        error: action.payload,
        isLoading: false,
        isLoaded: false,
      };
    
    default:
      return state;
  }
}



export default searchMahasiswaReducer;
