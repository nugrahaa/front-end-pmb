import axios from "axios";
import {
  FETCH_MAHASISWA_SUCCESS,
  FETCH_MAHASISWA_FAILED,
  FETCH_MAHASISWA,
} from "./constants";
// import DUMMY_MABA from "./dummy";

import { findMahasiswaApi } from "../../api";


// const DUMMY_DATA = [
//   {
//     name:'lala',
//     interests:[{interest_name:'soccer'},{interest_name:'football'}]
//   },
//   {
//     name:'muhammad',
//     interests:[{interest_name:'elektronik'},{interest_name:'computer'}]
//   }
// ]


export function fetchMahasiswa(query) {

  return (dispatch) => {

    dispatch({ type: FETCH_MAHASISWA });
    axios
      .get(findMahasiswaApi(query))
      .then((response) => {
   
        dispatch({
          payload: response.data,
          type: FETCH_MAHASISWA_SUCCESS,
        });
      })
      .catch((error) => {
        dispatch({
          payload: error,
        
          type: FETCH_MAHASISWA_FAILED,
        });
      });
  };
}
