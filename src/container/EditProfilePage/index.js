import React, { useEffect, useState } from "react";
import ProfileHeader from "../ProfilePage/components/ProfileHeader";
import InputWithCounter from "./components/InputWithCounter";
import styles from "../../utils/styles";
import Button from "../../components/Button";
import { push } from "connected-react-router";
import { connect } from "react-redux";
import { fetchProfile } from "../ProfilePage/actions";
import DateInput from "../../components/DateInput";
import { updateProfile, uploadPhoto } from "./actions";
import LoadingFullscreen from "../../components/LoadingFullscreen";
import { getMediaRootApi } from "../../api";
import { Helmet } from "react-helmet";
import {
  WEBSITE_MAX_CHARS,
  LINE_ID_MAX_CHARS,
  HIGHSCHOOL_MAX_CHARS,
  TITLE_MAX_CHARS,
} from "./constants";

const EditProfilePage = ({ ...props }) => {
  const {
    push,
    profile,
    fetchProfile,
    about,
    updateProfile,
    isLoading,
    uploadPhoto,
  } = props;
  const [isInputValid, setIsInputValid] = useState(false);
  const [isError, setIsError] = useState(false);
  const [titleValue, setTitleValue] = useState("");
  const [highschoolValue, setHighschoolValue] = useState("");
  const [lineIdValue, setLineIdValue] = useState("");
  const [websiteValue, setWebsiteValue] = useState("");
  const [birthdayValue, setBirthdayValue] = useState("");
  const [photoValue, setPhotoValue] = useState("");
  const [isPrivate, setIsPrivate] = useState(false);

  useEffect(() => {
    fetchProfile();
  }, [fetchProfile]);

  useEffect(() => {
    setTitleValue(about);
    setHighschoolValue(profile.high_school);
    setLineIdValue(profile.line_id);
    setWebsiteValue(profile.website);
    setBirthdayValue(profile.birth_date === null ? "" : profile.birth_date);
    setPhotoValue(profile.photo);
    setIsPrivate(profile.is_private);
  }, [
    about,
    profile.birth_date,
    profile.high_school,
    profile.is_private,
    profile.line_id,
    profile.photo,
    profile.website,
  ]);

  useEffect(() => {
    if (
      !isEmpty(titleValue) &
      !isEmpty(highschoolValue) &
      !isEmpty(lineIdValue) &
      !isEmpty(birthdayValue) &
      (typeof isPrivate === "boolean") &
      !isError &
      (titleValue?.length < TITLE_MAX_CHARS) &
      (highschoolValue?.length < HIGHSCHOOL_MAX_CHARS) &
      (lineIdValue?.length < LINE_ID_MAX_CHARS) &
      (websiteValue?.length < WEBSITE_MAX_CHARS)
    ) {
      setIsError(false);
      setIsInputValid(true);
    } else {
      setIsInputValid(false);
    }
  }, [
    isPrivate,
    birthdayValue,
    highschoolValue,
    isError,
    lineIdValue,
    titleValue,
    websiteValue,
  ]);

  const handleSubmit = () => {
    // if photoValue is a file, upload it
    if (photoValue.name) {
      uploadPhoto(photoValue)
        .then((res) => {
          const data = {
            profile: {
              about: titleValue,
              birth_date: birthdayValue,
              high_school: highschoolValue,
              line_id: lineIdValue,
              website: websiteValue,
              photo: ` ${getMediaRootApi}${res.data.photo}`,
              is_private: isPrivate,
            },
          };
          updateProfile(data)
            .then((res) => {
              push("/profile");
              alert(res);
            })
            .catch((err) => {
              alert(err, { type: "error" });
            });
        })
        .catch((err) => {
          alert(err, { type: "error" });
        });
    }
    // if not a file (a string), then dont upload it
    else {
      const data = {
        profile: {
          about: titleValue,
          birth_date: birthdayValue,
          high_school: highschoolValue,
          line_id: lineIdValue,
          website: websiteValue,
          is_private: isPrivate,
        },
      };
      updateProfile(data)
        .then((res) => {
          push("/profile");
          alert(res);
        })
        .catch((err) => alert(err, { type: "error" }));
    }
  };

  const isEmpty = (value) => {
    if (!value) {
      return true;
    }
    // remove spaces from the word and check its length
    return value.replace(/\s+/g, "").length === 0;
  };

  const handleType = (value, type) => {
    // const validWebRegex = /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9A-Z]+\.[a-z]+(\/[a-zA-Z0-9#]+\/?)*$/gm;
    if (type === "title") {
      setTitleValue(value);
    } else if (type === "highschool") {
      setHighschoolValue(value);
    } else if (type === "lineId") {
      setLineIdValue(value);
    } else if (type === "website") {
      setWebsiteValue(value);
      // if (!value.match(validWebRegex) && !isEmpty(value)) {
      //   setIsError(true);
      // } else {
      //   setIsError(false);
      // }
    } else if (type === "birthday") {
      setBirthdayValue(value);
    }
  };

  const handlePhotoChange = (file) => {
    setPhotoValue(file);
  };

  if (isLoading) {
    return <LoadingFullscreen />;
  } else {
    return (
      <div>
        <Helmet>
          <title>PMB 2020 - Edit Profile</title>
        </Helmet>
        <h2>Edit Profile</h2>
        <ProfileHeader
          fullName={profile.name}
          isVerified
          photoUrl={photoValue}
          title={titleValue}
          editable
          onPhotoChange={(file) => handlePhotoChange(file)}
        />
        {/* debugger start */}
        {/* <div>{`title: ${titleValue}`}</div>
        <div>{`highschool: ${highschoolValue}`}</div>
        <div>{`lineId: ${lineIdValue}`}</div>
        <div>{`website: ${websiteValue}`}</div>
        <div>{`birthday: ${moment(birthdayValue).format("DD MMM YYYY")}`}</div>{" "}
        <div>{`photo: ${photoValue}`}</div> */}
        {/* <div>{`Profile is private is ${isPrivate}`}</div> */}
        {/* {/* debugger end */}
        <div style={{ ...styles.mvxl }}>
          <div style={{ ...styles.mbl }}>
            <h4 style={{ margin: 0, marginBottom: 5 }}>Title</h4>
            <InputWithCounter
              isCharsLimited
              charsLimit={TITLE_MAX_CHARS}
              placeholder={about}
              value={titleValue}
              onChange={(event) => handleType(event.target.value, "title")}
            />
          </div>
          <div style={{ ...styles.mbl }}>
            <h4 style={{ margin: 0, marginBottom: 5 }}>Birthday</h4>
            <DateInput
              onSelectDate={(date) => handleType(date, "birthday")}
              value={birthdayValue}
            />
          </div>
          <div style={{ ...styles.mbl }}>
            <h4 style={{ margin: 0, marginBottom: 5 }}>Highschool</h4>
            <InputWithCounter
              placeholder={profile.high_school}
              isCharsLimited
              charsLimit={HIGHSCHOOL_MAX_CHARS}
              value={highschoolValue}
              onChange={(event) => handleType(event.target.value, "highschool")}
            />
          </div>
          <div style={{ ...styles.mbl }}>
            <h4 style={{ margin: 0, marginBottom: 5 }}>Email</h4>
            <InputWithCounter
              placeholder={profile.email}
              disabled
              style={{ backgroundColor: "transparent" }}
            />
          </div>
          <div style={{ ...styles.mbl }}>
            <h4 style={{ margin: 0, marginBottom: 5 }}>LINE ID</h4>
            <InputWithCounter
              isCharsLimited
              charsLimit={LINE_ID_MAX_CHARS}
              placeholder={profile.line_id}
              value={lineIdValue}
              onChange={(event) => handleType(event.target.value, "lineId")}
            />
          </div>
          <div style={{ ...styles.mbl }}>
            <h4 style={{ margin: 0, marginBottom: 5 }}>Website</h4>
            <InputWithCounter
              isCharsLimited
              charsLimit={WEBSITE_MAX_CHARS}
              placeholder={profile.website}
              value={websiteValue}
              onChange={(event) => handleType(event.target.value, "website")}
            />
          </div>
          <div style={{ ...styles.mbl, ...styles.flexDirCol }}>
            <h4 style={{ margin: 0 }}>Private Mode</h4>
            <div style={{ ...styles.flexDirRow, ...styles.mtl }}>
              <input
                type="radio"
                value={isPrivate}
                checked={isPrivate}
                onClick={() => setIsPrivate((prev) => !prev)}
              />
              <div style={{ ...styles.mlm }}>{isPrivate ? "On" : "Off"}</div>
            </div>
            <div style={{ ...styles.mtm, ...styles.fwb }}>
              {isPrivate
                ? `Your profile won't appear in search results`
                : `Your profile will appear in search results`}
            </div>
          </div>
        </div>
        <div
          style={{
            flex: 1,
            ...styles.flexDirRow,
            justifyContent: isError ? "space-between" : "flex-end",
          }}
        >
          {isError && (
            <div style={{ color: "red" }}>Please input a valid website</div>
          )}
          <div style={{ ...styles.flexDirRow, ...styles.aic }}>
            <div
              style={{ ...styles.mrxxl, cursor: "pointer" }}
              onClick={() => push("/profile")}
            >
              Cancel
            </div>
            <Button disabled={!isInputValid} onClick={() => handleSubmit()}>
              Save
            </Button>
          </div>
        </div>
      </div>
    );
  }
};

const mapStateToProps = (state) => ({
  profile: state.profileReducer.profile,
  group: state.profileReducer.group,
  isLoading: state.profileReducer.isLoading,
  batch: state.profileReducer.batch,
  usUserMaba: state.profileReducer.isUserMaba,
  about: state.profileReducer.about,
});

const mapDispatchToProps = (dispatch) => ({
  push: (url) => dispatch(push(url)),
  fetchProfile: () => dispatch(fetchProfile()),
  updateProfile: (data) => dispatch(updateProfile(data)),
  uploadPhoto: (data) => dispatch(uploadPhoto(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditProfilePage);
