import { postEditProfileApi, uploadPictureApi } from "../../api";
import axios from "axios";
import {
  UPDATE_PROFILE,
  UPDATE_PROFILE_SUCCESS,
  UPDATE_PROFILE_FAILED,
  UPLOAD_PHOTO,
} from "../ProfilePage/constants";

export const uploadPhoto = (file) => {
  const formData = new FormData();
  formData.append("photo", file);
  const config = {
    headers: {
      "content-type": "multipart/form-data",
    },
  };
  return (dispatch) => {
    dispatch({
      type: UPLOAD_PHOTO,
    });
    return new Promise((resolve, reject) => {
      axios
        .post(uploadPictureApi, formData, config)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err.message);
        });
    });
  };
};

export const updateProfile = (data) => {
  return (dispatch) => {
    dispatch({
      type: UPDATE_PROFILE,
    });
    return new Promise((resolve, reject) => {
      axios({
        method: "patch",
        url: postEditProfileApi,
        data: data,
      })
        .then((response) => {
          dispatch({
            type: UPDATE_PROFILE_SUCCESS,
            payload: response.data,
          });
          resolve("Edited Successfully");
        })
        .catch((err) => {
          dispatch({
            type: UPDATE_PROFILE_FAILED,
            payload: err,
          });
          reject(err.message);
        });
    });
  };
};
