import React from "react";
import Input from "../../../components/Input";
import styles from "../../../utils/styles";

const InputWithCounter = ({
  placeholder,
  value,
  isCharsLimited = false,
  charsLimit,
  handleType,
  disabled,
  ...rest
}) => {
  return (
    <div>
      <Input
        placeholder={placeholder}
        value={value}
        disabled={disabled}
        onChange={(event) => handleType(event.target.value, "lineId")}
        {...rest}
        style={{
          color: value?.length > charsLimit ? "red" : "black",
          backgroundColor: "transparent",
        }}
      />
      {isCharsLimited && (
        <div
          style={{
            width: "100%",
            ...styles.flexDirRow,
            justifyContent: "flex-end",
            ...styles.mtxs,
          }}
        >
          <div style={{ ...styles.flexDirRow }}>
            <div
              style={{
                color: value?.length > charsLimit ? "red" : "black",
              }}
            >
              {value?.length}
            </div>
            <div>/</div>
            <div>{charsLimit}</div>
          </div>
        </div>
      )}
    </div>
  );
};

export default InputWithCounter;
