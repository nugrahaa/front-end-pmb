import {
  POST_INTEREST,
  POST_INTEREST_SUCCESS,
  POST_INTEREST_FAILED,
} from "../ProfilePage/constants";

const initialState = {
  isLoading: false,
  error: null,
};

const editInterestPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_INTEREST:
      return {
        ...state,
        isLoading: true,
      };
    case POST_INTEREST_SUCCESS:
      return {
        ...state,
        isLoading: false,
      };
    case POST_INTEREST_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default editInterestPageReducer;
