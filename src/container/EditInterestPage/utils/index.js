import Fuse from "fuse.js";

const OPTIONS = {
  threshold: 0.1,
};

const searchArrayByKeyword = (array, keyword) => {
  const fuse = new Fuse(array, OPTIONS);
  const result = [...fuse.search(keyword).map((result) => result.item)];
  return result;
};

export { searchArrayByKeyword };
