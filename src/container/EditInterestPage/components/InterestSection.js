import React, { useState, useEffect } from "react";
import Card from "../../../components/Card";
import styles from "../../../utils/styles";
import InterestSearch from "./InterestSearch";
import InterestBubble from "../../ProfilePage/components/InterestBubble";
import { SortableContainer, SortableElement } from "react-sortable-hoc";
import arrayMove from "array-move";
import Emoji from "../../../components/Emoji";

const SortableItem = SortableElement(
  ({ text, isDeletable, onDelete, isSorting }) => (
    <InterestBubble
      text={text}
      isDeletable={isDeletable}
      onDelete={() => onDelete(text)}
      className="sortableHelper"
      moreStyles={{
        zIndex: 1,
        boxShadow: isSorting ? "0px 4px 4px rgba(0,0,0,0.1)" : "none",
      }}
    />
  )
);

const SortableList = SortableContainer(({ interests, isSorting, onDelete }) => {
  return (
    <div
      style={{
        ...styles.flexDirRow,
        ...styles.mbl,
        width: "100%",
        maxWidth: "100%",
        flexWrap: "wrap",
      }}
    >
      {interests.map((value, index) => (
        <SortableItem
          text={value}
          index={index}
          key={`Item-${index}`}
          onDelete={() => onDelete(value)}
          isDeletable={true}
          isSorting={isSorting}
        />
      ))}
    </div>
  );
});

const InterestSection = ({
  title,
  onDeleteInterest,
  interestsList,
  onAddInterest,
  searchPreset,
  onChange,
  emoji,
}) => {
  const [isBeingSorted, setIsBeingSorted] = useState(false);
  const [interestSortedList, setInterestSortedList] = useState([]);

  useEffect(() => {
    setInterestSortedList(interestsList);
  }, [interestsList]);

  const handleEndSort = ({ oldIndex, newIndex }) => {
    let newArray = arrayMove(interestSortedList, oldIndex, newIndex);
    onChange(newArray);
    setInterestSortedList(newArray);
    setIsBeingSorted(false);
  };

  const handleDelete = (text) => {
    setInterestSortedList(onDeleteInterest(text));
  };

  const handleAddInterest = (text) => {
    const interest = onAddInterest(text);
    setInterestSortedList(interest);
  };

  return (
    <Card containerStyle={{ ...styles.pam, ...styles.mbxl }}>
      <div style={{ ...styles.flexDirRow }}>
        <div style={{ width: "1em", ...styles.mrxs }}>
          <Emoji emoji={emoji} />
        </div>
        <h3 style={{ margin: 0, ...styles.mbl }}>{title}</h3>
      </div>
      <InterestSearch
        onAdd={(text) => handleAddInterest(text)}
        interestSearchPresets={searchPreset}
      />
      <SortableList
        interests={interestSortedList}
        onSortEnd={(oldIndex, newIndex) => handleEndSort(oldIndex, newIndex)}
        pressDelay={200}
        axis="xy"
        updateBeforeSortStart={() => setIsBeingSorted(true)}
        isSorting={isBeingSorted}
        onDelete={(text) => handleDelete(text)}
      />
    </Card>
  );
};

export default InterestSection;
