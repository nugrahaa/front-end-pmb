import React, { useState, useEffect } from "react";
import SearchBar from "../../../components/SearchBar";
import styles from "../../../utils/styles";
import styled from "styled-components";
import { searchArrayByKeyword } from "../utils";

const InterestSearchResult = ({
  text,
  handleClick,
  isCustomQuery,
  ...rest
}) => {
  return (
    <div
      {...rest}
      onClick={() => handleClick(text)}
      style={{ display: "flex", flexDirection: "row" }}
    >
      {isCustomQuery && <div style={{ ...styles.mrm }}>+</div>}
      {isCustomQuery ? `Add ${text} to your interest` : text}
    </div>
  );
};

const InterestSearchWrapper = styled.div`
  width: 100%;
  position: relative;

  .search-result-container {
    position: absolute;
    background-color: white;
    z-index: 90;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    border-radius: 10px;
    right: 0;
    left: 0;
    border: 1px solid black;
    overflow-y: auto;
    max-height: 150px;
  }

  .search-result-item {
    border-top: 1px solid black;
    padding-top: 1em;
    padding-bottom: 1em;
    padding: 10px;
  }
  .search-result-item:first-child {
    border: none;
    font-style: italic;
  }
  .search-result-item:hover {
    cursor: pointer;
    background-color: black;
    color: white;
    padding-left: 20px;
  }
`;

const InterestSearch = ({ onAdd, interestSearchPresets }) => {
  const [isResultVisible, setIsResultVisible] = useState(true);
  const [keyword, setKeyword] = useState("");
  const [searchResult, setSearchResult] = useState(interestSearchPresets);
  const handleType = (text) => {
    setKeyword(text);
    setSearchResult(searchArrayByKeyword(interestSearchPresets, keyword));
  };
  useEffect(() => {
    if (keyword === "") {
      setIsResultVisible(false);
    } else {
      setIsResultVisible(true);
    }
  }, [keyword]);

  const handleAddInterest = (text) => {
    onAdd(text);
    setIsResultVisible(false);
    setKeyword("");
  };

  return (
    <InterestSearchWrapper>
      <SearchBar
        placeholder="Search interests"
        onQueryChange={(text) => handleType(text)}
        query={keyword}
      />
      {/* search result section start */}
      {isResultVisible && (
        <div className="search-result-container">
          <InterestSearchResult
            className="search-result-item"
            text={keyword}
            handleClick={(text) => handleAddInterest(text)}
            isCustomQuery
          />
          {searchResult.map((result, index) => {
            return (
              <InterestSearchResult
                key={`interest number ${index}`}
                className="search-result-item"
                text={result}
                handleClick={(result) => handleAddInterest(result)}
              />
            );
          })}
        </div>
      )}
      {/* search result section end */}
    </InterestSearchWrapper>
  );
};

export default InterestSearch;
