/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import styles from "../../utils/styles";
import Button from "../../components/Button";
import InterestSection from "./components/InterestSection";
import {
  TECHNOLOGY_INTERESTS,
  NON_TECHNOLOGY_INTERESTS,
  ENTERTAINMENT_INTERESTS,
} from "./constants";
import { fetchProfile } from "../ProfilePage/actions";
import { connect } from "react-redux";
import LoadingFullscreen from "../../components/LoadingFullscreen";
import { push } from "connected-react-router";
import { postInterest } from "./actions";
import { Helmet } from "react-helmet";

const EditInterestPage = ({
  fetchProfile,
  interests,
  isLoading,
  push,
  postInterest,
  isPostInterestLoading,
}) => {
  const [technologyInterests, setTechnologyInterests] = useState([]);
  const [nonTechnologyInterests, setNonTechnologyInterests] = useState([]);
  const [entertainmentInterests, setEntertainmentInterests] = useState([]);

  useEffect(() => {
    fetchProfile();
  }, []);

  useEffect(() => {
    if (!isLoading && interests.length > 0) {
      setTechnologyInterests(interests[0].data);
      setNonTechnologyInterests(interests[1].data);
      setEntertainmentInterests(interests[2].data);
    }
  }, [interests]);

  const handleSubmit = () => {
    const interestData = {
      technology: technologyInterests,
      non_tech: nonTechnologyInterests,
      entertainment: entertainmentInterests,
    };
    postInterest(interestData)
      .then((res) => {
        alert(res);
        push("/profile");
      })
      .catch((err) => alert(err, { type: "error" }));
  };

  const handleDeleteTechnologyInterests = (array, value) => {
    const result = array.filter((item) => item !== value);
    setTechnologyInterests(result);
    return result;
  };

  const handleDeleteNonTechnologyInterests = (array, value) => {
    const result = array.filter((item) => item !== value);
    setNonTechnologyInterests(result);
    return result;
  };

  const handleDeleteEntertainmentInterests = (array, value) => {
    const result = array.filter((item) => item !== value);
    setEntertainmentInterests(result);
    return result;
  };

  const handleAdd = (array, value) => {
    let result = [...array];
    result.push(value);
    array.push(value);
    return result;
  };

  const handleTechnologyInterestsChange = (newArray) => {
    setTechnologyInterests(newArray);
  };

  const handleNonTechnologyInterestsChange = (newArray) => {
    setNonTechnologyInterests(newArray);
  };

  const handleEntertainmentInterestsChange = (newArray) => {
    setEntertainmentInterests(newArray);
  };

  if (isLoading || isPostInterestLoading) {
    return <LoadingFullscreen />;
  } else {
    return (
      <div>
        <Helmet>
          <title>PMB 2020 - Edit Interest</title>
        </Helmet>
        <h2>Edit Interest</h2>
        <div style={{ ...styles.mvxl }}>
          <InterestSection
            searchPreset={TECHNOLOGY_INTERESTS}
            title="Technology"
            interestsList={technologyInterests}
            onDeleteInterest={(value) =>
              handleDeleteTechnologyInterests(technologyInterests, value)
            }
            onAddInterest={(value) => handleAdd(technologyInterests, value)}
            onChange={(newArray) => handleTechnologyInterestsChange(newArray)}
            emoji="💻"
          />
        </div>
        <div style={{ ...styles.mvxl }}>
          <InterestSection
            searchPreset={NON_TECHNOLOGY_INTERESTS}
            title="Non Technology"
            interestsList={nonTechnologyInterests}
            onDeleteInterest={(value) =>
              handleDeleteNonTechnologyInterests(nonTechnologyInterests, value)
            }
            onAddInterest={(value) => handleAdd(nonTechnologyInterests, value)}
            onChange={(newArray) =>
              handleNonTechnologyInterestsChange(newArray)
            }
            emoji="🏀"
          />
        </div>
        <div style={{ ...styles.mvxl }}>
          <InterestSection
            searchPreset={ENTERTAINMENT_INTERESTS}
            title="Entertainment"
            interestsList={entertainmentInterests}
            onDeleteInterest={(value) =>
              handleDeleteEntertainmentInterests(entertainmentInterests, value)
            }
            onAddInterest={(value) => handleAdd(entertainmentInterests, value)}
            onChange={(newArray) =>
              handleEntertainmentInterestsChange(newArray)
            }
            emoji="🎤"
          />
        </div>
        <h5 style={{ color: "#4A69CA", fontStyle: "italic", ...styles.fwb }}>
          * hold an interest to change its order
        </h5>
        <div
          style={{
            flex: 1,
            ...styles.flexDirRow,
            justifyContent: "flex-end",
            ...styles.aic,
          }}
        >
          <div
            style={{ ...styles.mrxxl, cursor: "pointer" }}
            onClick={() => push("/profile")}
          >
            Cancel
          </div>
          <Button onClick={() => handleSubmit()}>Save</Button>
        </div>
      </div>
    );
  }
};

const mapStateToProps = (state) => ({
  interests: state.profileReducer.interests,
  isLoading: state.profileReducer.isLoading,
  isPostInterestLoading: state.editInterestPageReducer.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
  fetchProfile: () => dispatch(fetchProfile()),
  push: (url) => dispatch(push(url)),
  postInterest: (interest) => dispatch(postInterest(interest)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditInterestPage);
