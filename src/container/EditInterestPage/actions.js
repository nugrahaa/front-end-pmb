import { postInterestApi } from "../../api";

import axios from "axios";

import {
  POST_INTEREST,
  POST_INTEREST_SUCCESS,
  POST_INTEREST_FAILED,
} from "../ProfilePage/constants";

export const postInterest = (interest) => {
  return (dispatch) => {
    dispatch({
      type: POST_INTEREST,
    });
    return new Promise((resolve, reject) => {
      axios({
        method: "post",
        url: postInterestApi,
        data: interest,
      })
        .then(() => {
          dispatch({
            type: POST_INTEREST_SUCCESS,
          });
          resolve("Interests updated successfully");
        })
        .catch((err) => {
          dispatch({
            type: POST_INTEREST_FAILED,
            payload: err,
          });
          reject(err.message);
        });
    });
  };
};
