import {
  FETCH_FRIENDS,
  FETCH_FRIENDS_FAILED,
  FETCH_FRIENDS_SUCCESS,
  FETCH_STATISTIC,
  FETCH_STATISTIC_FAILED,
  FETCH_STATISTIC_SUCCESS,
} from "./constants"

const initialState = {
  friends: [],
  friendsApprove: [],
  isLoading: false,
  error: false,
  isLoaded: false,
  statisticOne: {},
  statisticTwo: {},

}

function friendPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_FRIENDS:
      return {
        ...state,
        isLoading: true
      }
    case FETCH_FRIENDS_SUCCESS:
      return {
        ...state,
        friends: action.payload.filter((friend) => friend.status !== "APPROVED"),
        friendsSuccess: action.payload.filter((friend) => friend.status === "APPROVED"),
        isLoading: false,
        isLoaded: true
      };
    case FETCH_FRIENDS_FAILED:
      return {
        error: action.payload,
        isLoading: false,
      }
    case FETCH_STATISTIC:
      return {
        ...state,
        isLoading: true,
      }
    case FETCH_STATISTIC_SUCCESS:
      // console.log(action.payload)
      return {
        ...state,
        isLoading: false,
        statisticOne: action.payload.filter((stat) => stat.kenalan_task.id === 1)
      }
    case FETCH_STATISTIC_FAILED:
      return {
        ...state,
        isLoading: false,
        error: true
      }

    default:
      return state;
  }
}

export default friendPageReducer;

