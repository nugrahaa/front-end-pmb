import axios from "axios"

import {
  FETCH_FRIENDS,
  FETCH_FRIENDS_FAILED,
  FETCH_FRIENDS_SUCCESS,
  FETCH_STATISTIC,
  FETCH_STATISTIC_FAILED,
  FETCH_STATISTIC_SUCCESS,
} from "./constants"

import { fetchFriendListApi, fetchKenalanStatisticsApi } from "../../api"

export function fetchFriends() {
  return dispatch => {
    dispatch({ type: FETCH_FRIENDS })
    axios
    .get(fetchFriendListApi)
    .then(response => {
      dispatch({
        type: FETCH_FRIENDS_SUCCESS,
        payload: response.data
      })
    })
    .catch(error => {
      dispatch({
        type: FETCH_FRIENDS_FAILED,
        payload: error
      })
    })
  }
}

export function fetchStatistics() {
  return dispatch => {
    dispatch({ type: FETCH_STATISTIC })
    axios
    .get(fetchKenalanStatisticsApi)
    .then(response => {
      dispatch({
        type: FETCH_STATISTIC_SUCCESS,
        payload: response.data
      })
    })
    .catch(error => {
      dispatch({
        type: FETCH_STATISTIC_FAILED,
        payload: error
      })
    })
  }
}



