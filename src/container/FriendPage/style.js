import styled from "styled-components";

export const FriendPageStyle = styled.div`
  font-family: Metropolis;
  .statistics-title,
  .friend-requests-title {
    font-family: Metropolis;
    font-style: normal;
    font-weight: normal;
    font-size: 24px;
    line-height: 20px;

    color: #1b274b;
  }

  .FriendPage__event-tab-container {
    display: flex;
    flex-direction: row;
    font-family: Metropolis;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 16px;

    text-align: center;

    width: 100%;
    margin: 1rem auto;
  }

  .FriendPage__tab {
    padding: 0.6rem 2.5rem;
    color: #4a69ca;
    cursor: pointer;
    width: 100%;

    border-radius: 10px 10px 0px 0px;
    background-color: #ffffff;
  }

  .FriendPage__controls__pagination {
    display: flex;
    justify-content: flex-end;
    align-items: flex-end;
    overflow: hidden;
    width: 100%;
    margin-top: 2rem;
    margin-right: 10rem;
  }

  .FriendPage_friendlist {
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  .FriendPage__tab:hover {
    background-color: #4a69ca;
    color: white;
    box-shadow: 0px -2px 4px rgba(0, 0, 0, 0.25);
    border-radius: 10px 10px 0px 0px;
  }

  .FriendPage__tab__active {
    color: white;
    background-color: #4a69ca;
    box-shadow: 0px -2px 4px rgba(0, 0, 0, 0.25);
    border-radius: 10px 10px 0px 0px;
  }

  .friendcard {
    width: 100%;
  }

  @media (min-width: 481px) and (max-width: 767px) {
    .FriendPage__event-tab-container {
      width: 100%;
      margin: 1rem auto;
    }
  }

  @media (min-width: 321px) and (max-width: 480px) {
    .FriendPage__event-tab-container {
      width: 100%;
      margin: 1rem auto;
    }
  }

  @media (max-width: 320px) {
    .FriendPage__event-tab-container {
      width: 100%;
      margin: 1rem auto;
    }
  }
`;
