export const FETCH_FRIENDS = "src/FriendPage/FETCH_FRIENDS";
export const FETCH_FRIENDS_SUCCESS =
  "src/FriendPage/FETCH_FRIENDS_SUCCESS";
export const FETCH_FRIENDS_FAILED = "src/FriendPage/FETCH_FRIENDS_FAILED";

export const FETCH_STATISTIC = "src/FriendPage/FETCH_STATISTIC";
export const FETCH_STATISTIC_SUCCESS =
  "src/FriendPage/FETCH_STATISTIC_SUCCESS";
export const FETCH_STATISTIC_FAILED = "src/FriendPage/FETCH_STATISTIC_FAILED";