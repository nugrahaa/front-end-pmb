import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import PropTypes from "prop-types";

import Loading from "../../components/Loading";
import EmptyCard from "../../components/EmptyCard";
import FriendCard from "../../components/FriendCard";
import SearchBar from "../../components/SearchBar";
// import StatisticsFriendCard from "../../components/StatisticsFriendCard";
import Pagination from "../../components/Pagination";
import { isMaba } from "../../selectors/user";
import { FriendPageStyle } from "./style";
import { Helmet } from "react-helmet";
import {
    fetchFriends,
    fetchStatistics,
} from "./actions";
import {
    patchKenalanById,
} from "../FormFriendPage/actions"

function FriendPage(props) {

    const [isFriendsTab, setIsFriendsTab] = useState(true);

    useEffect(() => {
        props.fetchFriends()
       
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const [filteredFriends, setFilteredFriends] = useState(props.friends.filter((friend) => friend.status !== "APPROVED"))
    const [searchQuery, setSearchQuery] = useState("");

    const [currentPage, setCurrentPage] = useState(1);
    const [paginatedRecords, setPaginatedRecords] = useState([]);

    useEffect(() => {
        if (props.friends?.length !== 0 || props.friendsSuccess?.length !== 0 ) {
            if (isFriendsTab) {
                setFilteredFriends(props.friends)
                setPaginatedRecords(props.friends?.slice(0, 5))
            } else {
                setFilteredFriends(props.friendsSuccess)
                setPaginatedRecords(props.friendsSuccess?.slice(0, 5))
            }

        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.friends, props.friendsSuccess, isFriendsTab])

    const handlePageChange = (newPage) => setCurrentPage(newPage);

    const handlePaginatedRecordsChange = (paginatedRecords) =>
        setPaginatedRecords(paginatedRecords);

    const handleSearchBarInput = (target) => {
        let allfriends = isFriendsTab ? props.friends : props.friendsSuccess
        setFilteredFriends([...props.friends]);
        setSearchQuery(target);

        if (target !== "") {
            filterSearch(target);
        } else {
            setFilteredFriends([...allfriends]);
            setPaginatedRecords([...allfriends]);
        }
    };

    const filterSearch = async (target) => {
        let allfriends = isFriendsTab ? props.friends : props.friendsSuccess
        // console.log(allfriends)
        let friends = allfriends.filter((friend) =>
            props.isUserMaba ?
                friend.user_elemen.profile.name?.toLowerCase().includes(target.toLowerCase()) :
                friend.user_maba.profile.name?.toLowerCase().includes(target.toLowerCase())
        )
        await setFilteredFriends(friends);
        await setPaginatedRecords(friends);
    };

    const renderCards = (data) =>
        data.map((friend) => (
            <FriendCard
                friend={friend}
                isUserMaba={props.isUserMaba}
                className="friendcard"
                handleApprove={(id, data) => props.patchKenalanById(id, data)}
                handleReject={() => props.push(`/friends/reject/${friend.id}`)}
                handleEdit={() => props.push(`/friends/add/${friend.id}`)}
            />
        ));

    let friendCards = null;
    if (paginatedRecords.length !== 0) {
        friendCards = renderCards(paginatedRecords);
    } else {
        friendCards = <EmptyCard text="Don't Worry! You will make friends soon!" />
    }

    return (
        <FriendPageStyle>
            <Helmet>
                <title>PMB 2020 - Friends</title>
            </Helmet>
            {
                props.isLoading ? <Loading /> :
                    <>
                        <div className="FriendPage__event-tab-container">
                            <div
                                className={`FriendPage__tab ${
                                    isFriendsTab ? "FriendPage__tab__active" : ""
                                    }`}
                                onClick={() => setIsFriendsTab(true)}
                            >
                                Friends
                            </div>
                            <div
                                className={`FriendPage__tab ${
                                    !isFriendsTab ? "FriendPage__tab__active" : ""
                                    }`}
                                onClick={() => setIsFriendsTab(false)}
                            >
                                My Friends
                            </div>
                        </div>
                        {/* {
                            props.isUserMaba && Array.isArray(props.statistic) &&
                            <div style={{ width: "100%", margin: "2em auto" }}>
                                <h2 style={{padding: "1rem 0"}}>Statistic</h2>
                                <StatisticsFriendCard stats={props.statistic} />
                            </div>
                        } */}
                         {
                            !props.isUserMaba &&
                            <h2 style={{padding: "1rem 0"}}>Friend Requests</h2>
                        }
                        <div style={{ width: "100%", margin: "0 auto" }}>
                            <SearchBar
                                placeholder="Search friends by name"
                                onQueryChange={handleSearchBarInput}
                                query={searchQuery}
                            />
                        </div>
                        <div className="FriendPage__controls__pagination">
                            <Pagination
                                records={filteredFriends}
                                itemPerPage={5}
                                currentPage={currentPage}
                                numberOfPagesShown={5}
                                onPageChange={(nextPageNumber) => handlePageChange(nextPageNumber)}
                                onPaginatedRecordsChange={(paginatedRecords) =>
                                    handlePaginatedRecordsChange(paginatedRecords)
                                }
                            />
                        </div>
                        <div className="FriendPage_friendlist">
                            {friendCards}
                        </div>
                    </>
            }
        </FriendPageStyle>
    );
}

FriendPage.propTypes = {
    fetchFriends: PropTypes.func.isRequired,
    friends: PropTypes.arrayOf(
        PropTypes.shape({})
    ),
    isLoading: PropTypes.bool,
    isUserMaba: PropTypes.bool,
};

function mapStateToProps(state) {
    return {
        friends: state.friendReducer.friends,
        friendsSuccess: state.friendReducer.friendsSuccess,
        isUserMaba: isMaba(state),
        isLoading: state.friendReducer.isLoading,
        isLoaded: state.friendReducer.isLoaded,
        statistic: state.friendReducer.statisticOne
    };
}

function mapDispatchToProps(dispatch) {
    return {
        push: (url) => dispatch(push(url)),
        fetchFriends: () => dispatch(fetchFriends()),
        patchKenalanById: (id, data) => dispatch(patchKenalanById(id, data)),
        fetchStatistics: () => dispatch(fetchStatistics())
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FriendPage);

