import React, { useState, useEffect } from "react";
import { connect } from "react-redux";

import { Container, PuzzleWrapper } from "./style";
import Loading from "../../components/Loading";
import SearchBar from "../../components/SearchBar";
import PuzzleCard from "./components/PuzzleCard";
import { fetchMaba, postMaba } from "./actions";
import {Helmet} from 'react-helmet';



function Puzzle({ maba, postMaba, fetchMaba, isLoading, isLoaded, ...props }) {
  const [filteredMaba, setFilteredMaba] = useState([
    /* kepikirannya initial valuenya props.maba
      trs difilter berdasarkan query
      jadi di render pake ini aja ya dari awal(?) */
    ...maba,
  ]);
  const [searchQuery, setSearchQuery] = useState("");

  // ini tadinya dikosongin dependencynya
  // soalnya mikirnya dia make componentDidMount doang
  useEffect(() => {
    fetchMaba();
  }, [fetchMaba ]);

  // componentWillReceiveProps equivalent
  useEffect(() => {
    setFilteredMaba([...maba]);
  }, [maba]);

  // buat handle searchbar input
  const handleSearchBarInput = (target) => {
    setFilteredMaba([...maba]);
    setSearchQuery(target);

    if (target !== "") {
      filterSearch(target);
    } else {
      setFilteredMaba([...maba]);
    }
  };

  const filterSearch = (target) => {
    // tadinya mau pake regex tp katanya cepetan filter -> includes
    const dataFiltered = maba.filter((mb) => {
      return (
        mb.target_maba.profile.name
          .toLowerCase()
          .includes(target.toLowerCase()) ||
        mb.target_maba.profile.interests.find((y) => {
          return y.interest_name.toLowerCase().includes(target.toLowerCase());
        })
      );
    });

    setFilteredMaba(dataFiltered);
  };

  return (
    <Container>
      <Helmet>
        {maba.length !== 0 ?
          <title>
            PMB 2020 - Puzzle {maba[0].target_maba.group.big_group.name}
          </title>
          :
          <title>
            PMB 2020 - Puzzle
          </title>}
      </Helmet>
      <h2>
        {maba.length !== 0
          ? `Puzzle ${maba[0].target_maba.group.big_group.name}`
          : "Group not found"}
      </h2>
      {/* <Input placeholder="Search friends or interests"/> */}
      <SearchBar
        placeholder="Search friends or interests"
        onQueryChange={handleSearchBarInput}
        query={searchQuery}
      />

      {isLoading ? (
        <Loading />
      ) : (
        <PuzzleWrapper>
          {maba.length !== 0 && filteredMaba.length !== 0 ? (
            filteredMaba.map((el, i) => (
              <PuzzleCard key={el.id} data={el} postMaba={postMaba} fetchMaba={fetchMaba} />
            ))
          ) : isLoaded || searchQuery !== "" ? (
            <h3> "{searchQuery}" not found</h3>
          ) : (
            ""
          )}
        </PuzzleWrapper>
      )}
    </Container>
  );
}

const mapStateToProps = (state) => ({
  maba: state.puzzleReducer.maba,
  isLoaded: state.puzzleReducer.isLoaded,
  isLoading: state.puzzleReducer.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
  fetchMaba: () => dispatch(fetchMaba()),
  postMaba: (id, data) => dispatch(postMaba(id, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Puzzle);
