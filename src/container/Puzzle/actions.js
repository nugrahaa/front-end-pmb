import axios from "axios";
import {
  FETCH_MABA,
  FETCH_MABA_SUCCESS,
  FETCH_MABA_FAILED,
  POST_MABA,
  POST_MABA_FAILED,
  POST_MABA_SUCCESS,
} from "./constants";
// import DUMMY_MABA from "./dummy";

import { fetchPuzzlesApi, postPuzzlesApi } from "../../api";

export function fetchMaba() {
  return (dispatch) => {

    dispatch({ type: FETCH_MABA });
    axios
      .get(fetchPuzzlesApi)
      .then((response) => {
        dispatch({
          payload: response.data,
          type: FETCH_MABA_SUCCESS,
        });
      })
      .catch((error) => {
        dispatch({
          // payload: error,
          payload: error,
          type: FETCH_MABA_FAILED,
        });
      });
  };
}

export function postMaba(idPuzzle, data) {
  return (dispatch) => {
    dispatch({ type: POST_MABA });
    axios
      .patch(postPuzzlesApi(idPuzzle), {
        attachment_link: data.attachment,
        questions_and_answers: data.answers,
      })
      .then((response) => {
        dispatch(fetchMaba());
        dispatch({
          payload: response.data,
          type: POST_MABA_SUCCESS,
        });
        alert("Submit Successful", "success");
      })
      .catch((error) => {
        dispatch({
          payload: error,
          type: POST_MABA_FAILED,
        });
        alert("Submit Failed", "error");
      });
  };
}
