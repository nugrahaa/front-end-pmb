// import { fromJS } from "immutable";
import {
  FETCH_MABA,
  FETCH_MABA_SUCCESS,
  FETCH_MABA_FAILED,
  POST_MABA,
  POST_MABA_FAILED,
  POST_MABA_SUCCESS,
} from "./constants";

const initialState = {
  maba: [],
  error: null,
  isLoaded: false,
  isLoading: false,
};

function puzzleReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_MABA:
      return {
        ...state,
        isLoading: true,
      };

    case FETCH_MABA_SUCCESS:
      return {
        ...state,
        maba: action.payload,
        isLoading: false,
        isLoaded: true,
      };

    case FETCH_MABA_FAILED:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
        isLoaded: false,
      };
    case POST_MABA:
      return {
        ...state,
        isLoading: true,
      };

    case POST_MABA_SUCCESS:
      return {
        ...state,
        data: action.payload,
        isLoading: false,
        isLoaded: true,
      };

    case POST_MABA_FAILED:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
        isLoaded: false,
      };
    default:
      return state;
  }
}



export default puzzleReducer;
