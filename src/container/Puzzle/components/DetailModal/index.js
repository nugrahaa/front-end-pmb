import React, { useEffect, useState, Fragment } from "react";
import Modal from "../../../../components/Modal";
import Input from "../../../../components/Input";
import Button from "../../../../components/Button";
import TimerCountdown from "../../../../components/TimerCountdown"
import {
  ModalContainer,
  Profile,
  Detail,
  DetailWrapper,
  Wrapper,
  Interests,
  Line,
  InterestItemWrapper,
  ItemContainer,
  FormContainer,
  InputWrapper,
  ActionContainer,
  PhotoContainer,
  FilledValue,
} from "./style";
import Email from "./assets/icon/email.svg";
import Education from "./assets/icon/education.svg";
import LineMediaSocial from "./assets/icon/line.svg";


const DetailUser = ({ icon, text }) => {
  return (
    <Wrapper>
      <img src={icon} alt="icon"></img>
      <h5>{text}</h5>
    </Wrapper>
  );
};
const InterestItem = ({ text }) => {
  return (
    <InterestItemWrapper>
      <h6>{text}</h6>
    </InterestItemWrapper>
  );
};

function DetailModal({
  data,
  postMaba = () => {},
  fetchMaba = () => {},
  open,
  handleClose,
  filled = false,
}) {

  const [answers, setAnswers] = useState([{}]);
  const [attachmentVal, setAttachmentVal] = useState("");

  const setValue = ()=>{
    setAnswers(data.questions_and_answers);
    setAttachmentVal(data.attachment_link);
  }

  const resetValue = ()=>{
    setAttachmentVal("");
    setAnswers([{}]);
  }

  useEffect(()=>{
    setAnswers(data.questions_and_answers);
    setAttachmentVal(data.attachment_link);
  },[open, data.attachment_link, data.questions_and_answers])

  const handleSubmit = async(event) => {
    event.preventDefault();
    let formData = { attachment: attachmentVal, answers: answers };
    await postMaba(data.id, formData);
    handleClose();
  };

  const handleCancel = ()=>{
    handleClose();
    if(data.status!=='EMPTY')
      setValue();
    else
      resetValue();
  }
  const handleChangeInput = (e, idx, id) => {
    const val = e.target.value;
    const prevData = [...answers];
    prevData[idx] = { id: id, answer: val };
    setAnswers(prevData);
  };

  const checkField = ()=>{
    let checked = false;
    let checkedData = answers.filter((el)=>{
      return el.answer === "" || el.answer ===undefined
    })
    let lengthChecked = checkedData.length >0
    if(attachmentVal === "") checked = true;

    if(answers[0].id===undefined || answers.length !== data.questions_and_answers.length ||lengthChecked ) checked=true;
    return checked;
  }

  return (
    <Modal open={open} handleClose={handleClose} maxWidth="sm" fullWidth  preventClose={true}>
      <ModalContainer>
        {data.status === "EMPTY" || data.status === "REJECTED" ?
          <TimerCountdown start={open} handleClose={handleClose} timerDuration={10}/>
          :
        null}
        <Profile>
          <img
            className="avatar"
            alt="avatar"
            src={data.target_maba.profile.photo}
          ></img>
          <Detail>
            <h4>{data.target_maba.profile.name}</h4>
            <DetailWrapper>
              <DetailUser
                icon={Education}
                text={data.target_maba.profile.major}
              />
              <DetailUser
                icon={Email}
                text={data.target_maba.profile.email} />
              <DetailUser
                icon={LineMediaSocial}
                text={data.target_maba.profile.line_id}
              />
            </DetailWrapper>
          </Detail>
        </Profile>
        {data.target_maba.profile.interests.length !== 0 ? (
          <Interests>
            <h5>Interests</h5>
            <Line />
            <ItemContainer>
              {data.target_maba.profile.interests.map((el, idx) => {
                return <InterestItem text={el.interest_name} key={idx} />;
              })}
            </ItemContainer>
          </Interests>
        ) : (
          <Interests>
            <h5>Interests</h5>
            <Line />
            <h6 style={{marginLeft:'2rem'}}>-</h6>
          </Interests>
        )}
       
          <PhotoContainer>
            <h5>Submit Photo</h5>
            {data.status !== "EMPTY" && data.status !== "REJECTED" ? (
              <h6>{data.attachment_link}</h6>
            ) : (
              <Input
                type="text"
                name="photo"
                value={attachmentVal}
                onChange={(e)=>setAttachmentVal(e.target.value)}
                placeholder='Awali dengan "https://" atau "http://"'
              ></Input>
            )}
          </PhotoContainer>

          <FormContainer>
            {data.questions_and_answers.length !== 0 ? (
              <Fragment>
                {data.questions_and_answers.map((el, idx) => {
                  return (
                    <InputWrapper key={idx}>
                      <h6>{el.question.question}</h6>
                      {data.status !== "EMPTY" && data.status !== "REJECTED" ? (
                        <FilledValue>
                          <h6>{el.answer}</h6>
                        </FilledValue>
                      ) : (
                        <Input
                          type="text"
                          name={`question${el.question.id}`}
                          value={answers[idx]?.answer ?? ""}
                          onChange={(e) =>
                            handleChangeInput(e, idx, el.id)
                          }
                        ></Input>
                      )}
                    </InputWrapper>
                  );
                })}
                <ActionContainer>
                  {(data.status === "EMPTY" || data.status=== "REJECTED") ? 
                    (<Button textColor="red" borderColor="red" onClick={handleCancel}>
                      Cancel
                     </Button>
                    ) : (
                      <Button textColor="#1b274b" borderColor="#1b274b" onClick={handleCancel}>
                        Close
                      </Button>
                    )}
                  {(data.status === "EMPTY" || data.status=== "REJECTED") && (
                    <Button disabled={checkField()} textColor="#1b274b" onClick={handleSubmit}>
                      Submit
                    </Button>
                  )}
                </ActionContainer>
              </Fragment>
            ) : (
              <h3>Empty</h3>
            )}
          </FormContainer>
      </ModalContainer>
    </Modal>
  );
}
export default DetailModal;
