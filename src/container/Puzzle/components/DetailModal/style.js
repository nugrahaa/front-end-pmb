import styled from "styled-components";

export const ModalContainer = styled.div`
  color: #1b274b;
  font-family: "Metropolis";
`;

export const Profile = styled.div`
  display: flex;
  flex-direction: row;
  img.avatar {
    width: 120px;
    height: 120px;
    border-radius: 50%;
  }
  @media only screen and (max-width: 767px) {
    margin-top: 1em;
  }
`;
export const Detail = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 1.75em;
  justify-content: center;
  h4 {
    margin: 0;
  }
`;
export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-contnet: center;
  align-items: center;
  img {
    margin-right: 0.5em;
  }
  
  h5 {
    font-weight: normal;
  }
  margin-right: 2em;
`;
export const DetailWrapper = styled.div`
  display: flex;
  flex-direction: row;
  @media only screen and (max-width: 767px) {
    flex-direction: column;
    h5 {
      margin: 1em 0em 0em 0em;
    }
    img {
      margin: 1em 0.5em 0em 0em;
    }
  }
  @media only screen and (max-width: 600px) {
    h5 {
      font-size: 10px;
    }
  }
`;

export const Interests = styled.div`
  margin-top: 1.75em;
  display: flex;
  flex-direction: column;
  h5 {
    font-weight: normal;
    margin: 0;
  }
`;
export const Line = styled.div`
  width: 100%;
  height: 0.1px;
  background-color: #1b274b;
`;
export const InterestItemWrapper = styled.div`
  margin-right: 0.75em;
  margin-bottom: 0.75em;
  background: #e7f6fd;
  opacity: 0.8;
  width: fit-content;
  height: fit-content;
  border-radius: 20px;
  h6 {
    margin: 0.5em 1em 0.5em 1em;
    color: black;
    font-weight: normal;
  }
`;
export const ItemContainer = styled.div`
  margin-top: 0.5em;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;
export const FormContainer = styled.div``;
export const InputWrapper = styled.div`
  width: 100%;
  h6 {
    margin: 0em 0em 0.25em 0.25em;
  }
  margin: 1em 0;
`;
export const ActionContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  button:nth-child(2){
    margin-left:1rem;
  }
`;
export const PhotoContainer = styled.div`
  width: 100%;
  h5 {
    font-weight: normal;
    margin: 0 0 0.25em 0.25em;
  }
  h6 {
    font-weight: normal;
    margin: 0.25em 0em 0 0.25em;
  }
`;
export const FilledValue = styled.div`
  h6 {
    margin: 0em 0em 0.25em 1em;
    font-weight: normal;
  }
`;
