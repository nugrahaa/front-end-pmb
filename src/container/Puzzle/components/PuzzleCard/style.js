import styled from "styled-components";

export const PuzzleCardWrap = styled.div`
  width: calc(20% - 10px);
  padding-top: 20%;
  margin: 5px;
  position: relative;

  @media (max-width: 768px) {
    & {
      width: calc(25% - 5px);
      padding-top: 25%;
      margin: 2.5px;
    }
  }
`;

export const PuzzleCardContainer = styled.div`
  width: 100%;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  font-family: Metropolis;

  background-color: ${props =>
    props.puzzle_status !== "VERIFIED" ?
    'rgba(196, 196, 196, 0.7)'
      : 'white'
  };

  mix-blend-mode: normal;
  box-shadow: 0px 2.025px 2.025px rgba(0, 0, 0, 0.25);
  border-radius: 5.0625px;
  text-align: center;

  display: flex;
  flex-direction: column;
  overflow-wrap: break-word;

  &:hover {
    cursor: pointer;
  }

  .text-wrapper {
    position: relative;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin-top: calc(52.5% - 20px);
    overflow-wrap: break-word;
    padding: 10px;
  }

  @media (max-width: 768px) {
    .text-wrapper {
      font-size: 18px;
    }
  }

  @media (max-width: 500px) {
    .text-wrapper {
      font-size: 10px;
    }
  }


  h6 {
    font-style: normal;
    font-weight: bold;
    font-size: 1em;
    line-height: fit-content;
    margin: 0px 0px 7px;
    display: block;

    ${props =>
        props.puzzle_status === "EMPTY" ||
        props.puzzle_status === "VERIFIED" ?
        "" :
        "white-space: nowrap; overflow: hidden; text-overflow: ellipsis;"
    }

    color: #4a69ca;
  }

  span {
    font-style: italic;
    font-weight: 500;
    font-size: 0.8em;
    margin: 0px 0px 5px;
    display: block;

    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;

    color: #1b274b;
  }

  span.rejected {
    color: red;
    font-weight: 600;
  }

  span.rejected.muted {
    filter: grayscale(0.7);
  }
`;
