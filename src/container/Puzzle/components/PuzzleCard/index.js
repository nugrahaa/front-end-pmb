import React, { useState } from "react";

import { PuzzleCardContainer, PuzzleCardWrap } from "./style";
import DetailModal from "../DetailModal";

const PuzzleCard = ({
  data,
  postMaba = () => {},
  fetchMaba = () => {},
  ...props }) => {
  const [open, setOpen] = useState(false);
  return (
    <PuzzleCardWrap>
      <PuzzleCardContainer
        onClick={() => setOpen(true)}
        puzzle_status={data.status}
      >
        <div className="text-wrapper">
          <h6>{data.target_maba.profile.name}</h6>

          {data.status === "VERIFYING" ? (
            <span>Waiting for verification</span>
          ) : data.status === "REJECTED" ? (
            <span className="rejected">Rejected</span>
          ) : data.status === "RESUBMITTING" ? (
            <div>
              <span className="rejected muted">Rejected</span>
              <span>Waiting for verification</span>
            </div>
          ) : (
            ""
          )}
        </div>
      </PuzzleCardContainer>

      <DetailModal
        open={open}
        handleClose={() => setOpen(false)}
        filled={false}
        data={data}
        fetchMaba={fetchMaba}
        postMaba={postMaba}
      />
    </PuzzleCardWrap>
  );
};

export default PuzzleCard;
