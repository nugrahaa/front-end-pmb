/*
  status/kondisi puzzle card
  1. empty => EMPTY
  2. waiting for verification => VERIFYING
  4. rejected => REJECTED
  5. re-submit => RESUBMITTING
  6. verified => VERIFIED
*/

const DUMMY_MABA = [
  {
    id: 1,
    nama: "Fadhil Ilham R.",
    line: "fadhil",
    npm: "1906398370",
    education: "SI 2020",
    email: "tes@gmail.com",
    puzzle_status: "VERIFIED",
    avatar:
      "https://github.com/identicons/jonathanfilbert.png",
    interests: [
      "Cyber Security",
      "Web Development",
      "JKT48",
      "Photoshop",
      "Anime",
    ],
    question: [
      {
        id: 1,
        question: "Apakah pilihan jurusan adalah pilihan anda?",
        value: "tidak",
      },
      {
        id: 2,
        question: "Kenapa anda memilih jurusan ini?",
        value: "tidak",
      },
      {
        id: 3,
        question: "Apakah anda menyesal mendaftar jurusan ini?",
        value: "tidak",
      },
      {
        id: 4,
        question: "Apakah menurut anda bisa bahagia di jurusan ini?",
        value: "tidak",
      },
      {
        id: 5,
        question: "Apakah anda pintar?",
        value: "tidak",
      },
    ],
  },
  {
    id: 2,
    nama: "Fahdii Ajmalal Fikrie",
    line: "fahdii",
    npm: "1906398370",
    education: "SI 2020",
    email: "tes@gmail.com",
    puzzle_status: "VERIFYING",
    avatar:
      "https://github.com/identicons/jonathanfilbert.png",
    interests: ["main pb", "pocong"],
    question: [
      {
        id: 1,
        question: "Apakah pilihan jurusan adalah pilihan anda?",
        value: "tidak",
      },
      {
        id: 2,
        question: "Kenapa anda memilih jurusan ini?",
        value: "tidak",
      },
      {
        id: 3,
        question: "Apakah anda menyesal mendaftar jurusan ini?",
        value: "tidak",
      },
      {
        id: 4,
        question: "Apakah menurut anda bisa bahagia di jurusan ini?",
        value: "tidak",
      },
      {
        id: 5,
        question: "Apakah anda pintar?",
        value: "tidak",
      },
    ],
  },
  {
    id: 3,
    nama: "Pak aji siap",
    npm: "1906398370",
    line: "aji",
    education: "SI 2020",
    email: "tes@gmail.com",
    puzzle_status: "RESUBMITTING",
    avatar:
      "https://github.com/identicons/jonathanfilbert.png",
    interests: ["main motor", "pocong"],
    question: [
      {
        id: 1,
        question: "Apakah pilihan jurusan adalah pilihan anda?",
        value: "tidak",
      },
      {
        id: 2,
        question: "Kenapa anda memilih jurusan ini?",
        value: "tidak",
      },
      {
        id: 3,
        question: "Apakah anda menyesal mendaftar jurusan ini?",
        value: "tidak",
      },
      {
        id: 4,
        question: "Apakah menurut anda bisa bahagia di jurusan ini?",
        value: "tidak",
      },
      {
        id: 5,
        question: "Apakah anda pintar?",
        value: "tidak",
      },
    ],
  },
  {
    id: 4,
    nama: "Zidan siap",
    npm: "1906398370",
    line: "zidan",
    education: "SI 2020",
    email: "tes@gmail.com",
    puzzle_status: "REJECTED",
    avatar:
      "https://github.com/identicons/jonathanfilbert.png",
    interests: ["wibu", "pocong"],
    question: [
      {
        id: 1,
        question: "Apakah pilihan jurusan adalah pilihan anda?",
        value: "tidak",
      },
      {
        id: 2,
        question: "Kenapa anda memilih jurusan ini?",
        value: "tidak",
      },
      {
        id: 3,
        question: "Apakah anda menyesal mendaftar jurusan ini?",
        value: "tidak",
      },
      {
        id: 4,
        question: "Apakah menurut anda bisa bahagia di jurusan ini?",
        value: "tidak",
      },
      {
        id: 5,
        question: "Apakah anda pintar?",
        value: "tidak",
      },
    ],
  },
  {
    id: 5,
    nama: "Lorong waktu",
    npm: "1906398370",
    line: "lorong",
    education: "SI 2020",
    email: "tes@gmail.com",
    puzzle_status: "VERIFIED",
    avatar:
      "https://github.com/identicons/jonathanfilbert.png",
    interests: ["jkt48", "pocong"],
    question: [
      {
        id: 1,
        question: "Apakah pilihan jurusan adalah pilihan anda?",
        value: "tidak",
      },
      {
        id: 2,
        question: "Kenapa anda memilih jurusan ini?",
        value: "tidak",
      },
      {
        id: 3,
        question: "Apakah anda menyesal mendaftar jurusan ini?",
        value: "tidak",
      },
      {
        id: 4,
        question: "Apakah menurut anda bisa bahagia di jurusan ini?",
        value: "tidak",
      },
      {
        id: 5,
        question: "Apakah anda pintar?",
        value: "tidak",
      },
    ],
  },
  {
    id: 6,
    nama: "bingung namain apaam",
    npm: "1906398370",
    line: "nikah",
    education: "SI 2020",
    email: "tes@gmail.com",
    puzzle_status: "EMPTY",
    avatar:
      "https://github.com/identicons/jonathanfilbert.png",
    interests: ["nikah", "pocong"],
    question: [
      {
        id: 1,
        question: "Apakah pilihan jurusan adalah pilihan anda?",
        value: "tidak",
      },
      {
        id: 2,
        question: "Kenapa anda memilih jurusan ini?",
        value: "tidak",
      },
      {
        id: 3,
        question: "Apakah anda menyesal mendaftar jurusan ini?",
        value: "tidak",
      },
      {
        id: 4,
        question: "Apakah menurut anda bisa bahagia di jurusan ini?",
        value: "tidak",
      },
      {
        id: 5,
        question: "Apakah anda pintar?",
        value: "tidak",
      },
    ],
  },
  {
    id: 7,
    nama: "bruh ngantuk",
    line: "burung",
    npm: "1906398370",
    education: "SI 2020",
    email: "tes@gmail.com",
    puzzle_status: "VERIFIED",
    avatar:
      "https://github.com/identicons/jonathanfilbert.png",
    interests: ["eh beneran aja bro", "pocong"],
    question: [
      {
        id: 1,
        question: "Apakah pilihan jurusan adalah pilihan anda?",
        value: "tidak",
      },
      {
        id: 2,
        question: "Kenapa anda memilih jurusan ini?",
        value: "tidak",
      },
      {
        id: 3,
        question: "Apakah anda menyesal mendaftar jurusan ini?",
        value: "tidak",
      },
      {
        id: 4,
        question: "Apakah menurut anda bisa bahagia di jurusan ini?",
        value: "tidak",
      },
      {
        id: 5,
        question: "Apakah anda pintar?",
        value: "tidak",
      },
    ],
  },
  {
    id: 8,
    nama: "yekan kacosi",
    line: "kaco",
    npm: "1906398370",
    education: "SI 2020",
    email: "tes@gmail.com",
    puzzle_status: "EMPTY",
    avatar:
      "https://github.com/identicons/jonathanfilbert.png",
    interests: ["sakit lu ya", "pocong"],
    question: [
      {
        id: 1,
        question: "Apakah pilihan jurusan adalah pilihan anda?",
        value: "tidak",
      },
      {
        id: 2,
        question: "Kenapa anda memilih jurusan ini?",
        value: "tidak",
      },
      {
        id: 3,
        question: "Apakah anda menyesal mendaftar jurusan ini?",
        value: "tidak",
      },
      {
        id: 4,
        question: "Apakah menurut anda bisa bahagia di jurusan ini?",
        value: "tidak",
      },
      {
        id: 5,
        question: "Apakah anda pintar?",
        value: "tidak",
      },
    ],
  },
  {
    id: 9,
    nama: "wibu",
    line: "wibu",
    npm: "1906398370",
    education: "SI 2020",
    email: "tes@gmail.com",
    puzzle_status: "VERIFIED",
    avatar:
      "https://github.com/identicons/jonathanfilbert.png",
    interests: ["sakit lu ya"],
    question: [
      {
        id: 1,
        question: "Apakah pilihan jurusan adalah pilihan anda?",
        value: "tidak",
      },
      {
        id: 2,
        question: "Kenapa anda memilih jurusan ini?",
        value: "tidak",
      },
      {
        id: 3,
        question: "Apakah anda menyesal mendaftar jurusan ini?",
        value: "tidak",
      },
      {
        id: 4,
        question: "Apakah menurut anda bisa bahagia di jurusan ini?",
        value: "tidak",
      },
      {
        id: 5,
        question: "Apakah anda pintar?",
        value: "tidak",
      },
    ],
  },
];

export default DUMMY_MABA;
