import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 100%;
  display: block;
  margin-left: auto;
  margin-right: auto;

  h2 {
    color: #1B274B;
    font-weight: normal;
  }
`;

export const PuzzleWrapper = styled.div`
  margin-top: 30px;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: start;
  width: 100%;
  height: fit-content;
  padding: 0px;

  @media (max-width: 768px) {
    & {
      margin-top: 15px;
    }
  }
`;
