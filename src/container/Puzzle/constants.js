export const FETCH_MABA =
  "src/container/Puzzle/FETCH_MABA";
export const FETCH_MABA_SUCCESS =
  "src/container/Puzzle/FETCH_MABA_SUCCESS";
export const FETCH_MABA_FAILED =
  "src/container/Puzzle/FETCH_MABA_FAILED";

  export const POST_MABA =
  "src/container/Puzzle/POST_MABA";
export const POST_MABA_SUCCESS =
  "src/container/Puzzle/POST_MABA_SUCCESS";
export const POST_MABA_FAILED =
  "src/container/Puzzle/POST_MABA_FAILED";