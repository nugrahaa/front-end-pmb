import styled from "styled-components";
import MetropolisLight from "../../fonts/Metropolis-Light.otf";
import MetropolisRegular from "../../fonts/Metropolis-Regular.otf";
import MetropolisRegularItalic from "../../fonts/Metropolis-RegularItalic.otf";
import MetropolisBold from "../../fonts/Metropolis-Bold.otf";
import MetropolisBoldItalic from "../../fonts/Metropolis-BoldItalic.otf";

export const AppContainer = styled.div`
  @font-face {
    font-family: "Metropolis";
    src: url(${MetropolisLight});
    font-weight: normal;
  }

  @font-face {
    font-family: "Metropolis";
    src: url(${MetropolisRegular});
    font-weight: 600;
  }

  @font-face {
    font-family: "Metropolis";
    src: url(${MetropolisBold});
    font-weight: bold;
  }

  @font-face {
    font-family: "Metropolis";
    src: url(${MetropolisBoldItalic});
    font-weight: bold;
    font-style: italic;
  }

  @font-face {
    font-family: "Metropolis";
    src: url(${MetropolisRegularItalic});
    font-weight: normal;
    font-style: italic;
  }

  display: flex;
  flex-direction: column;
  position: relative;
  box-sizing: border-box;
  width: 100%;
  max-width: 100%;
  overflow: hidden;
  z-index: 1;
  min-height: 100vh;
  background: url("/static/images/background.png");
  background-size: contain;
  background-repeat: repeat;
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  b,
  strong {
    font-weight: 500;
    font-family: "Metropolis";
  }

  font-family: "Metropolis";
`;
