import React, { useState, Fragment, useEffect } from "react";
import { Switch, Route, Redirect, withRouter } from "react-router-dom";
// import { useSelector } from "react-redux";
import { connect } from "react-redux";
import { ConnectedRouter, push } from "connected-react-router";
import Alert from "../../components/Alert";
import auth from "../../auth";
// import logo from './../../logo.svg';
import { AppContainer } from "./style";
import { history } from "../../store";
import { routes } from "./routes";
import { isMaba } from "../../selectors/user";
import { setUser } from "../../globalActions"

import AppLayout from "../../components/Layout/AppLayout";

function useAlert() {
  function Provider({ children }) {
    const [alerts, setAlerts] = useState([]);

    function handleCloseAlert(index) {
      return () => {
        const copyAlerts = [...alerts];
        copyAlerts[index] = null;
        setAlerts(copyAlerts);
      };
    }

    useEffect(() => {
      window.alert = (message, type) => {
        setAlerts((prevAlerts) => [{ type: type, message }, ...prevAlerts]);
      };
    }, []);

    return (
      <Fragment>
        {alerts.map((alert, i) => {
          const message = alert?.message;
          const type = alert?.type;

          return (
            <Alert
              type={type}
              left="14em"
              key={i}
              show={!!alert}
              handleClose={handleCloseAlert(i)}
              message={message}
            ></Alert>
          );
        })}
        {children}
      </Fragment>
    );
  }

  return { Provider };
}

const ProtectedRoute = ({
  component: Component,
  path,
  isMaba,
  isUserMaba,
  profile,
  kelompok,
  ...rest
}) => (
    <Route
      {...rest}
      render={(props) => {
        if (auth.loggedIn()) {
          if(isUserMaba==='fetching'){
            return null
          }
          // if (
          //   path !== "/edit-profile" &&
          //   !profile === '' &&
          //   (!profile.birth_place ||
          //     !profile.birth_date ||
          //     !profile.high_school ||
          //     !profile.line_id ||
          //     (isUserMaba && isEmpty(kelompok)))
          // ) {
          //   return <Redirect to="/edit-profile" />;
          // }
          if (isMaba && isUserMaba === false) {
            
            return <Redirect to={{ pathname: "/oops" }} />;
          }
          
          return <Component {...props} />;
        }
        return <Redirect to={{ pathname: "/login" }} />;
      }}
    />
  );

function App(props) {
  const { Provider } = useAlert();
  const pages = routes.map((route, i) => {
    if (route.protected) {
      return (
        <ProtectedRoute
          component={route.component}
          exact={route.exact}
          path={route.path}
          key={i}
          isMaba={route.isMaba}
          isUserMaba={props.isUserMaba}
          // profile={props.profile}
          // kelompok={props.profile.kelompok}
        />
      );
    }
    return (
      <Route
        component={route.component}
        exact={route.exact}
        path={route.path}
        key={i}
      />
    );
  });


  // useEffect(() => {
  //   props.fetchProfile()
  // }, [])

  return (
    <Provider>
      <ConnectedRouter history={history}>
        <AppContainer>
          <AppLayout> 
            <Switch>
              {pages}
              <Redirect to="/oops" />
            </Switch>     
          </AppLayout>
        </AppContainer>
      </ConnectedRouter>
    </Provider>
  );
}

function mapStateToProps(state) {
  return {
    // profile: state.profileReducer.profile,
    // profile: getUser(state),
    isUserMaba: isMaba(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    push: (url) => dispatch(push(url)),
    fetchProfile: () => dispatch(setUser())
  };
}
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)
);


// export default App;
