/* eslint-disable object-property-newline */
import NotFoundPage from "../../components/NotFoundPage";
import LandingPage from "../LandingPage";
import Puzzle from "../Puzzle";
import AboutPage from "../AboutPage";
import HomePage from "../HomePage";
import FriendPage from "../FriendPage";
import GalleryPage from "../GalleryPage";
import ProfilePage from "../ProfilePage";
import EditProfilePage from "../EditProfilePage";
import EditInterestPage from "../EditInterestPage";
import FormFriendPage from "../FormFriendPage";
import FormRejectFriendPage from "../FormRejectFriendPage";
import EventPage from "../EventPage";
import EventDetailPage from "../EventDetailPage";
import AnnouncementPage from "../AnnouncementPage";
import SearchMahasiswaPage from "../SearchMahasiswaPage";
import TaskListPage from "../TaskListPage";
import TaskDetailPage from "../TaskDetailPage";

export const routes = [
  {
    component: LandingPage,
    exact: true,
    path: "/login",
  },
  {
    component: HomePage,
    exact: true,
    path: "/",
    protected: true,
  },
  {
    component: EventPage,
    exact: true,
    path: "/event",
    protected: true,
  },
  {
    component: EventDetailPage,
    exact: true,
    path: "/event/:id",
    protected: true,
  },
  {
    component: AnnouncementPage,
    exact: true,
    path: "/announcement",
    protected: true,
  },
  {
    component: FriendPage,
    exact: true,
    path: "/friends",
    protected: true,
  },
  {
    component: GalleryPage,
    exact: true,
    path: "/gallery",
    protected: true,
  },
  {
    component: FormFriendPage,
    exact: true,
    path: "/friends/add/:id",
    protected: true,
  },
  {
    component: FormRejectFriendPage,
    exact: true,
    path: "/friends/reject/:id",
    protected: true,
  },
  {
    component: Puzzle,
    exact: true,
    path: "/puzzle",
    protected: true,
    isMaba: true,
  },
  {
    component: AboutPage,
    exact: true,
    path: "/about",
    protected: true,
  },
  {
    component: ProfilePage,
    exact: true,
    path: "/profile",
    protected: true,
  },
  {
    component: EditProfilePage,
    exact: true,
    path: "/profile/edit-profile",
    protected: true,
  },
  {
    component: EditInterestPage,
    exact: true,
    path: "/profile/edit-interest",
    protected: true,
  },
  {
    component: SearchMahasiswaPage,
    exact: true,
    path: "/search",
    protected: true,
  },
  {
    component: TaskDetailPage,
    protected: false,
    path: "/task/:id",
    exact: true,
    isMaba: true,
  },
  { component: NotFoundPage, protected: true, path: "/oops" },
  {
    component: TaskListPage,
    exact: true,
    path: "/task",
    protected: true,
    isMaba: true,
  },
  // { component: TaskListPage, exact: true, path: "/task", protected: true, isMaba: true },
];
