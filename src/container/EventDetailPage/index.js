import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import renderHTML from "react-render-html";
import { connect } from "react-redux";
import { push } from "connected-react-router";

import Card from "../../components/Card";
import iconDate from "../../assets/icons/calendar.svg";
import iconLocation from "../../assets/icons/location.svg";
import iconPaperClip from "../../assets/icons/attachment.svg";
import { fetchEventDetail } from "./actions";
import { StyledEventDetailPage } from "./style";
import Button from "../../components/Button";

const EventDetailPage = ({ location, fetchEventDetailData, event, push }) => {
  const [eventDetailData, setEventDetailData] = useState({});

  const getId = () => location.pathname.split("/")[2];
  const id = getId();

  const now = new Date();
  const eventDate = new Date(event.date);

  useEffect(() => {
    fetchEventDetailData(id);
  }, [fetchEventDetailData, id]);

  useEffect(() => {
    setEventDetailData({ ...event });
  }, [event]);

  return (
    <StyledEventDetailPage
      image={eventDetailData.cover_image_link}
      organizer={eventDetailData.organizer_image_link}
    >
      <div className="breadcrumb-container">
        {eventDate > now ? (
          <div onClick={() => push("/event")}>
            <h2 className="breadcrumb-text link">Upcoming Event</h2>
          </div>
        ) : (
          <div onClick={() => push("/event")}>
            <h2 className="breadcrumb-text link">Past Event</h2>
          </div>
        )}
        <h2 className="breadcrumb-text">{`> ${eventDetailData.title}`}</h2>
      </div>
      <Card
        containerStyle={{
          padding: 0,
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
          marginBottom: "1rem",
        }}
      >
        {eventDetailData.cover_image_link && <div className="cover-image" />}
        {eventDetailData.organizer_image_link && (
          <div className="organizer-image-container">
            <div className="orgnizer-image-position">
              <div className="organizer-image" />
            </div>
          </div>
        )}
        <div className="event-detail-container">
          <div className="event-detail-margin">
            <h3 className="title-container">{eventDetailData.title}</h3>
            <div className="detail-container">
              <div className="dateTime">
                <div className="row margin">
                  <img className="date-icon" src={iconDate} alt={"date"} />
                  <p>{eventDetailData.date}</p>
                </div>
                <div className="row">
                  <img className="location-icon" src={iconLocation} alt={"location"} />
                  <p>{eventDetailData.place}</p>
                </div>
              </div>
              {event.description && (
                <div className="description">{renderHTML(event.description)}</div>
              )}
              {event.attachment_link && (
                <div className="attachment">
                  <div className="row">
                    <img className="paper-clip-icon" src={iconPaperClip} alt={"attachment"} />
                    <a href={event.attachment_link} target="_blank" rel="noopener noreferrer">
                      <p>Event Attachment</p>
                    </a>
                  </div>
                </div>
              )}
              <div className="backButton">
                <Button onClick={() => push("/event")}>Back</Button>
              </div>
            </div>
          </div>
        </div>
      </Card>
    </StyledEventDetailPage>
  );
};

EventDetailPage.propTypes = {
  location: PropTypes.shape().isRequired,
  fetchEventDetailData: PropTypes.func.isRequired,
  event: PropTypes.shape().isRequired,
};

const mapStateToProps = (state) => ({
  event: state.eventDetailReducer.event,
  isLoaded: state.eventDetailReducer.isLoaded,
  isLoading: state.eventDetailReducer.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
  fetchEventDetailData: (id) => dispatch(fetchEventDetail(id)),
  push: (url) => dispatch(push(url)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EventDetailPage);
