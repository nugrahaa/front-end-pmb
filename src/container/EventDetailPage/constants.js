export const FETCH_EVENT_DETAIL = "src/EventDetailPage/FETCH_EVENT_DETAIL";
export const FETCH_EVENT_DETAIL_SUCCESS = "src/EventDetailPage/FETCH_EVENT_DETAIL_SUCCESS";
export const FETCH_EVENT_DETAIL_FAILED = "src/EventDetailPage/FETCH_EVENT_DETAIL_FAILED";
