import axios from "axios";

import {
  FETCH_EVENT_DETAIL,
  FETCH_EVENT_DETAIL_SUCCESS,
  FETCH_EVENT_DETAIL_FAILED,
} from "./constants";
import { fetchEventsDetailApi } from "../../api";

export const fetchEventDetail = (id) => {
  return (dispatch) => {
    dispatch({ type: FETCH_EVENT_DETAIL });
    axios
      .get(fetchEventsDetailApi(id))
      .then((response) => {
        dispatch({
          payload: response.data,
          type: FETCH_EVENT_DETAIL_SUCCESS,
        });
      })
      .catch((error) => {
        dispatch({
          payload: error,
          type: FETCH_EVENT_DETAIL_FAILED,
        });
      });
  };
};
