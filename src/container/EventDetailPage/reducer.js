import {
  FETCH_EVENT_DETAIL,
  FETCH_EVENT_DETAIL_SUCCESS,
  FETCH_EVENT_DETAIL_FAILED,
} from "./constants";

const initialState = {
  event: {},
  error: null,
  isLoaded: false,
  isLoading: false,
};

const eventDetailPageReducer = (state, action) => {
  if (typeof state === "undefined") {
    return initialState;
  }

  switch (action.type) {
    case FETCH_EVENT_DETAIL:
      return {
        ...state,
        isLoading: true,
      };

    case FETCH_EVENT_DETAIL_SUCCESS:
      return {
        ...state,
        event: action.payload,
        isLoading: false,
        isLoaded: true,
      };

    case FETCH_EVENT_DETAIL_FAILED:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
        isLoaded: false,
      };

    default:
      return state;
  }
};

export default eventDetailPageReducer;
