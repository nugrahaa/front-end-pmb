import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";

import Loading from "../../components/Loading";
import EmptyCard from "../../components/EmptyCard";
import AnnouncementCard from "./AnnouncementCard";
import { fetchAnnouncement } from "./actions";
import { StyledAnnouncementPage } from "./style";

const AnnouncementPage = ({ fetchAnnouncementData, announcements, isLoading }) => {
  const [announcementData, setAnnouncementData] = useState([]);

  useEffect(() => {
    fetchAnnouncementData();
  }, [fetchAnnouncementData]);

  useEffect(() => {
    setAnnouncementData([...announcements]);
  }, [announcements]);

  return (
    <StyledAnnouncementPage>
      <Helmet>
        <title>PMB 2020 - Announcement</title>
      </Helmet>
      <h2>Announcement</h2>
      {!isLoading ? (
        announcementData.length !== 0 ? (
          announcementData.map((item) => (
            <AnnouncementCard
              key={item.id}
              title={item.title}
              description={item.description}
              attachment_link={item.attachment_link}
            />
          ))
        ) : (
          <EmptyCard text="There's no announcement" />
        )
      ) : (
        <Loading />
      )}
    </StyledAnnouncementPage>
  );
};

AnnouncementPage.propTypes = {
  fetchAnnouncementData: PropTypes.func.isRequired,
  announcements: PropTypes.array.isRequired,
  isLoading: PropTypes.bool,
};

const mapStateToProps = (state) => ({
  announcements: state.announcementReducer.announcements,
  isLoaded: state.announcementReducer.isLoaded,
  isLoading: state.announcementReducer.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
  fetchAnnouncementData: () => dispatch(fetchAnnouncement()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AnnouncementPage);
