import {
  FETCH_ANNOUNCEMENT,
  FETCH_ANNOUNCEMENT_SUCCESS,
  FETCH_ANNOUNCEMENT_FAILED,
} from "./constants";

const initialState = {
  announcements: [],
  error: null,
  isLoaded: false,
  isLoading: false,
};

const announcementPageReducer = (state, action) => {
  if (typeof state === "undefined") {
    return initialState;
  }

  switch (action.type) {
    case FETCH_ANNOUNCEMENT:
      return {
        ...state,
        isLoading: true,
      };

    case FETCH_ANNOUNCEMENT_SUCCESS:
      return {
        ...state,
        announcements: action.payload,
        isLoading: false,
        isLoaded: true,
      };

    case FETCH_ANNOUNCEMENT_FAILED:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
        isLoaded: false,
      };

    default:
      return state;
  }
};

export default announcementPageReducer;
