import axios from "axios";

import {
  FETCH_ANNOUNCEMENT,
  FETCH_ANNOUNCEMENT_SUCCESS,
  FETCH_ANNOUNCEMENT_FAILED,
} from "./constants";
import { fetchAnnouncementsApi } from "../../api";

export const fetchAnnouncement = () => {
  return (dispatch) => {
    dispatch({ type: FETCH_ANNOUNCEMENT });
    axios
      .get(fetchAnnouncementsApi)
      .then((response) => {
        dispatch({
          payload: response.data,
          type: FETCH_ANNOUNCEMENT_SUCCESS,
        });
      })
      .catch((error) => {
        dispatch({
          payload: error,
          type: FETCH_ANNOUNCEMENT_FAILED,
        });
      });
  };
};
