import styled from "styled-components";

export const StyledAnnouncementCard = styled.div`
  .title-container {
    font-weight: bold;
    color: #4a69ca;
    font-size: 1.25rem;
    margin-top: 0px;
  }

  .description-container p {
    margin-top: 0px;
    font-size: 1rem;
    color: black;
  }

  .attachment-container a {
    font-weight: bold;
    font-size: 1rem;
    text-decoration: none;
    color: #1b274b;
  }

  .attachment-container a:hover {
    text-decoration: underline;
  }

  .attachment-container .row {
    display: flex;
    flex-direction: row;
    height: 1rem;
    align-items: center;
  }

  .paperclip-icon {
    width: 1rem;
    margin-right: 0.5rem;
  }

  .link-container {
    color: #4a69ca;
    font-style: italic;
    text-decoration: none;
  }

  .link-container:hover {
    text-decoration: underline;
  }

  @media screen and (max-width: 73.75rem) {
    .title-container {
      font-size: 1rem;
    }

    .description-container p {
      font-size: 0.75rem;
    }

    .attachment-container a {
      font-size: 0.75rem;
    }
  }
`;
