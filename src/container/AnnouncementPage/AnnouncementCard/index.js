import React from "react";
import PropTypes from "prop-types";
import renderHTML from "react-render-html";

import iconPaperClip from "../../../assets/icons/attachment.svg";
import Card from "../../../components/Card";
import { StyledAnnouncementCard } from "./style";

const AnnouncementCard = ({ title, description, attachment_link }) => {
  return (
    <StyledAnnouncementCard>
      <Card
        containerStyle={{
          padding: 20,
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
          marginBottom: "1rem",
        }}
      >
        <h3 className="title-container">{title}</h3>
        <div className="description-container">
          {attachment_link ? (
            <div className="attachment-container">
              {renderHTML(description)}
              <div className="row">
                <img className="paperclip-icon" src={iconPaperClip} alt="attachment" />
                <a href={attachment_link} target="_blank" rel="noopener noreferrer">
                  {attachment_link}
                </a>
              </div>
            </div>
          ) : (
            renderHTML(description)
          )}
        </div>
      </Card>
    </StyledAnnouncementCard>
  );
};

AnnouncementCard.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  attachment_link: PropTypes.string,
};

AnnouncementCard.defaultProps = {
  attachment_link: "",
};

export default AnnouncementCard;
