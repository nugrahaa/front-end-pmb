import axios from "axios";

import {
  FETCH_TASKS_LIST,
  FETCH_TASKS_LIST_SUCCESS,
  FETCH_TASKS_LIST_FAILED,
} from "./constants";

import { getTaskListApi } from "../../api";

export function fetchTasksList() {
  return (dispatch) => {
    dispatch({ type: FETCH_TASKS_LIST });
    axios
      .get(getTaskListApi)
      .then((response) => {
        dispatch({
          payload: response.data,
          type: FETCH_TASKS_LIST_SUCCESS,
        });
      })
      .catch((error) => {
        dispatch({
          payload: error,
          type: FETCH_TASKS_LIST_FAILED,
        });
      });
  };
}
