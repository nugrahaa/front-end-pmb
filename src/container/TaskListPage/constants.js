export const FETCH_TASKS_LIST = "src/TaskListPage/FETCH_TASKS_LIST";

export const FETCH_TASKS_LIST_SUCCESS =
  "src/TaskListPage/FETCH_TASKS_LIST_SUCCESS";

export const FETCH_TASKS_LIST_FAILED =
  "src/TaskListPage/FETCH_TASKS_LIST_FAILED";
