import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

import Loading from "../../../components/Loading";
import EmptyCard from "../../../components/EmptyCard";
import TaskListCard from "../../../components/TaskListCard";
import prevButton from "../../../assets/img/prevButton.png";
import nextButton from "../../../assets/img/nextButton.png";
import styles from "../../../utils/styles";

const TaskCollection = (props) => {
  const [itemInPage, setItemInPage] = useState([]);
  const [pageProperty, setPageProperty] = useState({
    page: 0,
    itemPerPage: 2,
    rightBondage: 3,
    leftBondage: 1,
  });

  const [pageElement, setPageElement] = useState(null);
  const [pageSelected, setPageSelected] = useState(1);

  const handlePageSelected = (index) => {
    setPageSelected(index);
  };

  const getPageStyle = (index) => {
    return index === pageSelected
      ? {
          margin: "0 0.75rem",
          cursor: "default",
          color: "#8BA6B6",
        }
      : {
          margin: "0 0.75rem",
          cursor: "pointer",
        };
  };

  const updatePageSelection = (difference, maxPage = pageProperty.page) => {
    const rightBondage = pageProperty.rightBondage + difference;
    const leftBondage = pageProperty.leftBondage + difference;
    const pageElement = [];
    for (let index = leftBondage; index <= rightBondage; index++) {
      if (index <= maxPage) {
        pageElement.push(
          <span
            onClick={() => handlePageSelected(index)}
            key={index}
            style={getPageStyle(index)}
          >
            {index}
          </span>
        );
      } else {
        break;
      }
    }

    setPageProperty((prevState) => {
      return {
        ...prevState,
        rightBondage,
        leftBondage,
      };
    });

    setPageElement(pageElement);
  };

  const addRightPage = (maxLength) => {
    if (pageProperty.rightBondage < maxLength) {
      updatePageSelection(1);
      handlePageSelected(pageProperty.rightBondage + 1);
    }
  };

  const addLeftPage = () => {
    if (pageProperty.leftBondage > 1) {
      updatePageSelection(-1);
      handlePageSelected(pageProperty.leftBondage - 1);
    }
  };

  const getLeftArrowStyle = () => {
    return pageProperty.leftBondage <= 1
      ? { opacity: "50%", cursor: "default" }
      : {};
  };

  const getRightArrowStyle = () => {
    return pageProperty.rightBondage >= pageProperty.page
      ? { opacity: "50%", cursor: "default" }
      : {};
  };

  const getStartEndSlicedTask = () => {
    let itemPerPage = pageProperty.itemPerPage;
    let start = (pageSelected - 1) * itemPerPage;

    let end =
      pageSelected * itemPerPage > props.sortedTask.length
        ? props.sortedTask.length
        : pageSelected * itemPerPage;
    return [start, end];
  };

  const getTotalPage = () => {
    let page = Math.floor(props.sortedTask.length / pageProperty.itemPerPage);

    page =
      page === 0
        ? 1
        : page * pageProperty.itemPerPage < props.sortedTask.length
        ? ++page
        : page;
    return page;
  };

  useEffect(() => {
    const startEnd = getStartEndSlicedTask();
    setItemInPage(props.sortedTask.slice(startEnd[0], startEnd[1]));
    const page = getTotalPage();
    setPageProperty((prevState) => ({
      ...prevState,
      page,
    }));
    updatePageSelection(0, page);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.sortedTask]);

  useEffect(() => {
    const startEnd = getStartEndSlicedTask();
    setItemInPage(props.sortedTask.slice(startEnd[0], startEnd[1]));
    updatePageSelection(0);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pageSelected]);

  return (
    <div
      className="TaskList_container"
      style={{ display: "flex", marginBottom: "2rem" }}
    >
      <div style={{ ...styles.w100 }}>
        <div
          className="header"
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "flex-end",
            justifyContent: "space-between",
          }}
        >
          <h2 style={itemInPage.length !== 0 ? { marginBottom: "0px" } : {}}>
            {props.taskCategory}
          </h2>
          {!props.isLoadingTasks && itemInPage.length !== 0 && (
            <div
              className="pagination"
              style={{
                fontFamily: "Metropolis",
                fontStyle: "normal",
                fontWeight: "bold",
                fontSize: "1rem",
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                color: "#1B274B",
              }}
            >
              <div
                className="left-arrow"
                onClick={addLeftPage}
                style={{
                  backgroundImage: `url(${prevButton})`,
                  backgroundRepeat: "no-repeat",
                  backgroundPosition: "50% 50%",
                  width: "10px",
                  height: "12px",
                  marginRight: "0.5rem",
                  cursor: "pointer",
                  ...getLeftArrowStyle(),
                }}
              ></div>
              <div>{pageElement}</div>
              <div
                className="right-arrow"
                onClick={() => addRightPage(pageProperty.page)}
                style={{
                  backgroundImage: `url(${nextButton})`,
                  backgroundRepeat: "no-repeat",
                  backgroundPosition: "50% 50%",
                  width: "10px",
                  height: "12px",
                  marginLeft: "0.5rem",
                  cursor: "pointer",
                  ...getRightArrowStyle(),
                }}
              ></div>
            </div>
          )}
        </div>
        {props.isLoadingTasks ? (
          <Loading />
        ) : itemInPage.length !== 0 ? (
          itemInPage.map((task) => (
            <div style={{ margin: "1rem 0" }}>
              <TaskListCard
                key={task.id}
                id={task.id}
                title={task.title}
                date={task.end_time}
                submissionStatus={task.is_submitted}
                onOpen={(id) => props.onClick(id)}
              />
            </div>
          ))
        ) : (
          <EmptyCard text={props.emptyText} />
        )}
      </div>
    </div>
  );
};

TaskCollection.propTypes = {
  sortedTask: PropTypes.array.isRequired,
  isLoadingTasks: PropTypes.bool.isRequired,
  taskCategory: PropTypes.string.isRequired,
  emptyText: PropTypes.string.isRequired,
};

export default TaskCollection;
