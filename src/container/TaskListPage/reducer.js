import { fromJS } from "immutable";
import {
  FETCH_TASKS_LIST,
  FETCH_TASKS_LIST_SUCCESS,
  FETCH_TASKS_LIST_FAILED,
} from "./constants";

const initialState = fromJS({
  error: null,
  isLoaded: false,
  isLoading: false,
  tasks: [],
});

function taskPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_TASKS_LIST:
      return state.set("isLoading", true);
    case FETCH_TASKS_LIST_SUCCESS:
      return state
        .set("tasks", action.payload)
        .set("isLoading", false)
        .set("isLoaded", true);
    case FETCH_TASKS_LIST_FAILED:
      return state
        .set("error", action.payload)
        .set("isLoaded", false)
        .set("isLoading", false);
    default:
      return state;
  }
}

export default taskPageReducer;
