import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { useSelector } from "react-redux";
import { getTasks, isTasksLoading } from "../../selectors/tasks";
import TaskCollection from "./components/TaskCollection";
import { push } from "connected-react-router";
import { connect } from "react-redux";

const TaskListPage = ({ push }) => {
  const tasks = useSelector((state) => getTasks(state));
  const isLoadingTasks = useSelector((state) => isTasksLoading(state));
  const [onGoingTask, setOnGoingTask] = useState([]);
  const [previousTask, setPreviousTask] = useState([]);

  const sortTask = (array, reversed = false) => {
    if (reversed) {
      return [...array].sort(
        (elem1, elem2) =>
          new Date(elem2.end_time).getTime() -
          new Date(elem1.end_time).getTime()
      );
    } else {
      return [...array].sort(
        (elem1, elem2) =>
          new Date(elem1.end_time).getTime() -
          new Date(elem2.end_time).getTime()
      );
    }
  };

  const separateTask = (array) => {
    const onGoingTask = [];
    const previousTask = [];
    const mockServerTime = new Date();
    const serverTimeInMilli = new Date(mockServerTime).getTime();
    array.forEach((item) => {
      if (new Date(item.end_time).getTime() - serverTimeInMilli >= 0) {
        onGoingTask.push(item);
      } else {
        previousTask.push(item);
      }
    });
    setOnGoingTask(sortTask(onGoingTask));
    setPreviousTask(sortTask(previousTask, true));
  };

  useEffect(() => {
    separateTask(tasks);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tasks]);

  const handleOpenTask = (id, ...rest) => {
    push(`/task/${id}`, ...rest);
  };

  return (
    <div>
      <Helmet>
        <title>PMB 2020 - Task</title>
      </Helmet>
      <div>
        <TaskCollection
          sortedTask={onGoingTask}
          isLoadingTasks={isLoadingTasks}
          taskCategory="Ongoing Task"
          onClick={(id) => handleOpenTask(id)}
          emptyText="There's no ongoing task"
        />
        <TaskCollection
          sortedTask={previousTask}
          isLoadingTasks={isLoadingTasks}
          taskCategory="Previous Task"
          onClick={(id) => handleOpenTask(id)}
          emptyText="There's no previous task"
        />
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => ({
  push: (url, data) => dispatch(push(url, { ...data })),
});

export default connect(null, mapDispatchToProps)(TaskListPage);
