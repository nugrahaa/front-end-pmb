import axios from "axios";
import {
  FETCH_TOKEN_SUCCESS,
  FETCH_TOKEN_FAILED,
  FETCH_TOKEN,
  CHECK_TOKEN,
  CHECK_TOKEN_SUCCESS,
  CHECK_TOKEN_FAILED
} from "./constants";

import { fetchTokenApi, checkTokenApi } from "../../api";



export function fetchToken(count) {

  return (dispatch) => {

    dispatch({ type: FETCH_TOKEN });
    axios
      .get(fetchTokenApi(count))
      .then((response) => {
        dispatch(checkToken());
        dispatch({
          payload: response.data,
          type: FETCH_TOKEN_SUCCESS,
        });
      })
      .catch((error) => {
        dispatch({
          payload: error,     
          type: FETCH_TOKEN_FAILED,
        });
      });
  };
}


export function checkToken() {

  return (dispatch) => {

    dispatch({ type: CHECK_TOKEN });
    axios
      .get(checkTokenApi)
      .then((response) => {
       
        dispatch({
          payload: response.data,
          type: CHECK_TOKEN_SUCCESS,
        });
      })
      .catch((error) => {
        dispatch({
          payload: error,     
          type: CHECK_TOKEN_FAILED,
        });
      });
  };
}
