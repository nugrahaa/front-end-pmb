// import { fromJS } from "immutable";
import {
    FETCH_TOKEN,
    FETCH_TOKEN_SUCCESS,
    FETCH_TOKEN_FAILED,
  CHECK_TOKEN,
  CHECK_TOKEN_FAILED,
  CHECK_TOKEN_SUCCESS
  } from "./constants";
  
  const initialState = {
    data: [],
    error: null,
    isLoaded: false,
    isLoading: false,
  };
  
  function fetchTokenReducer(state = initialState, action) {
    switch (action.type) {
      case FETCH_TOKEN:
        
        return {
          ...state,
          isLoading: true,
        };
  
      case FETCH_TOKEN_SUCCESS:
       
        return {
          ...state,
          data: action.payload,
          isLoading: false,
          isLoaded: true,
        };
  
      case FETCH_TOKEN_FAILED:
       
        return {
          ...state,
          data:action.payload,
          error: action.payload,
          isLoading: false,
          isLoaded: false,
        };
      
      default:
        return state;
    }
  }
  
    
  function checkTokenReducer(state = initialState, action) {
    switch (action.type) {
      case CHECK_TOKEN:
        
        return {
          ...state,
          isLoading: true,
         
        };
  
      case CHECK_TOKEN_SUCCESS:
       
        return {
          ...state,
          data: action.payload,
          isLoading: false,
          isLoaded: true,
        
        };
  
      case CHECK_TOKEN_FAILED:
       
        return {
          ...state,
          data:[],
          error: action.payload,
          isLoading: false,
          isLoaded: false,
         
        };
      
      default:
        return state;
    }
  }
  
  export {fetchTokenReducer, checkTokenReducer};
  