import styled from 'styled-components';

export const OutermostContainer = styled.div`
  width: 100%;
  height: fit-content;
`

export const TokenGeneratorWrapper = styled.div`
  width: 100%;
  height: 50px;
  display: flex;
  flex-direction: row;
  text-align: center;
  padding: 0;
`

export const GeneratedTokenWidget = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
  padding: 0;

  .generated-token-wrapper {
    border: 1px solid #1B274B;
    box-sizing: border-box;
    border-radius: 10px;
    padding: 0;
    margin: 0px 0px 15px;
    min-height: 40px;
  }

  .generated-token-wrapper h1 {
    font-family: Metropolis;
    font-style: normal;
    font-weight: bold;
    font-size: 28px;
    line-height: 28px;
    text-align: center;
    margin: 5px 0px;
  }

  .generated-token-info-wrapper {
    display: flex;
    flex-direction: row;
  }

  .generated-token-info-wrapper .token-num {
    width: 0%;
    text-align: left;
  }

  .generated-token-info-wrapper .token-num h5 {
    margin: 0;
  }

  .generated-token-info-wrapper .token-num h5 span {
    font-weight: bold;
  }

  .generated-token-info-wrapper .timer {
    width: 100%;
    margin: 0;
  }
`

export const IncrementButton = styled.div`
  width: 30%;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  touch-action: manipulation;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
`

export const IncrementField = styled.div`
  width: 40%;
  border-radius: 10px;
  border: 1px solid #1B274B;

  display: flex;
  align-items: center;
  justify-content: center;

  h3 {
    font-family: "Metropolis";
    font-size: 28px;
    font-weight: bold;
    text-align: center;
    color: #1B274B;
  }
`

export const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  margin: 20px 0px 0px;
`