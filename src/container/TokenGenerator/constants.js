export const FETCH_TOKEN=
  "src/container/TokenGenerator/FETCH_TOKEN";
export const FETCH_TOKEN_SUCCESS =
  "src/container/TokenGenerator/FETCH_TOKEN_SUCCESS";
export const FETCH_TOKEN_FAILED =
  "src/container/TokenGenerator/FETCH_TOKEN_FAILED";

  export const CHECK_TOKEN=
  "src/container/TokenGenerator/CHECK_TOKEN";
export const CHECK_TOKEN_SUCCESS =
  "src/container/TokenGenerator/CHECK_TOKEN_SUCCESS";
export const CHECK_TOKEN_FAILED =
  "src/container/TokenGenerator/CHECK_TOKEN_FAILED";

