import React, { useState, useEffect} from 'react';

import {
  TokenGeneratorWrapper,
  GeneratedTokenWidget,
  IncrementButton,
  IncrementField,
  OutermostContainer,
  ButtonContainer
} from './style';
import { connect } from "react-redux";
import Button from '../../components/Button';
import Timer from '../../components/Timer';
import TokenModal from '../../components/TokenModal';
import TokenWidget from '../../components/TokenWidget';
import MinusIcon from './assets/icon/incMinus.svg';
import PlusIcon from './assets/icon/incPlus.svg';
import { fetchToken, checkToken } from "./actions";
import { getServerTime } from "../../globalActions";


const TokenGenerator = ({
  data,
  checkTokenData,
  checkTokenDataLoading,
  isLoaded,
  isLoading,
  fetchToken,
  checkToken,
  checkTokenDataActive,
  getServerTime,
  ...props
}) => {
  const [open, setOpen] = useState(false);
  const [generatedNum, setGeneratedNum] = useState(1);
  const [generatedToken, setGeneratedToken] = useState({
    count:checkTokenData?.length !==0 ?checkTokenData.count:0,
    token:checkTokenData?.length !==0 ?checkTokenData.token:"",
    endTime:checkTokenData?.length !==0 ?checkTokenData.end_time:"",
    isGenerating:checkTokenData?.length !==0 ?true:false,
  });

  const handleIncrement = (increment) => {
    if (generatedNum >= 1 && generatedNum <= 15) {
      if ((generatedNum !== 15 && increment === 1) ||
          (generatedNum !== 1 && increment === -1)) {
        setGeneratedNum(generatedNum + increment)
      } else {
        setGeneratedNum(generatedNum)
      };
    };
  };

  useEffect(()=>{
    checkToken();
  },[checkToken])

  useEffect(()=>{
    if(checkTokenData?.length!==0){
      setGeneratedToken((prev)=>({
        ...prev,
        'count':checkTokenData.count,
        'token':checkTokenData.token,
        'endTime':checkTokenData.end_time,
        'isGenerating':true
      }));
    }
  },[checkTokenData])

  const handleGenerating = () => {
    getServerTime();
    fetchToken(generatedNum)
    setGeneratedToken((prev)=>({
      ...prev,
      isGenerating: true,
    }));
    setOpen(true);
    console.log(isLoading);
    console.log(isLoaded);
    console.log(checkTokenDataLoading);
  };

  const handleStopGenerating = () => {
    setGeneratedToken(
      {
        token:'',
        endTime:'',
        isGenerating: false ,
      }
    )
    setOpen(false);
    alert("Time's Up!", "error");
  }

  return (
    <TokenWidget>
       <TokenModal
          open={open}
          handleClose={() => {setOpen(false)}}
          token={generatedToken.token}
          count={generatedToken.count}>
        {/* Passing TimerCountdown as children props for modal */}
        <Timer
          start={generatedToken.isGenerating}
          handleClose={handleStopGenerating}
          endDate={generatedToken.endTime}>
        </Timer>
      </TokenModal>
        {generatedToken.isGenerating ?
        <OutermostContainer>
          <GeneratedTokenWidget>
            <div className="generated-token-wrapper">
              {!checkTokenDataLoading
                ? <h1>{generatedToken.token}</h1>
                : <h1>...</h1>}
            </div>
            <div className="generated-token-info-wrapper">
              {/* <div className="token-num">
                <h5><span>Count</span> : {generatedToken.count}</h5>
              </div> */}
              <div className="timer">
                <Timer
                  start={generatedToken.isGenerating}
                  handleClose={() => setOpen(false)}
                  endDate={generatedToken.endTime}>
                </Timer>
              </div>
            </div>
          </GeneratedTokenWidget>
      </OutermostContainer>
        :
        <OutermostContainer>
            <TokenGeneratorWrapper>
              <IncrementButton
                onClick={() => handleIncrement(-1)}>
                <img src={MinusIcon} alt="minus" />
              </IncrementButton>
              <IncrementField>
                <h3>{generatedNum}</h3>
              </IncrementField>
              <IncrementButton
                onClick={() => handleIncrement(1)}>
                <img src={PlusIcon} alt="plus" />
              </IncrementButton>
            </TokenGeneratorWrapper>
            <ButtonContainer>
              <Button
                width="90px"
                height="25px"
                textColor="#1B274B"
                onClick={() => handleGenerating()}>
                Generate
              </Button>
            </ButtonContainer>
          </OutermostContainer>
          }
    </TokenWidget>
  )
}

const mapStateToProps = (state) => ({
  data: state.fetchTokenReducer.data,
  isLoaded: state.fetchTokenReducer.isLoaded,
  isLoading: state.fetchTokenReducer.isLoading,
  checkTokenDataLoading:state.checkTokenReducer.isLoading,
  checkTokenData:state.checkTokenReducer.data,
  checkTokenDataActive:state.checkTokenReducer.isActive
});

const mapDispatchToProps = (dispatch) => ({
  fetchToken: (count) => dispatch(fetchToken(count)),   
  checkToken: () => dispatch(checkToken()),   
  getServerTime:()=>dispatch(getServerTime())
});

export default connect(mapStateToProps, mapDispatchToProps)(TokenGenerator);
