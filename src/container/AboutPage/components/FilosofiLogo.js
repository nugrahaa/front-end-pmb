import React from "react";
import Card from "../../../components/Card/index";
import ProgressiveImage from "../../../components/ProgressiveImage";
import Logo from "./../assets/logo-PMB.png";
import LogoPlace from "./../assets/logo-PMB-small.jpg";

const FilosofiLogo = ({ onClick }) => {
  return (
    <Card
      containerStyle={{
        padding: 21,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <ProgressiveImage
        src={Logo}
        fallbackSrc={LogoPlace}
        style={{
          height: "10em",
          width: "100%",
          objectFit: "contain",
          objectPosition: "center",
        }}
      />
      <h1
        style={{
          alignSelf: "center",
          fontWeight: "bold",
        }}
        onClick={() => onClick()}
      >
        Filosofi Logo
      </h1>
      <p style={{ fontSize: "0.875em" }}>
        Tema layang-layang sebagai suatu entitas yang mengimplementasikan tujuan
        dan nilai kami yaitu adaptasi, kolaborasi, dan ambisi. Kami berharap
        Mahasiswa baru bisa terbang dengan layang-layangnya walaupun menghadapi
        dinamika angin yang menghadangnya, tetapi tetap bisa terbang dengan kuat
        dan menyesuaikan kepada kondisi eksternal yang menerjang. Hamparan
        langit yang luas menggambarkan luasnya tujuan yang bisa diraih para
        mahasiswa baru, dan kita, PMB Fasilkom UI 2020, yang akan bertugas
        sebagai tali untuk membantu mahasiswa baru menerbangkan layangannya
        tinggi di hamparan langit yang luas.
      </p>
    </Card>
  );
};

export default FilosofiLogo;
