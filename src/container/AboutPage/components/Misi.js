import React from "react";
import Card from "../../../components/Card/index";

const Misi = ({ onClick }) => {
  const MISI = [
    "1. Memperkenalkan mahasiswa baru kepada kehidupan dan budaya di Fasilkom UI",
    "2. Membantu mahasiswa baru menyesuaikan diri dengan lingkungan perkuliahan",
    "3. Mendampingi mahasiswa baru untuk menemukan tujuan kedepannya sebagai mahasiswa Fasilkom UI",
    "4. Memberikan kesempatan mahasiswa baru untuk memperluas koneksi dan sudut pandang",
    "5. Mempertemukan mahasiswa baru dengan elemen Fasilkom UI",
  ];

  return (
    <Card
      containerStyle={{
        padding: "1.3125em",
        marginTop: "1.5em",
        alignItems: "flex-start",
      }}
    >
      <h1
        style={{
          fontWeight: "bold",
          textAlign: "center",
        }}
        onClick={() => onClick()}
      >
        Misi
      </h1>
      <div style={{ fontSize: "0.875em" }}>
        {MISI.map((mission, index) => (
          <div key={index} style={{ marginTop: 16, marginBottom: 16 }}>
            {mission}
          </div>
        ))}
      </div>
    </Card>
  );
};

export default Misi;
