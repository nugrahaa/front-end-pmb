import React from "react";
import Card from "../../../components/Card/index";

const Visi = ({ onClick }) => {
  return (
    <Card
      containerStyle={{
        padding: 21,
        marginTop: "1.5em",
        alignItems: "flex-start",
      }}
    >
      <h1
        style={{
          fontWeight: "bold",
          textAlign: "center",
        }}
        onClick={() => onClick()}
      >
        Visi
      </h1>
      <p style={{ fontSize: "0.875em" }}>
        Menjadikan PMB Fasilkom UI 2020 sebagai wadah adaptasi untuk mahasiswa
        baru berkolaborasi dan mencari tujuannya di Fasilkom UI.
      </p>
    </Card>
  );
};

export default Visi;
