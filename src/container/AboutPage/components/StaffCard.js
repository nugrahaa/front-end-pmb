import React from "react";
import ProgressiveImage from "../../../components/ProgressiveImage";

const StaffCard = ({ position, fallbackSrc, fullSrc }) => {
  return (
    <div
      style={{
        boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
        borderRadius: 10,
        padding: 21,
        textAlign: "center",
        display: "flex",
        flexDirection: "column",
        backgroundColor: "white",
      }}
    >
      <div
        style={{
          textAlign: "center",
          fontFamily: "Metropolis",
          fontWeight: "bold",
          fontSize: 20,
        }}
      >
        {position}
      </div>
      <div style={{ flex: 1 }} />
      <ProgressiveImage
        src={fullSrc}
        fallbackSrc={fallbackSrc}
        style={{
          height: "10em",
          width: "100%",
          objectFit: "contain",
          objectPosition: "bottom",
        }}
      />
    </div>
  );
};

export default StaffCard;
