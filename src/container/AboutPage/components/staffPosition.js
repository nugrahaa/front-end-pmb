const POSITION = [
  "Ketua Pelaksana",
  "Wakil Ketua Pelaksana",
  "Sekretaris",
  "Sekretaris 2",
  "Bendahara 1",
  "Bendahara 2",
  "PJ Acara",
  "PJ Akademis",
  "PJ Mentor",
  "PJ Komdis",
  "PJ Danus",
  "PJ Dokum",
  "PJ Humas",
  "PJ Kreatif",
  "PJ Medis",
  "PJ Penunjang",
];

export default POSITION;
