import React from "react";
import StaffCard from "./StaffCard";
import POSITION from "./staffPosition";
import { Media } from "../../../components/Layout/Media";

const StaffSection = () => {
  return (
    <Media>
      {(matches) => (
        <div
          style={{
            width: "100%",
            display: "grid",
            gridTemplateColumns: matches.xxs ? "1fr" : "1fr 1fr",
            gridGap: 20,
            marginTop: "1.5em",
          }}
        >
          {POSITION.map((position, index) => (
            <StaffCard
              key={index}
              position={position}
              fullSrc={`/static/images/staff-${index + 1}.png`}
              fallbackSrc={`/static/images/placeholder/staff-${
                index + 1
              }-small.png`}
            />
          ))}
        </div>
      )}
    </Media>
  );
};

export default StaffSection;
