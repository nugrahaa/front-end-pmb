import React, { useState, useEffect } from "react";
import StaffSection from "./components/StaffSection";
import { Media } from "../../components/Layout/Media";
import Visi from "./components/Visi";
import Misi from "./components/Misi";
import FilosofiLogo from "./components/FilosofiLogo";
import { Helmet } from "react-helmet";

const AboutPage = () => {
  const [secret, setSecret] = useState("");
  useEffect(() => {
    if (secret === "abcabcabc") {
      localStorage.setItem(
        "pmb-2020-secret-verified",
        "If you're seeing this, then keep on hacking!"
      );
      alert("Secret Unlocked!");
    }
  }, [secret]);
  const handleClick = (section) => {
    switch (section) {
      case "logo":
        setSecret((prev) => prev.concat("a"));
        break;
      case "visi":
        setSecret((prev) => prev.concat("b"));
        break;
      case "misi":
        setSecret((prev) => prev.concat("c"));
        break;
      default:
        return;
    }
  };
  return (
    <Media>
      {(matches) => (
        <div>
          <Helmet>
            <title>PMB 2020 - About</title>
          </Helmet>
          {!matches.mobile && <h2>About</h2>}
          <FilosofiLogo onClick={() => handleClick("logo")} />
          <Visi onClick={() => handleClick("visi")} />
          <Misi onClick={() => handleClick("misi")} />
          <StaffSection />
        </div>
      )}
    </Media>
  );
};

export default AboutPage;
