// import { fromJS } from "immutable";
import {
    POST_TOKEN,
    POST_TOKEN_SUCCESS,
    POST_TOKEN_FAILED,
  
  } from "./constants";
  
  const initialState = {
    data: [],
    error: null,
    isLoaded: false,
    isLoading: false,
  };
  
  function postTokenReducer(state = initialState, action) {
    switch (action.type) {
      case POST_TOKEN:
        
        return {
          ...state,
          isLoading: true,
        };
  
      case POST_TOKEN_SUCCESS:
       
        return {
          ...state,
          data: action.payload,
          isLoading: false,
          isLoaded: true,
        };
  
      case POST_TOKEN_FAILED:
       
        return {
          ...state,
          data:action.payload,
          error: action.payload,
          isLoading: false,
          isLoaded: false,
        };
      
      default:
        return state;
    }
  }
  
  
  
  export default postTokenReducer;
  