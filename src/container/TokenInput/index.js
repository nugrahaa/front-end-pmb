import React, { useState } from 'react';
import {
  OutermostContainer,
  TokenInputField,
  ButtonContainer
} from './style';

import Button from '../../components/Button';
import TokenWidget from '../../components/TokenWidget';
import { connect } from "react-redux";
import { postToken } from "./actions";

const TokenInput = ({postToken, props}) => {
  
  const [tokenInput, setTokenInput] = useState("");

  const handleInputChange = (e) => {
    setTokenInput(e.target.value);
  }

  const handleSubmit = () => {
   
    postToken(tokenInput)
    
  }

  return (
    <TokenWidget>
      <OutermostContainer>
        <TokenInputField>
          <input
            className="token-input-field"
            value={tokenInput}
            onChange={(e) => handleInputChange(e)}
            maxLength="6"
          />
        </TokenInputField>
        <ButtonContainer>
          <Button
            width="90px"
            height="25px"
            textColor="#1B274B"
            onClick={() => handleSubmit()}
            disabled={tokenInput.length !== 6}>
            Submit
          </Button>
        </ButtonContainer>
      </OutermostContainer>
    </TokenWidget>
  )
}

const mapStateToProps = (state) => ({
  data: state.postTokenReducer.data,
  isLoaded: state.postTokenReducer.isLoaded,
  isLoading: state.postTokenReducer.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
  postToken: (token) => dispatch(postToken(token)),   
});

export default connect(mapStateToProps, mapDispatchToProps)(TokenInput);

