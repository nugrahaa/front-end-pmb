import axios from "axios";
import {
  POST_TOKEN_SUCCESS,
  POST_TOKEN_FAILED,
  POST_TOKEN,
} from "./constants";
import { push } from "connected-react-router";
import { postTokenApi } from "../../api";



export function postToken(token) {

  return (dispatch) => {

    dispatch({ type: POST_TOKEN });
    axios
      .post(postTokenApi, {
        token: token,
      })
      .then((response) => {
        // console.log(response.data.status)
        if (response.data.status === 'NOT_FRIEND') {
          alert("Token Add Success");
          dispatch(push(`/friends/add/${response.data.id}`));
        } else {
          alert("This person is already in your friendlist!", 'error');
          setTimeout(
            () => dispatch(push(`/friends`))
          ,2000);
        }
        dispatch({
          payload: response.data,
          type: POST_TOKEN_SUCCESS,
        });
      })
      .catch((error) => {
        alert("Token Add Failed",'error');
        dispatch({
          payload: error,     
          type: POST_TOKEN_FAILED,
        });
      });
  };
}
