import styled from 'styled-components';

export const OutermostContainer = styled.div`
  width: 100%;
`

export const TokenInputField = styled.div`
  width: 100%;
  text-align: center;

  border: 1px solid #1B274B;
  box-sizing: border-box;
  border-radius: 10px;

  font-family: Metropolis;
  font-style: normal;

  .token-input-field {
    width: calc(100% - 70px);
    max-width: fit-content;
    margin: 5px 0px;

    font-weight: bold;
    font-size: 28px;
    line-height: 28px;
    text-align: center;
    color: #1B274B;

    border: none;
    text-decoration: none;

    @media (max-width: 768px) {
      width: calc(100% - 100px);
    }
  }

  .token-input-field::placeholder {
    font-weight: normal;
    transform: scale(0.485);
  }

  .token-input-field:-ms-input-placeholder {
    font-weight: normal;
    transform: scale(0.485);
  }

  .token-input-field:focus {
    outline: none;
  }
`

export const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  margin: 10px 0px 0px;
`