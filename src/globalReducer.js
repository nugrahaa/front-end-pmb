import { fromJS } from "immutable";
import { isEmpty } from "lodash";
import auth from "./auth";
import {
  SET_AUTH,
  SET_USER,
  SET_USER_SUCCESS,
  // SET_USER_FAILURE,
  SET_SERVER_TIME,
  SENDING_REQUEST,
  REQUEST_ERROR,
  CLEAR_ERROR,
} from "./globalConstants";

const isLoggedIn = auth.loggedIn();

const initialState = fromJS({
  currentlySending: false,
  error: "",
  loggedIn: !isEmpty(isLoggedIn),
  serverTime: null,
  user: {},
  hasLoaded: false,
  isLoading: false,
});

function globalReducers(state = initialState, action) {
  switch (action.type) {
    case SET_AUTH:
      return state.set("loggedIn", action.newAuthState);
    case SET_USER:
      return state.set("isLoading", true);
    case SET_USER_SUCCESS:
      return state.set("user", action.user).set("isLoading", false);
    case SENDING_REQUEST:
      return state.set("currentlySending", action.sending);
    case REQUEST_ERROR:
      return state.set("error", action.error);
    case CLEAR_ERROR:
      return state.set("error", "");
    case SET_SERVER_TIME:
      return state.set("serverTime", action.payload).set("hasLoaded", true);
    default:
      return state;
  }
}

export default globalReducers;
